
 $(document).ready(function(){
 	//alert("Page loaded");
	var present=[];
	var lec_date=[];
	var lec_num=[];
	var adminids=[];
	var batch_id=$("#BATCHID").val();
	var start_time=$("#STARTTIME").val();
	var end_time=$("#ENDTIME").val();
	totalCount=($("#maxcount").val()-$("#startVal").val())*$("#rowcount").val();
	 for (var i = 0; i < totalCount; i++) {
		present.push(0);
	}
	var admissionids = $("input[name='admissionid[]']").map(function(){return $(this).val();}).get();
	
	$.each(admissionids, function(i, val) {
 		if(val!='')
 		{
 			adminids.push(val);
 		}
	 });


 	$("#attendance").click(function(){
 		lec_date=[];
 		lec_num=[];
	 	var lecture_number = $("input[name='lecture_number[]']").map(function(){return $(this).val();}).get();
	 	var lecture_date = $("input[name='lecture_date[]']").map(function(){return $(this).val();}).get();
	 	$.each(lecture_number, function (i, val) {
	 		if(val!='')
	 		{
	 			lec_num.push(val);
	 		}
	 	});
	 	$.each(lecture_date, function (i, val) {
	 		if(val!='')
	 		{
	 			lec_date.push(val);
	 		}
	 	});

	 	if(lec_num.length==0)
	 	{
	 		alert("No entries made for lecture number,kindly enter atleast one Lecture detail.");
	 	}
	 	else
	 	{
		 	if(lec_date.length==0)
		 	{
		 		alert("No date entries done,kindly enter atleast one Lecture detail.");
		 	}
		 	else if(lec_date.length!=lec_num.length)
		 	{
		 		alert("Kindly enter date for the selected Lecture.");	
		 	}
		 	else
		 	{
				var columnCount=$("#maxcount").val()-$("#startVal").val();
				var JSONData=[];
				for (var i=0;i<lec_num.length;i++)
				{
					var admissionArray=[];
					var attendanceArray=[];
					var adminidCount=0;
					var jsonBatchData={};
					var admissionAttendanceArray=[];
					for (var j=0;j<present.length;j++)
					{
						var admissionAttendance={};
						if(j%columnCount==i)
						{
							adminidCount=parseInt(j/columnCount,10);
							admissionAttendance.attendance=present[j];
							admissionAttendance.admission=adminids[adminidCount];
							admissionAttendanceArray.push(admissionAttendance);
						}
					}
					jsonBatchData.lectureNumber=lec_num[i];
					jsonBatchData.lectureDate=lec_date[i];
					jsonBatchData.admissionAttendance=admissionAttendanceArray;
					JSONData.push(jsonBatchData);
				}
				$(this).prop("disabled", true);
				$.ajax({
				    dataType: "json",
				    url: base_url('batchMaster/saveAttendance'),
				    type: "post",
				    data: { admissionDetails: JSONData,
				            batch_id: batch_id,
				            start_time:start_time,
				            end_time:end_time,
				         angelos_csrf_token: csrfTkn()},
				    success: function(result){
				    	alert("Entry Successfully Done");
				    	location.reload();
				    },
				    error: function(xhr, status, error) {
			            alert("Entry Successfully Done");
				    	location.reload();
			        }
				});
		 	}
	 	}
	 	
	 });

 	$(".select").click(function(){

 		checkboxID=$(this).attr("id").split('_');
 		clickedCheckBox=checkboxID[1];
 		if($(this).is(':checked')==true)
 		{
 			present[clickedCheckBox]=1;
 		}
 		else
 		{
 			present[clickedCheckBox]=0;
 		}
 		 
	});

});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var currentDate = dd+'/'+mm+'/'+yyyy;

$('.add').datepicker(
    { 
        dateFormat: 'dd/mm/yy'
    }
).val(currentDate);

datePicker();

function datePicker(){
  $('.enquiry_date').datepicker(
      { 
          dateFormat: 'dd/mm/yy'


      }
  ).val(currentDate);
}


