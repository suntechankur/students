
function base_url(page = ''){
	if (window.location.origin == 'http://localhost' || window.location.origin == 'http://localhost:'+location.port) {
		return window.location.origin + '/student_desk/' + page;
	}
	else{
		return window.location.origin + '/student_desk/' + page;
	}
}


function csrfTkn(){
	var admToken = $('.adminToken').val();
	return admToken;
}

$( function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
} );

$('.student_desk_tabs').click(function(e) {
	var tabName = $(this).attr("data-name");
	$(".student_desk_forms").fadeOut(100);
	$("#"+tabName+"-form").delay(100).fadeIn(100);
	$(".student_desk_tabs").removeClass('active');
	if(tabName == "basic-details"){
		$("#slideoutalert").show();
	}
	else{
		$("#slideoutalert").hide();
	}
	$(this).addClass('active');
	var date = new Date();
	var twoDigitMonth = ((date.getMonth().length+1) === 1)? (date.getMonth()+1) : '0' + (date.getMonth()+1);
	var currentDate = date.getDate() + "/" + twoDigitMonth + "/" + date.getFullYear();
	if(tabName == "practical-exam"){
		var practicalDate = $("#"+tabName+"-form").find("#practical_date").val();
		var startPracticalDate = $("#"+tabName+"-form").find("#start_practical_date").text();
		var endPracticalDate = $("#"+tabName+"-form").find("#end_practical_date").attr("data-date");
		var availability = dateCheck(startPracticalDate,endPracticalDate,currentDate);
		$("#practical-exam-booking-form").empty();
		if(availability){
			$("#practical-exam-booking-form").html('<br/><input type="submit" name="accept_terms" tabindex="4" data-form_type="practical" class="form-control btn btn-login exam_form_visible" value="I Accept & Understand" style="background-color: #F00;outline: none;color: #fff;font-size: 14px;font-weight: normal;padding: 5px 0;text-transform: uppercase;border-color: #F00;">');
		}
	}
	else if(tabName == "theory-exam"){
		var theoryDate = $("#"+tabName+"-form").find("#theory_date").val();
		var startTheoryDate = $("#"+tabName+"-form").find("#start_theory_date").text();
		var endTheoryDate = $("#"+tabName+"-form").find("#end_theory_date").attr("data-date");
		var availability = dateCheck(startTheoryDate,endTheoryDate,currentDate);
		$("#theory-exam-booking-form").empty();
		if(availability){
			$("#theory-exam-booking-form").html('<br/><input type="submit" name="accept_terms" tabindex="4" data-form_type="theory" class="form-control btn btn-login exam_form_visible" value="I Accept & Understand" style="background-color: #F00;outline: none;color: #fff;font-size: 14px;font-weight: normal;padding: 5px 0;text-transform: uppercase;border-color: #F00;">');
		}
	}
	e.preventDefault();
});

function dateCheck(dateFrom,dateTo,dateCheck) {
	var d1 = dateFrom.split("/");
	var d2 = dateTo.split("/");
	var c = dateCheck.split("/");

	var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
	var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
	var check = new Date(c[2], parseInt(c[1])-1, c[0]);

	// console.log(check > from && check < to);

    if(check > from && check < to) {
        return true;
    }
    return false;
}

//exam booking page center selection
$(".select_exam_centre_name").change(function(){
	var centreid = $(this).val();
	var exam_date = $("#exam_scheduled_date").val();
	$.ajax({
		dataType: "json",
		url: base_url('exam/get_exam_slots'),
		type: "post",
		data: { centreid  : centreid,
						examdate  : exam_date,
						studentdesk_token : csrfTkn() },
		success: function(result) {
			console.log(result);
			$('#exam_time_select_table').show();
			var bookednine=0;
			var bookedninefifty=0;
			var bookedtenforty=0;
			var bookedeleventhirty=0;
			var bookedtwelvetwenty=0;
			var bookedoneten=0;
			var bookedtwo=0;
			var bookedtwofifty=0;
			var bookedthreeforty=0;
			var bookedfourthirty=0;
			var bookedfivetwenty=0;
			var notbookednine=0,notbookedninefifty=0,notbookedtenforty=0,notbookedeleventhirty=0,notbookedtwelvetwenty=0,notbookedoneten=0,notbookedtwo=0,notbookedtwofifty=0,notbookedthreeforty=0,notbookedfourthirty=0,notbookedfivetwenty=0;
		  $.each(result, function(index,item) {
				if((item.EXAM_TIME == "09:00") || (item.EXAM_TIME == "09:00 AM")){
					bookednine += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "09:50") || (item.EXAM_TIME == "09:50 AM")) {
					bookedninefifty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "10:40") || (item.EXAM_TIME == "10:40 AM")) {
					bookedtenforty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "11:30") || (item.EXAM_TIME == "11:30 AM")) {
					bookedeleventhirty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "12:20") || (item.EXAM_TIME == "12:20 PM")) {
					bookedtwelvetwenty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "01:10") || (item.EXAM_TIME == "01:10 PM")) {
					bookedoneten += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "02:00") || (item.EXAM_TIME == "02:00 PM")) {
					bookedtwo += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "02:50") || (item.EXAM_TIME == "02:50 PM")) {
					bookedtwofifty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "03:40") || (item.EXAM_TIME == "03:40 PM")) {
					bookedthreeforty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "04:30") || (item.EXAM_TIME == "04:30 PM")) {
					bookedfourthirty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "05:20") || (item.EXAM_TIME == "05:20 PM")) {
					bookedfivetwenty += parseInt(item.Booking_Count);
				}
				notbookednine= parseInt(20 - bookednine);
				notbookedninefifty= parseInt(20 - bookedninefifty);
				notbookedtenforty= parseInt(20 - bookedtenforty);
				notbookedeleventhirty= parseInt(20 - bookedeleventhirty);
				notbookedtwelvetwenty= parseInt(20 - bookedtwelvetwenty);
				notbookedoneten= parseInt(20 - bookedoneten);
				notbookedtwo= parseInt(20 - bookedtwo);
				notbookedtwofifty= parseInt(20 - bookedtwofifty);
				notbookedthreeforty= parseInt(20 - bookedthreeforty);
				notbookedfourthirty= parseInt(20 - bookedfourthirty);
				notbookedfivetwenty= parseInt(20 - bookedfivetwenty);
		  });
			$("#bookedfor900am").text(bookednine);
			$("#bookedfor950am").text(bookedninefifty);
			$("#bookedfor1040am").text(bookedtenforty);
			$("#bookedfor1130am").text(bookedeleventhirty);
			$("#bookedfor1220pm").text(bookedtwelvetwenty);
			$("#bookedfor110pm").text(bookedoneten);
			$("#bookedfor200pm").text(bookedtwo);
			$("#bookedfor250pm").text(bookedtwofifty);
			$("#bookedfor340pm").text(bookedthreeforty);
			$("#bookedfor430pm").text(bookedfourthirty);
			$("#bookedfor520pm" ).text(bookedfivetwenty);

			$("#notbookedfor900am").text(notbookednine);
			$("#notbookedfor950am").text(notbookedninefifty);
			$("#notbookedfor1040am").text(notbookedtenforty);
			$("#notbookedfor1130am").text(notbookedeleventhirty);
			$("#notbookedfor1220pm").text(notbookedtwelvetwenty);
			$("#notbookedfor110pm").text(notbookedoneten);
			$("#notbookedfor200pm").text(notbookedtwo);
			$("#notbookedfor250pm").text(notbookedtwofifty);
			$("#notbookedfor340pm").text(notbookedthreeforty);
			$("#notbookedfor430pm").text(notbookedfourthirty);
			$("#notbookedfor520pm" ).text(notbookedfivetwenty);
		}
	});
});

$(".selectexamtime").click(function(){
	if($(this).closest('tr').find(".availableseat").text() <= 20){
		$("#exam_scheduled_time").val($(this).data("time"));
	}
	else{
		alert("Seats already full of this time slot.");
	}
});
//end of exam booking page center selection

$(document).on("click",".exam_form_visible",function(){
	var exam_type = $(this).attr("data-form_type");
	$("#"+exam_type+"_booking_form").show();
	$("#"+exam_type+"_exam_form_visible").val("show");
});

$(".notice_form").click(function(){
	var notice_type = $(this).attr("data-notice");
	var switch_value = $("#"+notice_type+"_notice_form").attr("data-switch");
	if(switch_value == "0"){
		$("#"+notice_type+"_notice_form").show();
		$("#"+notice_type+"_span").html('<i class="fa fa-caret-down"></i>');
		$("#"+notice_type+"_notice_form").attr("data-switch","1");
	}
	else if(switch_value == "1"){
		$("#"+notice_type+"_notice_form").hide();
		$("#"+notice_type+"_span").html('<i class="fa fa-caret-right"></i>');
		$("#"+notice_type+"_notice_form").attr("data-switch","0");
	}
});

$(".notice_title").click(function(){
	var noticeId = $(this).attr("id");
	var switch_value = $("#"+noticeId+"_body").attr("data-switch");
	if(switch_value == "0"){
		$("#"+noticeId+"_body").show();
		$("#"+noticeId+"_body").attr("data-switch","1");
	}
	else if(switch_value == "1"){
		$("#"+noticeId+"_body").hide();
		$("#"+noticeId+"_body").attr("data-switch","0");
	}
});

$(".notice_body").on("click","a",function(){
	var link = $(this).attr("href");
	window.location.replace(base_url('notice_download/'+link+'/lwn'));
	return false;
});

$(".book_type_radio").change(function(){
	var bookType = $('input[name="book_type"]:checked').val();
	if(bookType != undefined){
		if(bookType == 'U'){
			$(".book_selection").hide();
			$(".book_download_table").hide();
			$(".university_book_download_table").show();
		}
		else{
			$(".book_selection").show();
			$(".university_book_download_table").hide();

			$.ajax({
				dataType: "json",
				url: base_url('ebook/get_subject_name'),
				type: "post",
				data: { book_type  : bookType,
								studentdesk_token : csrfTkn() },
				success: function(result) {
					var option = "";
					var subject = result.subjects;
					$(".book_download_from_student_desk").empty();
					$(".book_download_from_student_desk").append("<option disabled='disabled' selected='selected'>Select Subject</option>");
					for(var i= 0;i<subject.length;i++){
						option = "<option value="+subject[i]['COURSE_ID']+">"+subject[i]['COURSE_NAME']+"</option>";
						$(".book_download_from_student_desk").append(option);
					}
				}
			});
		}
	}
	else{
		alert("Kindly select book category By University / By Course.");
	}
});

$(".book_download_from_student_desk").change(function(){
	var fileName = $(this).val();
	$.ajax({
		dataType: "json",
		url: base_url('ebook/get_book_details'),
		type: "post",
		data: { file_name  : fileName,
						studentdesk_token : csrfTkn() },
	  beforeSend: function() {
      $("#divLoading").addClass('show');
    },
		complete: function() {
      $("#divLoading").removeClass('show');
    },
		success: function(result) {
			var book = result.books;
			$(".book_download_table").show();
			$(".books_details_download_section").empty();
			if(book.length > 0){
				for(var i= 0;i<book.length;i++){
					var semester = "";
					if(book[i]['SEMISTER'] == null){
						semester = "";
					}
					else{
						semester = book[i]['SEMISTER'];
					}
					var option = "<tr><td>"+(i+1)+"</td><td>"+book[i]['TYPE']+"</td><td>"+book[i]['FILE_NAME']+"</td><td>"+book[i]['MODIFIED_DATE']+"</td><td>"+semester+"</td><td align='center'><span data-file_name="+book[i]['TITLE']+" style='font-size:12px;cursor:pointer' class='download_book_link'><i class='fa fa-download'></i></span></td></tr>";
					$(".books_details_download_section").append(option);
				}
			}
			else{
				var option = "<tr><td>No book data available.</td></tr>";
				$(".books_details_download_section").append(option);
			}
		}
	});
});

$(".books_details_download_section,.university_books_details_download_section").on("click",".download_book_link",function(){
	var bookPath = $(this).attr("data-file_name");
	window.location.replace(base_url('book_download/'+bookPath+'/ndb'));
});

$(".application_or_request").click(function(){
	var elementId = $(this).val();
	$(".request_application_form").hide();
	$("#"+elementId).show();
	$("#application_button").show();
});

// disable right click and f12 and ctrl+shift+I
// $(document).bind("contextmenu",function(e){
//  return false;
// });
// $(document).keydown(function (event) {
//     if (event.keyCode == 123) { // Prevent F12
//         return false;
//     } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
//         return false;
//     }
// });
// disable right click and f12 and ctrl+shift+I
