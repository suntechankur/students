$('#item').change(function(){
	var itemId = $(this).val();
	$.ajax({
            url: base_url('requisition/stationeryRequisition'),
            dataType: 'Json',
            data: {
            	itemId: itemId,
                angelos_csrf_token: csrfTkn()
            },
            	method: 'post',
            success: function(result)
            {
            	$("#item, .item").removeAttr('style');
            	$('.statForm').empty();
            	var html = '<div class="form-group col-md-2"><label for="reqQty">Required Quantity:</label><input type="text" class="form-control" id="reqQty"><span class="text-danger reqQtyMsg"></span></div><div class="col-md-3"><label for="maxQty">Max.Qty for Requisition</label><br><span id="maxQty">'+result[0]['MAX_QTY_FOR_REQUISITION']+'</span></div><div class="form-group col-md-2"><label for="curStock">Enter Current Stock:</label><input type="text" class="form-control" id="curStock"><span class="text-danger curStockMsg"></span></div><div class="col-md-2"><label for="">&nbsp;</label><br><button type="button" class="btn btn-primary addReq">Add</button></div>';

            	$('.statForm').append(html);
            }
        });
});


$('.QTY_ISSUED').keyup(function(){
	var idval = $(this).attr("id");
	var issued = $("#"+idval).val();
	var qtyrequested = parseInt($("#QTY_REQUESTED"+idval).text());

	if(issued.match(/^\d+$/)){
		if(issued <= qtyrequested){
		}
		else{
			alert("Quantity Issued cannot be greater than Quantity Requested.");
			$("#"+idval).val('');
			return false;
		}
	}
	else{
		//alert("issued should be numeric");
		$("#"+idval).val('');
	}

	// alert("Issued value = "+issued+" requested count = "+qtyrequested);
});

$('.statForm').on('keyup','#reqQty', function(){
	if($(this).val() != ''){
		$('#reqQty').removeAttr('style');
		$('.reqQtyMsg').empty();
	}
	else{
		$('#reqQty').attr('style','border:2px solid red');
		$('.reqQtyMsg').html('Enter required quantity');
	}
});

$('.statForm').on('keyup','#curStock', function(){
	if($(this).val() != ''){
		$('#curStock').removeAttr('style');
		$('.curStockMsg').empty();
	}
	else{
		$('#curStock').attr('style','border:2px solid red');
		$('.curStockMsg').html('Enter Current Stock');
	}
});

$('.statForm').on('click','.addReq', function(){

	var item = $("#item option:selected").text();
	var itemVal = $("#item").val();
	var reqQty = $('#reqQty').val();
	var maxQty = $('#maxQty').text();
	var curStock = $('#curStock').val();

	if (itemVal == '' || reqQty == '' || curStock == '') {
		if (itemVal == '') {
			$('#item').attr('style','border:2px solid red');
			$('.itemMsg').html('Select Required stationery Items');
		}
		else{
			$('#item').removeAttr('style');
			$('.itemMsg').empty();
		}

		if (reqQty == '') {
			$('#reqQty').attr('style','border:2px solid red');
			$('.reqQtyMsg').html('Enter required quantity');
		}
		else{
			$('#reqQty').removeAttr('style');
			$('.reqQtyMsg').empty();
		}

		if (curStock == '') {
			$('#curStock').attr('style','border:2px solid red');
			$('.curStockMsg').html('Enter Current Stock');
		}
		else{
			$('#curStock').removeAttr('style');
			$('.curStockMsg').empty();
		}
	}
	else{
		var purchOrdLastRow = 1;
	            	if($('.statReqTbl tr').data('row_num')){
	            		purchOrdLastRow = $('.statReqTbl tr').last().data('row_num') + 1;
	            	}

                	var html = '<tr data-row_num="'+purchOrdLastRow+'"><td>'+purchOrdLastRow+'</td><td>'+item+'</td><td>'+reqQty+'</td><td>'+maxQty+'</td><td>'+curStock+'</td></tr>';

                	var saveRec = '<input class="itemVal" type="hidden" value="'+itemVal+'" name="item"><input class="reqQty" type="hidden" value="'+reqQty+'" name="reqQty[]"><input class="curStockVal" type="hidden" value="'+curStock+'" name="curStock[]">';

                	var saveRecBtn = '<button type="button" class="btn btn-primary saveRec">Save Requisition</button>';

                	$('.statReqTbl').append(html);
                	$('.saveRecDiv').append(saveRec);
                	$('.saveRec').remove();
                	$('.saveRecDiv').append(saveRecBtn);
	}
});



$('.saveRecDiv').on('click', '.saveRec', function(){
	// var items = $('.itemVal').val().serializeArray();
	// alert(items);
	var items = $('.itemVal').map(function()
    {
       return $(this).val();
    }).get();

    var reqQty = $('.reqQty').map(function()
    {
       return $(this).val();
    }).get();

    var curStockVal = $('.curStockVal').map(function()
    {
       return $(this).val();
    }).get();

    var recForm = $(".itemVal").serializeArray();

	$.ajax({
						 dataType: 'json',
	           url: base_url('requisition/addReq'),
						 type: 'post',
	           data: {
	           	item: items,
	           	reqQty: reqQty,
	           	curStockVal: curStockVal,
	            angelos_csrf_token: csrfTkn()
	           },
             beforeSend: function(){
               $("#wait").show();
             },
             complete: function(){
               $("#wait").hide();
             },
	           success: function(result)
	           {
	           	window.location.href = base_url('requisition/stationery');
	           }
	       });
});
