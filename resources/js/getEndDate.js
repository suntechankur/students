$("body").change(function(){
	var duration = $("#DURATION").val();
	var endtime = $("#ENDTIME").val();
	var starttime = $("#STARTTIME").val();
	if(duration && endtime && starttime){
		if(($("#Mon").is(':checked')) || ($("#Tue").is(':checked')) || ($("#Wed").is(':checked')) || ($("#Thu").is(':checked')) || ($("#Fri").is(':checked')) || ($("#Sat").is(':checked')) || ($("#Sun").is(':checked'))){
			$("#STARTDATE").removeAttr("disabled");
		}
	}
});

function endDate()
{
 if (document.getElementById("STARTDATE").value!="")
{
    end = document.getElementById("ENDTIME").value;
    start = document.getElementById("STARTTIME").value;

    var endsplit = end.split(":");
    var startsplit = start.split(":");

    var endampmsplit = endsplit[1].split(" ");
    var startampmsplit = startsplit[1].split(" ");

    if(endampmsplit[1] == 'pm')
    {
    	endsplit[0] = parseInt(endsplit[0]) + 12;
    }

    if(startampmsplit[1] == 'pm')
    {
    	startsplit[0] = parseInt(startsplit[0]) + 12;
    }

    var endhr = endsplit[0];
    var endmin = endampmsplit[0];
    var starthr =  startsplit[0];
    var startmin = startampmsplit[0];

     var endDate = new Date(0, 0, 0, endhr, endmin, 0);
     var startDate = new Date(0, 0, 0, starthr, startmin, 0);


    var diff = endDate - startDate;
    var hr = diff / 1000 / 60 / 60;
	var days;

	if (hr<=0)
	{
	alert("Batch start time should not be greater than end time");
	document.getElementById("ENDTIME").focus();
	}
	else
	{
		var dt=new Date();
		var dt1=document.getElementById("STARTDATE").value;
		var dt2=dt1.split("/");

		dt.setDate(dt2[0]);
		dt.setMonth(eval(dt2[1])-1);
		dt.setYear(dt2[2]);

		var dur=parseInt(document.getElementById("DURATION").value);

		//var dur=40;//document.getElementById("<%=ddDur.ClientID%>").options[document.getElementById("<%=ddsubject.ClientID%>").selectedIndex].value
		var bdays=new Array();
		bdays[0]=document.getElementById("Sun").checked;
		bdays[1]=document.getElementById("Mon").checked;
		bdays[2]=document.getElementById("Tue").checked;
		bdays[3]=document.getElementById("Wed").checked;
		bdays[4]=document.getElementById("Thu").checked;
		bdays[5]=document.getElementById("Fri").checked;
		bdays[6]=document.getElementById("Sat").checked;
		days=Math.floor((dur+1)/hr);
		while(days>1)
		{
			dt.setDate(dt.getDate()+ 1);
			if(bdays[dt.getDay()-1]===true)
			{
				days--;
			}

	    }
		//document.getElementById("STARTDATE").value= dt2[2]+'/'+dt2[1]+'/'dt2[0];
		document.getElementById("EXPECTEDENDDATE").value=dt.getDate()+ '/' + (dt.getMonth()+1) + '/' + dt.getFullYear();
		dt.setDate(dt.getDate()+ 7);
		document.getElementById("INTERNALEXAMDATE").value=dt.getDate()+ '/' + (dt.getMonth()+1) + '/' + dt.getFullYear();
		document.getElementById("SESSION").value=Math.floor((dur)/hr);
	}
}

}
