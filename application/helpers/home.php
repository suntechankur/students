    <div id="myCarousel" class="carousel">
    <!-- Wrapper for slides -->
    <!-- Indicators -->
    <div class="carousel-inner" role="listbox">
    <div class="item active">
    <img src="resources/assets/images/bg.jpg" alt="Banner" >
    <div class="col-md-6 col-md-offset-4" style="position:absolute;margin-left: 29%;
    margin-top: 5%;width:40%;background-color:#1118174d;color:black;top:0;left:0;padding:10px;font-size:16px;z-index:5;text-align:center;">
    <?php echo form_open('members_index/index')?>
    <select type="text" style="text-align:center;width:80%" placeholder="select city" name="city" id="searchcity" >
    <?php foreach($citylist as $cities){?>
    <option style="padding: 10px;
    border-bottom: 2px solid silver" value="<?php echo $cities['cityid']?>"><?php echo $cities['cityname'];?></option>
    <?php }?>
    </select>
    <?php echo form_close();?>
    </div>
   </div>
  </div>
</div>
<style>
.panel-heading{    box-shadow: 0px 0px 2px -1px;
    background: transparent radial-gradient(300% 202%, rgb(234, 234, 234), rgb(245, 241, 241)) repeat scroll 0% 0%;
  }
#select_button{box-shadow: 0px 1px 1px 0px;
background-color: #39cccc;margin-bottom:10px;}
</style>
<div id="service_container">
  <div id="services">
      <!-- <div class="well text-center" id="heading">

      </div> -->
      <div id="ser_well" class="row well">
        <div style="margin-top:5px;">
        <?php foreach ($services as $service) {?>
        <div class="col-xs-6 col-sm-3">
      <a href="#" class="<?php echo $service['SMId']; ?>" data-toggle="modal" data-target="#<?php echo $service['SMId']; ?>"> <?php if($service['SMId']>9){?><div class="tooltips"><?php }?>
        <div class="img-thumbnail img-circle" id="plumbing">
          <img class="img-responsive img-thumbnail img-circle inner" src="<?php echo base_url().$service['file_path']; ?>">
        </div><?php if($service['SMId']>9){?><span class="tooltiptext">comming soon</span></div><?php }?>
          <p><?php echo $service['SMName']; ?></p>

      </a>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>

  <!-- Modal -->
<?php foreach ($services as $servicesformodal) {
if($servicesformodal['SMId']<=9)
{
  ?>

<div id="<?php echo $servicesformodal['SMId'];?>" tabindex="-1" class="modal fade" aria-labelledby="jobModalLabel<?php echo $servicesformodal['SMId']; ?>"  role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <ul class="nav navbar-nav">
        <li><h4 class="modal-title"><?php echo $servicesformodal['SMName']; ?></h4></li>
<li>      <span style="font-size:18px;padding-left:90px;">Select Mode Of Work</span></li>
       <ul>
      </div>

      <!-- modal body -->

      <div class="modal-body">
      <div class="row">

            <?php if($servicesformodal['SMId']==1) {?>
            <div class="col-md-6">
        <div class="panel panel-default" style="padding:0px;">

            <div class="panel-heading">Book your service at a fixed rate card</div>
          <div class="panel-body" style="height:auto">
<table>
                  <li>There will be no inspection charge</li>
                <li>Background checked service professionals.</li>
                <li>Trained and experienced service workers.</li>
                <li>we will redo the work in case problem occurs with our service within a week.</li>
                <li>material cost is excluded from service charge.</li>
                <li>book our service and relax.</li>
</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==2) {?>
<div class="col-md-6">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading">Book your service at a fixed rate card</div>
          <div class="panel-body" style="height:auto">
            <table>
<li>2 There will be no inspection charge</li>
<li>Background checked service professionals.</li>
<li>Trained and experienced service workers.</li>
<li>we will redo the work in case problem occurs with our service within a week.</li>
<li>material cost is excluded from service charge.</li>
<li>book our service and relax.</li>
</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==3) {?>
<div class="col-md-6">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">CONTRACT BASIS (AFTER INSPECTION OF TOTAL WORK.)</div>
          <div class="panel-body" style="height:auto">
            <table>
<li>there will be no inspection charge</li>
<li>skilled and experienced service providers</li>
<li>material will be provided by contractors</li>
<li>painting material, colour, design etc</li> will be shown in catalog book</li>
<li>handover of work will be done only after verified by our service inspector</li>
<li>we will redo the work if problems occurs within a week</li>
<li> select your favourite design and colour code</li>

</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==4) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">CONTRACT BASIS (AFTER INSPECTION OF TOTAL WORK.)</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>  There will be no visiting or inspection charge</li>
<li>  ₹ 200 will be charged for first hour when project is undertaken (for repair works only)</li>
<li>  ₹ 100 will be charged after every successive hour of repair work</li>
<li>  Material cost is excluded</li>
<li>  We provide 30 days warrantee for our service</li>

</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==5) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">TERMS AND SCOPE OF SERVICE</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>  ₹ 99 is our inspection charge</li>
<li>  ₹ 150 will be charged for first hour of work</li>
<li>  Next 30 mins ₹ 50 will be charged and so on</li>
<li>  Material cost will be added along with service charge</li>
<li>  Time spent on procurement of material will considered in work time</li>


</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==6) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">TERMS AND SCOPE OF SERVICE</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>₹ 149 will be charged at the time of inspection</li>
<li>Inspection amount of ₹ 149 will be deducted from the final bill when work is completed</li>
<li>There are no hidden charges </li>
<li>experienced and skilled mechanics</li>


</table>

   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==7) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">TERMS AND SCOPE OF SERVICE</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>₹ 149 will be charged at the time of inspection</li>
<li>Inspection amount of ₹ 149 will be deducted from the final bill when work is completed</li>
<li>There are no hidden charges </li>
<li>experienced and skilled mechanics</li>


</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==8) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">TERMS AND SCOPE OF SERVICE</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>We have all type of floor installing experts on the topics of hardwood, laminate, ceramic tile, floating, engineered hardwood, vinyl sheet and carpet</li>
<li>We will provide a FREE FLOORING ESTIMATE: Our service partners will come to your property take measurements and discussion</li>
<li>We make you sure to provide most economic and best service in your location</li>
<li>Our workers make you sure to leave your property clean when the work is completed</li>
<li>₹200 is our visiting charge in case work not undertaken</li>
<li>We provide 1 month service guarantee
<li>Give us your requirements, mode of work and floor design</li>
<li>Floor design and type will be shown at the time of inspection</li>

</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php } else if($servicesformodal['SMId']==9) {?>
<div class="col-md-12">
        <div class="panel panel-default" style="padding:0px;">

<div class="panel-heading" style="font-size:13px;">TERMS AND SCOPE OF SERVICE</div>
          <div class="panel-body" style="height:auto:width:">
            <table>
<li>₹199 is our inspection charge only if work is not undertaken</li>
<li>Experienced and background checked service experts</li>
<li>Rough rate analysis will be done during inspection time
<li>Final bill will be done as per work and material cost</li>
<li>Design and metal selection will be done according to your requirement</li>
<li>Our experts will give you unique and economic metal design frame structure</li>
<li>Give us your requirement and relax</li>
</table>
   </div>
          <div class="modal-footer">
            <div id="buttons">
              <a href="<?php echo base_url(); ?>servicelist/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a></div>
          </div>
<?php }?>


        </div>
        </div>
        <!-- </div>    -->
        <div class="col-md-6">
        <div class="panel panel-default" style="padding:0px;">

                  <?php if($servicesformodal['SMId']==1) {?>
                  <div class="panel-heading">Book your service at hourly fixed rate
            </div>

            <div class="panel-body" style="height:auto">
              <table>

  <li> No visiting or inspection charge</li>
  <li> first hour of work we will charge ₹ 150</li>
  <li> Next 30 mins ₹ 75 will be charged and so on.</li>
  <li> Total time spent on service related issues will be included in the work hours.</li>
  <li> Material cost is excluded from service charge.</li>
  <li> We will redo the work if problem occurs within a week. </li>
              </table>       </div> <div class="modal-footer">
              <div id="buttons">

                <a href="<?php echo base_url(); ?>members/Members_index/usersform_view/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a>
              </div>
            </div>
              <?php } else if($servicesformodal['SMId']==2) {?>
              <div class="panel-heading">Book your service at hourly fixed rate
            </div>

            <div class="panel-body" style="height:auto">
              <table>
<li> No visiting or inspection charge</li>
<li>first hour of work we will charge ₹ 155</li>
<li>Next 30 mins ₹ 75 will be charged and so on.</li>
<li>Total time spent on service related issues will be included in the work hours.</li>
<li>We will redo the work if problem occurs within a week. </li>
</table>       </div> <div class="modal-footer">
              <div id="buttons">

                <a href="<?php echo base_url(); ?>members/Members_index/usersform_view/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a>
              </div>
            </div>
              <?php } else if($servicesformodal['SMId']==3) {?>
              <div class="panel-heading">AT A FIXED RATE (PER UNIT OF WORK.)
            </div>

            <div class="panel-body" style="height:auto">
              <table>
<li>There will be no inspection charge</li>
<li>material will be provided by client side</li>
<li>Background checked service professionals</li>
<li>Trained and experienced service workers</li>
<li>we will redo the work in case problem occurs with our service within a week</li>
<li>material cost is excluded from service charge</li>
<li>book our service and relax</li>
              </table>       </div> <div class="modal-footer">
              <div id="buttons">

              <a href="<?php echo base_url(); ?>members/Members_index/usersform_view/<?php echo $servicesformodal['SMId']; ?>" class="btn btn-default" id="select_button">SELECT</a>
              </div>
            </div>
              <?php }?>


        </div>
        </div>
        </div>
      </div>

         <!--  <div class="modal-footer" style="background-color:#39cccc">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> -->
    </div>
  </div>
</div>
 <?php }


  }?>
<?php $this->load->view("members/include/footer");?>
