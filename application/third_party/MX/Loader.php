<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library extends the CodeIgniter CI_Loader class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Loader.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Loader extends CI_Loader
{
	protected $_module;

	public $_ci_plugins = array();
	public $_ci_cached_vars = array();

	/** Initialize the loader variables **/
	public function initialize($controller = NULL)
	{
		/* set the module name */
		$this->_module = CI::$APP->router->fetch_module();

		if ($controller instanceof MX_Controller)
		{
			/* reference to the module controller */
			$this->controller = $controller;

			/* references to ci loader variables */
			foreach (get_class_vars('CI_Loader') as $var => $val)
			{
				if ($var != '_ci_ob_level')
				{
					$this->$var =& CI::$APP->load->$var;
				}
			}
		}
		else
		{
			parent::initialize();

			/* autoload module items */
			$this->_autoloader(array());
		}

		/* add this module path to the loader variables */
		$this->_add_module_paths($this->_module);
	}

	/** Add a module path loader variables **/
	public function _add_module_paths($module = '')
	{
		if (empty($module)) return;

		foreach (Modules::$locations as $location => $offset)
		{
			/* only add a module path if it exists */
			if (is_dir($module_path = $location.$module.'/') && ! in_array($module_path, $this->_ci_model_paths))
			{
				array_unshift($this->_ci_model_paths, $module_path);
			}
		}
	}

	/** Load a module config file **/
	public function config($file, $use_sections = FALSE, $fail_gracefully = FALSE)
	{
		return CI::$APP->config->load($file, $use_sections, $fail_gracefully, $this->_module);
	}

	/** Load the database drivers **/
	public function database($params = '', $return = FALSE, $query_builder = NULL)
	{
		if ($return === FALSE && $query_builder === NULL &&
			isset(CI::$APP->db) && is_object(CI::$APP->db) && ! empty(CI::$APP->db->conn_id))
		{
			return FALSE;
		}

		require_once BASEPATH.'database/DB'.EXT;

		if ($return === TRUE) return DB($params, $query_builder);

		CI::$APP->db = DB($params, $query_builder);

		return $this;
	}

	/** Load a module helper **/
	public function helper($helper = array())
	{
		if (is_array($helper)) return $this->helpers($helper);

		if (isset($this->_ci_helpers[$helper]))	return;

		list($path, $_helper) = Modules::find($helper.'_helper', $this->_module, 'helpers/');

		if ($path === FALSE) return parent::helper($helper);

		Modules::load_file($_helper, $path);
		$this->_ci_helpers[$_helper] = TRUE;
		return $this;
	}

	/** Load an array of helpers **/
	public function helpers($helpers = array())
	{
		foreach ($helpers as $_helper) $this->helper($_helper);
		return $this;
	}

	/** Load a module language file **/
	public function language($langfile, $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
	{
		CI::$APP->lang->load($langfile, $idiom, $return, $add_suffix, $alt_path, $this->_module);
		return $this;
	}

	public function languages($languages)
	{
		foreach($languages as $_language) $this->language($_language);
		return $this;
	}

	/** Load a module library **/
	public function library($library, $params = NULL, $object_name = NULL)
	{
		if (is_array($library)) return $this->libraries($library);

		$class = strtolower(basename($library));

		if (isset($this->_ci_classes[$class]) && $_alias = $this->_ci_classes[$class])
			return $this;

		($_alias = strtolower($object_name)) OR $_alias = $class;

		list($path, $_library) = Modules::find($library, $this->_module, 'libraries/');

		/* load library config file as params */
		if ($params == NULL)
		{
			list($path2, $file) = Modules::find($_alias, $this->_module, 'config/');
			($path2) && $params = Modules::load_file($file, $path2, 'config');
		}

		if ($path === FALSE)
		{
			$this->_ci_load_library($library, $params, $object_name);
		}
		else
		{
			Modules::load_file($_library, $path);

			$library = ucfirst($_library);
			CI::$APP->$_alias = new $library($params);

			$this->_ci_classes[$class] = $_alias;
		}
		return $this;
    }

	/** Load an array of libraries **/
	public function libraries($libraries)
	{
		foreach ($libraries as $library => $alias)
		{
			(is_int($library)) ? $this->library($alias) : $this->library($library, NULL, $alias);
		}
		return $this;
	}

	/** Load a module model **/
	public function model($model, $object_name = NULL, $connect = FALSE)
	{
		if (is_array($model)) return $this->models($model);

		($_alias = $object_name) OR $_alias = basename($model);

		if (in_array($_alias, $this->_ci_models, TRUE))
			return $this;

		/* check module */
		list($path, $_model) = Modules::find(strtolower($model), $this->_module, 'models/');

		if ($path == FALSE)
		{
			/* check application & packages */
			parent::model($model, $object_name, $connect);
		}
		else
		{
			class_exists('CI_Model', FALSE) OR load_class('Model', 'core');

			if ($connect !== FALSE && ! class_exists('CI_DB', FALSE))
			{
				if ($connect === TRUE) $connect = '';
				$this->database($connect, FALSE, TRUE);
			}

			Modules::load_file($_model, $path);

			$model = ucfirst($_model);
			CI::$APP->$_alias = new $model();

			$this->_ci_models[] = $_alias;
		}
		return $this;
	}

	/** Load an array of models **/
	public function models($models)
	{
		foreach ($models as $model => $alias)
		{
			(is_int($model)) ? $this->model($alias) : $this->model($model, $alias);
		}
		return $this;
	}

	/** Load a module controller **/
	public function module($module, $params = NULL)
	{
		if (is_array($module)) return $this->modules($module);

		$_alias = strtolower(basename($module));
		CI::$APP->$_alias = Modules::load(array($module => $params));
		return $this;
	}

	/** Load an array of controllers **/
	public function modules($modules)
	{
		foreach ($modules as $_module) $this->module($_module);
		return $this;
	}

	/** Load a module plugin **/
	public function plugin($plugin)
	{
		if (is_array($plugin)) return $this->plugins($plugin);

		if (isset($this->_ci_plugins[$plugin]))
			return $this;

		list($path, $_plugin) = Modules::find($plugin.'_pi', $this->_module, 'plugins/');

		if ($path === FALSE && ! is_file($_plugin = APPPATH.'plugins/'.$_plugin.EXT))
		{
			show_error("Unable to locate the plugin file: {$_plugin}");
		}

		Modules::load_file($_plugin, $path);
		$this->_ci_plugins[$plugin] = TRUE;
		return $this;
	}

	/** Load an array of plugins **/
	public function plugins($plugins)
	{
		foreach ($plugins as $_plugin) $this->plugin($_plugin);
		return $this;
	}

	/** Load a module view **/
	public function view($view, $vars = array(), $return = FALSE)
	{
		list($path, $_view) = Modules::find($view, $this->_module, 'views/');

		if ($path != FALSE)
		{
			$this->_ci_view_paths = array($path => TRUE) + $this->_ci_view_paths;
			$view = $_view;
		}

		return $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_prepare_view_vars($vars), '_ci_return' => $return));
		}

	protected function &_ci_get_component($component)
	{
		return CI::$APP->$component;
	}

	public function __get($class)
	{
		return (isset($this->controller)) ? $this->controller->$class : CI::$APP->$class;
	}

	public function _ci_load($_ci_data)
	{
		extract($_ci_data);

		if (isset($_ci_view))
		{
			$_ci_path = '';

			/* add file extension if not provided */
			$_ci_file = (pathinfo($_ci_view, PATHINFO_EXTENSION)) ? $_ci_view : $_ci_view.EXT;

			foreach ($this->_ci_view_paths as $path => $cascade)
			{
				if (file_exists($view = $path.$_ci_file))
				{
					$_ci_path = $view;
					break;
				}
				if ( ! $cascade) break;
			}
		}
		elseif (isset($_ci_path))
		{

			$_ci_file = basename($_ci_path);
			if( ! file_exists($_ci_path)) $_ci_path = '';
		}

		if (empty($_ci_path))
			show_error('Unable to load the requested file: '.$_ci_file);

		if (isset($_ci_vars))
			$this->_ci_cached_vars = array_merge($this->_ci_cached_vars, (array) $_ci_vars);

		extract($this->_ci_cached_vars);

		ob_start();

		if ((bool) @ini_get('short_open_tag') === FALSE && CI::$APP->config->item('rewrite_short_tags') == TRUE)
		{
			echo eval('?>'.preg_replace("/;*\s*\?>/", "; ?>", str_replace('<?=', '<?php echo ', file_get_contents($_ci_path))));
		}
		else
		{
			include($_ci_path);
		}

		log_message('debug', 'File loaded: '.$_ci_path);

		if ($_ci_return == TRUE) return ob_get_clean();

		if (ob_get_level() > $this->_ci_ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			CI::$APP->output->append_output(ob_get_clean());
		}
	}

	/** Autoload module items **/
	public function _autoloader($autoload)
	{
		$path = FALSE;

		if ($this->_module)
		{
			list($path, $file) = Modules::find('constants', $this->_module, 'config/');

			/* module constants file */
			if ($path != FALSE)
			{
				include_once $path.$file.EXT;
			}

			list($path, $file) = Modules::find('autoload', $this->_module, 'config/');

			/* module autoload file */
			if ($path != FALSE)
			{
				$autoload = array_merge(Modules::load_file($file, $path, 'autoload'), $autoload);
			}
		}

		/* nothing to do */
		if (count($autoload) == 0) return;

		/* autoload package paths */
		if (isset($autoload['packages']))
		{
			foreach ($autoload['packages'] as $package_path)
			{
				$this->add_package_path($package_path);
			}
		}

		/* autoload config */
		if (isset($autoload['config']))
		{
			foreach ($autoload['config'] as $config)
			{
				$this->config($config);
			}
		}

		/* autoload helpers, plugins, languages */
		foreach (array('helper', 'plugin', 'language') as $type)
		{
			if (isset($autoload[$type]))
			{
				foreach ($autoload[$type] as $item)
				{
					$this->$type($item);
				}
			}
		}

		// Autoload drivers
		if (isset($autoload['drivers']))
		{
		    foreach ($autoload['drivers'] as $item => $alias)
		    {
		        (is_int($item)) ? $this->driver($alias) : $this->driver($item, $alias);
		    }
		}

		/* autoload database & libraries */
		if (isset($autoload['libraries']))
		{
			if (in_array('database', $autoload['libraries']))
			{
				/* autoload database */
				if ( ! $db = CI::$APP->config->item('database'))
				{
					$this->database();
					$autoload['libraries'] = array_diff($autoload['libraries'], array('database'));
				}
			}

			/* autoload libraries */
			foreach ($autoload['libraries'] as $library => $alias)
			{
				(is_int($library)) ? $this->library($alias) : $this->library($library, NULL, $alias);
			}
		}

		/* autoload models */
		if (isset($autoload['model']))
		{
			foreach ($autoload['model'] as $model => $alias)
			{
				(is_int($model)) ? $this->model($alias) : $this->model($model, $alias);
			}
		}

		/* autoload module controllers */
		if (isset($autoload['modules']))
		{
			foreach ($autoload['modules'] as $controller)
			{
				($controller != $this->_module) && $this->module($controller);
			}
		}
	}

    /* Admin View Function Starts */
    public function admin_view($view,$admin_data = array()){
         $admin_data['admin_view'] = $view;
				 $student_data = $this->session->userdata('student_data');

				 $admission_id = preg_replace('/[^0-9]/', '', $_SERVER['REQUEST_URI']);

        if($admission_id == $this->session->userdata('student_data')[0]['ADMISSION_ID'])
         {
             $this->load->view('studentdetails/student_base_template',$admin_data);
         }
         else{
             redirect(base_url('logout'));
         }
    }
    /* Admin View Function Ends */

		/* encryption and decryption function starts*/
		public function encrypt_decrypt_data($value,$type){
			// $plaintext = 'shivshekhar@intermind.in';

			//Password must be of 32 bytes (256 bit)//*********This has to match with the Android secretKey
			$password = 'Wf7brMdmIDSjb3cLaQIezuLqkUBOnHKB';

			// IV must be exact 16 chars (128 bit)//*********This has to match with the Android iv
			$iv = "0123456789SUNTEC";

			// CBC has an IV and thus needs randomness every time a message is encrypted
			$method = 'aes-256-cbc';

			// Must be exact 32 chars (256 bit)
			//$key = substr(hash('sha256', $password, true), 0, 32);

			// $key = $password;
			// echo "Password:" . $password . "<br/>";

			//****** This is the method to encrypt the data useign AES 256 Bit Algorithm
			if($type == "encrypt"){
				return base64_encode(openssl_encrypt($value, $method, $password, OPENSSL_RAW_DATA, $iv));
			}
			else{
				//****** This is the method to decrypt the data useign AES 256 Bit Algorithm
				return openssl_decrypt(base64_decode($value), $method, $password, OPENSSL_RAW_DATA, $iv);
			}
		}
		/* encryption and decryption function Ends*/

		/* Phpmailer Customize Function Starts */
    public function mailer($from,$to,$cc,$bcc,$attachments,$subject,$body,$type){

    	$username="";
    	$password="";
    	if($type == 'teleenquiry'){
    		$username="emis@saintangelos.com";
    		$password="emis@123";
    	}
    	else if($type == 'enquiry'){
    		$username="emis@saintangelos.com";
    		$password="emis@123";
    	}
    	else if($type == 'admission'){
    		$username="emis@saintangelos.com";
    		$password="emis@123";
    	}
    	else if($type == 'studentdesk'){
    		$username="studentdesk@saintangelos.com";
    		$password="studentdesk@123";
    	}

		$this->load->library('My_PHPMailer');
   		$mail = new PHPMailer();

   		$mail->isSMTP();                  // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;           // Enable SMTP authentication
		$mail->Username = $username;// SMTP username
		$mail->Password = $password;   // SMTP password
		$mail->SMTPSecure = 'tls';// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;  // TCP port to connect to

		$mail->setFrom($from, 'Saintangelos Notification');

		if(is_array($to)){
			foreach ($to as $recepient) {
				$mail->addAddress($recepient);     // for multiple recipient
			}
		}
		else{
			$mail->addAddress($to);     // for single recipient
		}

		if(!empty($cc)){
			if(is_array($cc)){
				foreach ($cc as $ccrecepient) {
					$mail->addCC($ccrecepient);     // for multiple cc recipient
				}
			}
			else{
				$mail->addCC($cc);			// for single cc recipient
			}
		}

		if(!empty($bcc)){
			if(is_array($bcc)){
				foreach ($bcc as $bccrecepient) {
					$mail->addBCC($bccrecepient);   // for multiple bcc recipient
				}
			}
			else{
				$mail->addBCC($bcc);		// for single bcc recipient
			}
		}

		if(!empty($attachments)){
			if(is_array($attachments)){
				foreach ($attachments as $fileattached) {
					$mail->AddStringAttachment($fileattached['file'],$fileattached['name']);
				}
			}
			else{
				$mail->addAttachment($attachments);
			}
		}

		$mail->Subject = $subject;

		if(preg_match("/([\<])([^\>]{1,})*([\>])/i", $body )){
			$mail->isHTML(true);
			$mail->Body = $body;
		}
		else{
			$mail->Body = $body;
		}


		if($mail->send()){
			return 1;
		}
		else {
			// return $mail->ErrorInfo;   for getting error while mail is sent or not
			return 0;
		}

    }
    /* Phpmailer Customize Function Ends */

    /* Smscountry Customize Function Starts  */
    public function sendSms($mobile_no,$message,$cc,$to_type) {
		//Please Enter Your Details
		$user="st.angelo"; //your username
		$password="stangelo1231"; //your password

		$this->load->model('tele_enquiry_model');

		$mobilenumbers= $mobile_no; //enter Mobile numbers comma seperated
		$message = $message; //enter Your Message

		$this->tele_enquiry_model->insert_into_sms_log($mobilenumbers,$message,$cc,$to_type);

		$senderid="SMSCountry"; //Your senderid
		$messagetype="N"; //Type Of Your Message
		$DReports="Y"; //Delivery Reports
		$url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
		$message = urlencode($message);
		$ch = curl_init();
		if (!$ch){die("Couldn't initialize a cURL handle");}
		$ret = curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,
		"User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
		$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//If you are behind proxy then please uncomment below line and provide your proxy ip with port.
		// $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
		$curlresponse = curl_exec($ch); // execute
		if(curl_errno($ch))
		echo 'curl error : '. curl_error($ch);
		if (empty($ret)) {
		// some kind of an error happened
		die(curl_error($ch));
		curl_close($ch); // close cURL handler
		} else {
		$info = curl_getinfo($ch);
		curl_close($ch); // close cURL handler
		return $curlresponse; //echo "Message Sent Succesfully" ;
		}

    }
    /* Smscounrty Function Ends */
    /* Phpmailer Customize Function Ends */

    public function GridColumnActionsAccessibility(){
    	$EmployeesWithGridColumnDeletionAccessible = array('3170');
    	$EmployeesWithGridColumnUpdationAccessible = array('3170','1233');
    	$EnquiryFollowupWithGridColumnUpdationAccessible = array('3170');
    	$ConvertToEnquiryConversionAccess = array('2770','3170');
    	$ConvertToEnquiryConversionAccessByRoleId = array('35','36','37','38','39','40','41','42');
    	$BatchActionsAccessiblesEmployeeIDs = array('3170','1338');

    	$admin_cred = $this->session->userdata('admin_data')[0];
    	$Grid_Column_Actions_Permissions = array(
    		'IsAdmissionDeletionPermitted' => false,
    		'IsAdmissionUpdationPermitted' => false,
    		'IsEnquiryDeletionPermitted'  =>  false,
    		'IsEnquiryUpdationPermitted'  =>  false,
    		'IsFollowupDetailsDeletionPermitted'  =>  false,
    		'IsConvertToEnquiryAndWalkinPermitted'  =>  false,
    		'ConvertToEnquiryConversionAccessByRoleId'  =>  false,
    		'IsBatchActionsPermitted'  =>  false
    		);

    	if(in_array($admin_cred['EMPLOYEE_ID'], $EmployeesWithGridColumnDeletionAccessible)){
			$Grid_Column_Actions_Permissions['IsAdmissionDeletionPermitted'] = true;
		}

		if(in_array($admin_cred['EMPLOYEE_ID'], $EmployeesWithGridColumnUpdationAccessible)){
			$Grid_Column_Actions_Permissions['IsAdmissionUpdationPermitted'] = true;
		}

		if(in_array($admin_cred['EMPLOYEE_ID'], $EnquiryFollowupWithGridColumnUpdationAccessible)){
			$Grid_Column_Actions_Permissions['IsFollowupDetailsDeletionPermitted'] = true;
		}

		if(in_array($admin_cred['EMPLOYEE_ID'], $ConvertToEnquiryConversionAccess)){
			$Grid_Column_Actions_Permissions['IsConvertToEnquiryAndWalkinPermitted'] = true;
		}

		if(in_array($admin_cred['ROLE_ID'], $ConvertToEnquiryConversionAccessByRoleId)){
			$Grid_Column_Actions_Permissions['ConvertToEnquiryConversionAccessByRoleId'] = true;
		}

		if(in_array($admin_cred['EMPLOYEE_ID'], $BatchActionsAccessiblesEmployeeIDs)){
			$Grid_Column_Actions_Permissions['IsBatchActionsPermitted'] = true;
		}

		foreach ($Grid_Column_Actions_Permissions as $Grid_Column_Actions_Permissions_Key => $Grid_Column_Actions_Permissions_Value) {

			define($Grid_Column_Actions_Permissions_Key, $Grid_Column_Actions_Permissions_Value);
		}
		// define("GREETING", "Welcome to W3Schools.com!");
		return $Grid_Column_Actions_Permissions;
    }
}


/** load the CI class for Modular Separation **/
(class_exists('CI', FALSE)) OR require dirname(__FILE__).'/Ci.php';
