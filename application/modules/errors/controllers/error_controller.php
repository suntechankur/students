<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_controller extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function page_not_found()
  	{
  		$this->load->view('page_not_found');
  	}

    public function access_denied()
  	{
  		$this->load->view('access_denied');
  	}

}
?>
