<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public $exam_db;
    public function __construct() {
        $this->exam_db = $this->load->database('exam',TRUE);
        parent::__construct();
    }

    public function check_user_exist($username){
        $query = $this->db->select('USERNAME')
                 ->from('student_desk_users')
                 ->where('USERNAME',$username)
                 ->where('IS_ACTIVE',"1")
                 ->get();

          $count = $query->num_rows();
        return $count;
    }

    public function give_access_to_student($username,$password){
        $password = $this->load->encrypt_decrypt_data($password,"encrypt");
        $query = $this->db->select('SU.ADMISSION_ID,AM.IS_STUDENT_POLICY_ACCEPTED,concat(ENQUIRY_FIRSTNAME," ",ENQUIRY_LASTNAME) as student_name,AM.STREAM,EM.ENQUIRY_EMAIL')
                           ->from('student_desk_users SU')
                           ->join('admission_master AM','AM.ADMISSION_ID=SU.ADMISSION_ID','right')
                           ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','right')
                           ->where('SU.USERNAME',$username)
                           ->where('SU.PASSWORD',$password)
                           ->get();
        return $query->result_array();
    }

    public function update_policy_details($admission_id){
      $policy = array('IS_STUDENT_POLICY_ACCEPTED' => "1");
      $this->db->set($policy)
               ->where('ADMISSION_ID',$admission_id)
               ->update('admission_master');
      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }
}
