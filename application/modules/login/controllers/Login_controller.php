<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends MX_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->clear_cache();
            $this->load->model('login_model');
    }


    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    public function admin_credentials(){

        if(isset($_POST['student-login'])){
          $username = $this->input->post('username');
          $password = $this->input->post('password');
          $this->form_validation->set_rules('username', 'Username', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');

          if ($this->form_validation->run() == TRUE){
            $this->validate_student_credentials($username,$password);
          }
          else{
            echo "<script>alert('Please Fill All Valid Credentials.');</script>";
          }
        }
        $this->load->view('online_login');
    }

    public function validate_captcha(){
        if($this->input->post('captcha') != $this->session->userdata['captcha'])
        {
            $this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
            return false;
        }else{
            return true;
        }
    }

    public function validate_student_credentials($username,$password){
        $user_exist = $this->login_model->check_user_exist($username);
        if($user_exist){
          $data = array();
          $data = $this->login_model->give_access_to_student($username,$password);
          if(empty($data)){
            echo "<script>alert('Password is not valid.');</script>";
          }
          else if($username == $data[0]['ADMISSION_ID']){
            $this->session->set_userdata('student_data', $data);
            $admission_id = $data[0]['ADMISSION_ID'];
            //if($data[0]['IS_STUDENT_POLICY_ACCEPTED'] == "1"){
              redirect(base_url('student_details/'.$admission_id.'/ds'));
            //}
            //else{
            //  redirect(base_url('student_instructions/'.$admission_id.'/si/0'));
            //}
          }
        }
        else{
          echo "<script>alert('User doesn't exist.');</script>";
        }
    }

    public function logout(){
        $this->session->unset_userdata('student_data');
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
