<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_jobs_controller extends MX_Controller {

    public function __construct()
    {
        $this->load->model('cron_jobs_model');
        parent::__construct();
    }

  public function today_birthdays()
	{
    $get_employee_details = $this->cron_jobs_model->todays_bithday_list_of_employees();
    echo "<pre>";
    print_r($get_employee_details);
    echo "</pre>";

    foreach($get_employee_details as $employee){
      $from = "hr.admin@saintangelos.com";
      $toemail = "";
      if($employee['EMP_OFFICIAL_EMAIL'] == ""){
        $toemail = 'hrd@saintangelos.com';
      }
      else{
        $toemail = $employee['EMP_OFFICIAL_EMAIL'];
      }
      $to = $toemail;
      $bcc = array('centres@saintangelos.com','agnelorajesh@savvglobal.com');

      $ran = array('Blackadder ITC','Viner Hand ITC','Algerian','Tempus Sans ITC','Blackadder ITC');
      $font_name = $ran[array_rand($ran, 1)];

      // $to = 'ankurprajapati66@gmail.com';
      // $bcc = array('ankur.prajapati@saintangelos.com');

      date_default_timezone_set('Asia/Kolkata');
      $today_date = date('d-m-Y');
      $subject = "Wish you Many Many Happy Returns of The Day to ".$employee['EMPLOYEE_NAME'];
      $body = '<table style="background-image:http://www.saintangelos.com/emis/IMAGES/imagesCA798H16.jpg" width="1200">
                <tbody>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="6" face="'.$font_name.'" color="navy"><b>Date : '.$today_date.'</b></font>
                      <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left"><font size="6" face="'.$font_name.'" color="red"><b>Dear '.ucwords($employee['EMPLOYEE_NAME']).',</b></font></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="6" face="'.$font_name.'" color="green"><b>('.$employee['DESIGNATION'].' - '.$employee['CENTRE_NAME'].')</b></font>
                      <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#490062"><b>On your birthday we wish you much pleasure and joy, </b></font></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#490062"><b>We hope all your wishes come true.</b></font></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#490062"><b>May each hour and minute be filled with delight,</b></font></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#490062"><b>And your birthday be perfect for you!!!</b></font>
                    <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#590000"><b>And your birthday be perfect for you!!!</b></font>
                    <br><br><br><br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#590000"><b>A simple celebration, a gathering of friends; here is wishing you great happiness,</b></font>
                    <br><br><br><br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><font size="6" face="'.$font_name.'" color="#590000"><b>a joy that never ends.</b></font>
                    <br><br><br><br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td width="250"><img src="'.base_url('resources/images/').'roses.gif"></td>
                    <td width="700" align="center"><font size="10" face="'.$font_name.'" color="red"><b>Happy Birthday</b></font></td>
                    <td width="250"><img src="'.base_url('resources/images/').'roses.gif"></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center"><img src="'.base_url('resources/images/').'cake-birthday.gif" alt="image1"></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left"><font size="5" face="'.$font_name.'"><b>From,<br><br>St.Angelos Family !!!</b></font></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left"><b>St. Angelos Professional Education now offers 3 years Govt. recog. University Degree programs in three streams of IT-<br>B.Sc. in Visual Media (Animation)<br>B.Sc. in System &amp; Network Administration<br>BCA in Web &amp; Software<br>Visit : Career Counseling-English: www.youtube.com/saintangelosmedia<br>Call us on 986 786 4444 / 932 003 6666 or<br>enquiry@(saintangelos.com)<br>www.saintangelos.com.<br>www.facebook.com/StAngelos,<br>www.twitter.com/StAngelos,<br>www.blog.saintangelos.com. </b>
                    </td>
                  </tr>
                </tbody>
              </table>';


      $confirmationMail = $this->load->mailer($from,$to,"",$bcc,"",$subject,$body,"birthday_or_applaud_or_birthday_list");

      if($confirmationMail == 0){
        $new_subject = "Birth day mail failure ".$subject;
        $this->load->mailer($from,"ankur.prajapati@saintangelos.com","ankurprajapati66@gmail.com","","",$new_subject,$body,"birthday_or_applaud_or_birthday_list");
      }

    }
	}

  public function centre_mails(){
    $get_centre_mail_ids = $this->cron_jobs_model->get_mail_ids("154");
    $new_array = array('ankur@gmail.com','ap@yahoo.com');
    $third_array = array_merge($get_centre_mail_ids,$new_array);
    echo '<pre>';
    print_r($get_centre_mail_ids);
    print_r($new_array);
    print_r($third_array);
    echo '</pre>';
  }

  public function today_applauds(){

    $get_employee_details = $this->cron_jobs_model->todays_applaud_of_employees();
    echo "<pre>";
    print_r($get_employee_details);
    echo "</pre>";
    foreach($get_employee_details as $employee){
      $from = "hr.admin@saintangelos.com";
      $toemail = "";
      if($employee['EMP_OFFICIAL_EMAIL'] == ""){
        $toemail = 'hrd@saintangelos.com';
      }
      else{
       $toemail = $employee['EMP_OFFICIAL_EMAIL'];
      }

      $to = $toemail;
      $bcc = array('centres@saintangelos.com','agnelorajesh@savvglobal.com');


      // $to = 'ankurprajapati66@gmail.com';
      // $bcc = array('ankur.prajapati@saintangelos.com');

      //date_default_timezone_set('Asia/Kolkata');
      //$today_date = date('d-m-Y');

      $year = "Year";
      if($employee['YEAR'] > 1){
        $year = "Years";
      }
      $subject = "Applaud for completing " .$employee['YEAR']." ".$year." - " .$employee['EMPLOYEE_NAME'];
      $body = '<table width="1200">
                <tbody>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4"><b>Name &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : '.$employee['EMPLOYEE_NAME'].' </b></font>
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4"><b>Designation : '.$employee['CONTROLFILE_VALUE'].' </b></font>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4"><b>Center &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; : '.$employee['CENTRE_NAME'].' </b></font>
                      <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4"><b>Dear '.$employee['EMP_FNAME'].',</b></font>
                      <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4">We take this cheerful moment to congratulate you for all your tremendous support<br> and co-operation towards the development of the Company and being with us for<br> '.$employee['YEAR']." ".$year.'.</font>
                      <br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      <font size="4">We want to commend on your efforts and the scale of commitment you have<br> maintained with us which has taken us to greater heights and has made us feel
                      <br>proud for having capable members like you as part of our team. Keep on<br> contributing the companys growth in the future too. We are looking forward to see<br> that this growing relationship is mutually beneficial.</font>
                      <br><br><br><br>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="3" align="left"><font size="4">We wish you all the very best for the accomplishments in the future.</font><br><br><br><br></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left"><font size="4">Thanks & Regards,</font><br><br></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left"><font size="4">HR DEPT.</font>
                    <br><br><br>
                    </td>
                  </tr>
                </tbody>
              </table>';

      $confirmationMail = $this->load->mailer($from,$to,"",$bcc,"",$subject,$body,"birthday_or_applaud_or_birthday_list");

      if($confirmationMail == 0){
        $new_subject = "Birth day mail failure ".$subject;
        $this->load->mailer($from,"ankur.prajapati@saintangelos.com","ankurprajapati66@gmail.com","","",$new_subject,$body,"birthday_or_applaud_or_birthday_list");
      }

    }

  }

  public function monthly_employees_birthday_list(){

    $get_employee_details = $this->cron_jobs_model->birthday_list_of_employees();
    echo "<pre>";
    print_r($get_employee_details);
    echo "</pre>";

      $from = "hr.admin@saintangelos.com";

      $to = 'hrd@saintangelos.com';
      $bcc = array('centres@saintangelos.com','agnelorajesh@savvglobal.com');


      // $to = 'ankurprajapati66@gmail.com';
      // $bcc = array('ankur.prajapati@saintangelos.com');

      $count = 1;

      $subject = "Birthday list for the Month of ".$get_employee_details[0]['month']. " ".$get_employee_details[0]['year'];
      $body = '<table>
                <tbody>
                  <tr>
                    <td colspan="3" align="left">
                      Dear All,
                      <br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      Greetings !!!!
                      <br><br>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="left">
                      Kindly find the below details for <b>Birthday List</b> for the Month of <b> '.$get_employee_details[0]['month'].'</b> '.$get_employee_details[0]['year'].' for Your reference.
                      <br><br><br>
                    </td>
                  </tr>
                </tbody>
                </table>';
                $body = $body."<table border=1 cellpadding=0 cellspacing=0 width=60%  text-align=center>";
                $body = $body."<tr>";
                $body = $body."<th style='padding:4px 8px'>Sr.No.</th>";
                $body = $body."<th style='padding:4px 8px'>Date of birth</th>";
                $body = $body."<th style='padding:4px 8px'>Name</th>";
                $body = $body."<th style='padding:4px 8px'>Designation</th>";
                $body = $body."<th style='padding:4px 8px'>Centre</th>";
                $body = $body."</tr>";
                foreach($get_employee_details as $employee){

                  $body = $body."<tr>";
                  $body = $body."<td align=center style='padding:4px 8px'>" .$count. "</td> ";
                  $body = $body."<td align=center style='padding:4px 8px'>" .$employee['DATEOFBIRTH']." ".$employee['year']."</td> ";
                  $body = $body."<td align=center style='padding:4px 8px'>" .$employee['EMPLOYEE_NAME']. "</td> ";
                  $body = $body."<td align=center style='padding:4px 8px'>" .$employee['CONTROLFILE_VALUE']. "</td> ";
                  $body = $body."<td align=center style='padding:4px 8px'>" .$employee['CENTRE_NAME']. "</td> ";
                  $body = $body."</tr>";
                  $count++;
                }
                  $body = $body."</table>";

      $confirmationMail = $this->load->mailer($from,$to,"",$bcc,"",$subject,$body,"birthday_or_applaud_or_birthday_list");

      if($confirmationMail == 0){
        $new_subject = "Birth day mail failure ".$subject;
        $this->load->mailer($from,"ankur.prajapati@saintangelos.com","ankurprajapati66@gmail.com","","",$new_subject,$body,"birthday_or_applaud_or_birthday_list");
      }

  }

}
?>
