<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('resources/login/'); ?>css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-ui.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    padding-top: 10px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: red;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #F00;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #F00;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #FE3B3B;
	border-color: #FE3B3B;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.submit_test{
  border-radius: 10px;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
</style>

<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height:50px;"><br/></div><br/>
            <div class="row">
							<div class="col-xs-3">
								<a href="javascript:void(0)" id="download-exam-list-link">Download Exam List</a>
							</div>
							<div class="col-xs-3">
								<a href="javascript:void(0)" id="upload-exam-link">Upload Exam Data</a>
							</div>
							<div class="col-xs-2">
								<a href="javascript:void(0)" id="exam-list-link">Exam List</a>
							</div>
              <?php if($this->session->userdata('admin_details')[0]['role'] == 'admin'){ ?>
                <div class="col-xs-3">
  								<a href="javascript:void(0)" id="download-question-paper-data-link">Download Exam Backup</a>
  							</div>
              <?php } ?>
							<div class="col-xs-1">
								<a href="<?php echo  base_url('admin_logout');?>">Logout</a>
							</div>
						</div>
            <hr>
          </div>
					<div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <div id="admin-form" style="display: block;">
                  <label>Welcome Admin</label>
                </div>
                <div id="download-exam-list-form" style="display: none;">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  									<div class="form-group">
  										<input type="text" name="username" id="username-download-exam" tabindex="1" class="form-control" placeholder="Username" value="">
  									</div>
  									<div class="form-group">
  										<input type="password" name="password" id="password-download-exam" tabindex="2" class="form-control" placeholder="Password">
  									</div>
  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="admin-login" id="download-exam-list" tabindex="4" class="form-control btn btn-login" value="Download List">
  											</div>
  										</div>
  									</div>
                  </div>
                  <div class="col-sm-4"></div>
                  <!-- below the button -->
                  <div class="col-lg-12" id="download_exam_data" class="table-responsive">
                  </div>
                </div>
                <div id="upload-exam-data-form" style="display: none;">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  									<div class="form-group">
  										<input type="text" name="username" id="username-upload-exam" tabindex="1" class="form-control" placeholder="Username" value="">
  									</div>
  									<div class="form-group">
  										<input type="password" name="password" id="password-upload-exam" tabindex="2" class="form-control" placeholder="Password">
  									</div>
  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="admin-login" id="upload-exam-data" tabindex="4" class="form-control btn btn-login" value="Upload Data">
  											</div>
  										</div>
  									</div>
                  </div>
                  <div class="col-sm-4"></div>
                  <!-- below the button -->
                  <div class="col-lg-12" id="upload_exam_data" class="table-responsive">
                  </div>
                </div>
                <div id="exam-list-form" style="display: none;">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  									<div class="form-group">
  										<input type="text" name="studentname" id="studentname-exam-list" tabindex="1" class="form-control" placeholder="Student Name" value="">
  									</div>
  									<div class="col-sm-6 form-group">
  										<input type="text" name="fromdate" id="fromdate-exam-list" tabindex="1" class="form-control" placeholder="From Date" value="">
  									</div>
  									<div class="col-sm-6 form-group">
  										<input type="text" name="todate" id="todate-exam-list" tabindex="1" class="form-control" placeholder="To Date" value="">
  									</div>
  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="admin-login" id="get-exam-list" tabindex="4" class="form-control btn btn-login" value="Get List">
  											</div>
  										</div>
  									</div>
                  </div>
                  <div class="col-sm-4"></div>
                  <!-- below the button -->
                  <div class="col-lg-12" id="view_exam_data" class="table-responsive"></div>
                </div>
                <div id="download-exam-paper-form" style="display: none;">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="form-group">
  										<input type="text" name="studentname" id="studentname-question-paper" tabindex="1" class="form-control" placeholder="Student Name" value="">
  									</div>
  									<div class="col-sm-6 form-group">
  										<input type="text" name="fromdate" id="fromdate-question-paper" tabindex="1" class="form-control" placeholder="From Date" value="">
  									</div>
  									<div class="col-sm-6 form-group">
  										<input type="text" name="todate" id="todate-question-paper" tabindex="1" class="form-control" placeholder="To Date" value="">
  									</div>
  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="admin-login" id="get-question-paper" tabindex="4" class="form-control btn btn-login" value="Get Paper Data">
  											</div>
  										</div>
  									</div>
                  </div>
                  <div class="col-sm-4"></div>
                  <!-- below the button -->
                  <div class="col-lg-12" id="view_question_paper_details" class="table-responsive"></div>
                </div>
              </div>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <script>
  $('document').ready(function(){

    $("#fromdate-exam-list,#todate-exam-list,#fromdate-question-paper,#todate-question-paper").datepicker({
        dateFormat: 'dd/mm/yy'
    });

    // tabs clicking function
    $('#download-exam-list-link').click(function(e) {
      $("#download-exam-list-form").delay(100).fadeIn(100);
      $("#admin-form").fadeOut(100);
      $("#upload-exam-data-form").fadeOut(100);
      $("#exam-list-form").fadeOut(100);
      $("#download-exam-paper-form").fadeOut(100);
      $('#upload-exam-link').removeClass('active');
      $('#exam-list-link').removeClass('active');
      $('#download-question-paper-data-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });
    $('#upload-exam-link').click(function(e) {
      $("#upload-exam-data-form").delay(100).fadeIn(100);
      $("#admin-form").fadeOut(100);
      $("#download-exam-list-form").fadeOut(100);
      $("#exam-list-form").fadeOut(100);
      $("#download-exam-paper-form").fadeOut(100);
      $('#download-exam-list-link').removeClass('active');
      $('#exam-list-link').removeClass('active');
      $('#download-question-paper-data-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });
    $('#exam-list-link').click(function(e) {
      $("#exam-list-form").delay(100).fadeIn(100);
      $("#admin-form").fadeOut(100);
      $("#download-exam-list-form").fadeOut(100);
      $("#upload-exam-data-form").fadeOut(100);
      $("#download-exam-paper-form").fadeOut(100);
      $('#download-exam-list-link').removeClass('active');
      $('#upload-exam-link').removeClass('active');
      $('#download-question-paper-data-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });
    $('#download-question-paper-data-link').click(function(e) {
      $("#download-exam-paper-form").delay(100).fadeIn(100);
      $("#admin-form").fadeOut(100);
      $("#download-exam-list-form").fadeOut(100);
      $("#upload-exam-data-form").fadeOut(100);
      $("#exam-list-form").fadeOut(100);
      $('#download-exam-list-link').removeClass('active');
      $('#upload-exam-link').removeClass('active');
      $('#exam-list-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });
    // end of tabs clicking function

    // for getting csrf token for doing form actions
    function csrfTkn(){
      var examToken = $('.adminToken').val();
      return examToken;
    }
    // end of csrf token functions

    // setting a base url for ajax call
    function base_url(page = ''){
      if (window.location.origin == 'http://localhost' || window.location.origin == 'http://localhost:'+location.port) {
        return window.location.origin + '/student_desk/' + page;
      }
      else{
        return window.location.origin + '/student_desk/' + page;
      }
    }
    // base url function ends

    $("#download-exam-list").click(function(){
      var username = $("#username-download-exam").val();
      var password = $("#password-download-exam").val();
      validate_authentication(username,password,"download_list");
    });

    $("#upload-exam-data").click(function(){
      var username = $("#username-upload-exam").val();
      var password = $("#password-upload-exam").val();
      validate_authentication(username,password,"upload_list");
    });

    $("#get-exam-list").click(function(){
      var studentname = $("#studentname-exam-list").val();
      var fromdate = $("#fromdate-exam-list").val();
      var todate = $("#todate-exam-list").val();
      get_exam_list_and_paper_details(studentname,fromdate,todate,"exam_list");
    });

    $("#get-question-paper").click(function(){
      var studentname = $("#studentname-question-paper").val();
      var fromdate = $("#fromdate-question-paper").val();
      var todate = $("#todate-question-paper").val();
      get_exam_list_and_paper_details(studentname,fromdate,todate,"question_paper_backup");
    });

    function validate_authentication(username,password,type){
      $.ajax({
         dataType: "json",
         url: base_url('get_required_details_and_authenticate/'+type),
         type: "post",
         data: { username: username,
                 password: password,
                 onlineexam_token: csrfTkn()},
         success: function(result){
           $('.adminToken').val(result.csrf_token);
           if(result.num == "1"){
             alert("Check username is correct or not");
           }
           else if(result.num == "2"){
             alert("Check password is correct or not");
           }
           else if(result.num == "3"){
             alert("Check user credential is correct or not");
           }
           else if(result.num == "4"){
             if(result.type == "download_list"){
               $("#download_exam_data").empty();
               if(result.message == ""){
                 $("#download_exam_data").append("<table class='table table-bordered table-hover table-condensed' style='font-size:12px;'><thead><tr><th>Centre</th><th>Admission Id</th><th>Exam Id</th><th>Name</th><th>Subject</th><th>Date</th><th>Time</th><th>Internal Marks</th></tr></thead><tbody id='todays_exam'></tbody></table>");
                 var i = 0;
                 for(i=0;i<result.exam_data.length;i++){
                   if(result.exam_data[i].ExamGiven){
                     var color = "#0ba52c6e";
                   }
                   else{
                     var color = "#ff00006e";
                   }
                   $("#todays_exam").append("<tr style='background-color:"+color+"'><td>"+result.exam_data[i].Centre_Name+"</td><td>"+result.exam_data[i].AdmissionID+"</td><td>"+result.exam_data[i].ExamID+"</td><td>"+result.exam_data[i].StudentName+"</td><td>"+result.exam_data[i].Course_Name+"</td><td>"+result.exam_data[i].Date+"</td><td>"+result.exam_data[i].Time+"</td><td>"+result.exam_data[i].InternalExamMarks+"/"+result.exam_data[i].InternalOutOf+"</td></tr>");
                 }
               }
               else{
                 $("#download_exam_data").append("<label>"+result.message+"</label>");
               }
               $("#download-exam-list-form").find("#username-download-exam").val("");
               $("#download-exam-list-form").find("#password-download-exam").val("");
             }
             else if(result.type == "upload_list"){
               $("#upload_exam_data").empty();
               if(result.message == ""){
                 $("#upload_exam_data").append("<table class='table table-bordered table-hover table-condensed' style='font-size:12px;'><thead><tr><th>Centre</th><th>Admission Id</th><th>Exam Id</th><th>Name</th><th>Subject</th><th>Date</th><th>Time</th><th>Internal Marks</th><th>Online Marks</th></tr></thead><tbody id='todays_exam'></tbody></table>");
                 var i = 0;
                 for(i=0;i<result.exam_data.length;i++){

                   $("#todays_exam").append("<tr><td>"+result.exam_data[i].Centre_Name+"</td><td>"+result.exam_data[i].AdmissionID+"</td><td>"+result.exam_data[i].ExamID+"</td><td>"+result.exam_data[i].StudentName+"</td><td>"+result.exam_data[i].Course_Name+"</td><td>"+result.exam_data[i].Date+"</td><td>"+result.exam_data[i].Time+"</td><td>"+result.exam_data[i].InternalExamMarks+"/"+result.exam_data[i].InternalOutOf+"</td><td>"+result.exam_data[i].Marks+"/"+result.exam_data[i].outof+"</td></tr>");
                 }
               }
               else{
                 $("#upload_exam_data").append("<label>"+result.message+"</label>");
               }
               $("#upload-exam-data-form").find("#username-upload-exam").val("");
               $("#upload-exam-data-form").find("#password-upload-exam").val("");
             }
           }
         }
      });
    }

    function get_exam_list_and_paper_details(studentname,fromdate,todate,type){
      $.ajax({
         dataType: "json",
         url: base_url('get_exam_and_paper_details/'+type),
         type: "post",
         data: { studentname: studentname,
                 fromdate: fromdate,
                 todate: todate,
                 onlineexam_token: csrfTkn()},
         success: function(result){
           $('.adminToken').val(result.csrf_token);
           if(result.num == "1"){
             alert("Check searching data is correct or not");
           }
           else if(result.num == "5"){
             alert("Please fill all details.");
           }
           else if(result.num == "3"){
             alert("Check date is proper or not.");
           }
           else if(result.num == "2"){
             if(result.type == "exam_list"){
               $("#view_exam_data").empty();
               if(result.message == ""){
                 $("#view_exam_data").append("<table class='table table-bordered table-hover table-condensed' style='font-size:12px;'><thead><tr><th>Exam Id</th><th>Admission Id</th><th>Name</th><th>Subject</th><th>Date</th><th>Time</th><th>Internal Marks</th><th>Online Marks</th><th>Total Marks</th><th>Percentage</th><th>Result</th><th>Exam Given</th><th>Centre Name</th></tr></thead><tbody id='students_exam_list'></tbody></table>");
                 var i = 0;
                 for(i=0;i<result.exam_list.length;i++){
                   var DateCreated = $.datepicker.formatDate('dd/mm/yy', new Date(result.exam_list[i].Date));

                   var internalMarks = result.exam_list[i].InternalExamMarks;
                   var internalOutOf = result.exam_list[i].InternalOutOf;
                   var onlineMarks = result.exam_list[i].Marks;
                   var onlineOutOf = result.exam_list[i].outof;

                   if(internalMarks == null){
                     internalMarks = 0;
                   }
                   if(internalOutOf == null){
                     internalOutOf = 0;
                   }
                   if(onlineMarks == null){
                     onlineMarks = 0;
                   }
                   if(onlineOutOf == null){
                     onlineOutOf = 0;
                   }

                   var totalMarks = parseInt(internalMarks) + parseInt(onlineMarks);
                   var totalOutOf = parseInt(internalOutOf) + parseInt(onlineOutOf);;
                   var percentage = Math.round(totalMarks/totalOutOf*100);
                   var internalPercentage = Math.round(internalMarks/internalOutOf*100);
                   var onlinePercentage = Math.round(onlineMarks/onlineOutOf*100);

                   var internal = 0;
                   var online = 0;
                   var internalColor = "red";
                   var onlineColor = "red";
                   var practicalColor = "red";
                   if(internalPercentage >= 40){
                     internal = 1;
                     internalColor = "green";
                   }
                   if(onlinePercentage >= 40){
                     online = 1;
                     onlineColor = "green";
                   }

                   var marksStatus = internal * online;
                   var Status = "<label style='font-weight:bold;color:red;'>Fail</label>";
                   if(marksStatus){
                     Status = "<label style='font-weight:bold;color:green;'>Pass</label>";
                   }

                   var examgiven = '<i class="glyphicon glyphicon-remove" style="color:red;"></i>';
                   var numExamGiven = result.exam_list[i].ExamGiven;
                   if(numExamGiven == "1"){
                     examgiven = '<i class="glyphicon glyphicon-ok" style="color:green;"></i>';
                   }
                   $("#students_exam_list").append("<tr><td>"+result.exam_list[i].ExamID+"</td><td>"+result.exam_list[i].AdmissionID+"</td><td>"+result.exam_list[i].StudentName+"</td><td>"+result.exam_list[i].Course_Name+"</td><td>"+DateCreated+"</td><td>"+result.exam_list[i].Time+"</td><td><label style='color:"+internalColor+"'>"+internalMarks+"/"+internalOutOf+"</label></td><td><label style='color:"+onlineColor+"'>"+onlineMarks+"/"+onlineOutOf+"</label></td><td>"+totalMarks+"/"+totalOutOf+"</td><td>"+percentage+"</td><td>"+Status+"</td><td>"+examgiven+"</td><td>"+result.exam_list[i].Centre_Name+"</td></tr>");
                 }
               }
               else{
                 $("#view_exam_data").append("<label>"+result.message+"</label>");
               }
               $("#exam-list-form").find("#studentname-exam-list").val("");
               $("#exam-list-form").find("#fromdate-exam-list").val("");
               $("#exam-list-form").find("#todate-exam-list").val("");
             }
             else if(result.type == "question_paper_backup"){
               $("#view_question_paper_details").empty();
               if(result.message == ""){
                 $("#view_question_paper_details").append("<table class='table table-bordered table-hover table-condensed' style='font-size:12px;'><thead><tr><th>Exam Id</th><th>Admission Id</th><th>Name</th><th>Subject</th><th>Date</th><th>Internal Marks</th><th>Online Marks</th><th>Total Marks</th><th>Percentage</th><th>Result</th><th>Centre Name</th><th>Download</th></tr></thead><tbody id='students_given_exam_list'></tbody></table>");
                 var i = 0;
                 for(i=0;i<result.exam_list.length;i++){
                   var DateCreated = $.datepicker.formatDate('dd/mm/yy', new Date(result.exam_list[i].Date));

                   var internalMarks = result.exam_list[i].InternalExamMarks;
                   var internalOutOf = result.exam_list[i].InternalOutOf;
                   var onlineMarks = result.exam_list[i].Marks;
                   var onlineOutOf = result.exam_list[i].outof;

                   if(internalMarks == null){
                     internalMarks = 0;
                   }
                   if(internalOutOf == null){
                     internalOutOf = 0;
                   }
                   if(onlineMarks == null){
                     onlineMarks = 0;
                   }
                   if(onlineOutOf == null){
                     onlineOutOf = 0;
                   }

                   var totalMarks = parseInt(internalMarks) + parseInt(onlineMarks);
                   var totalOutOf = parseInt(internalOutOf) + parseInt(onlineOutOf);;
                   var percentage = Math.round(totalMarks/totalOutOf*100);
                   var internalPercentage = Math.round(internalMarks/internalOutOf*100);
                   var onlinePercentage = Math.round(onlineMarks/onlineOutOf*100);

                   var internal = 0;
                   var online = 0;
                   var internalColor = "red";
                   var onlineColor = "red";
                   var practicalColor = "red";
                   if(internalPercentage >= 40){
                     internal = 1;
                     internalColor = "green";
                   }
                   if(onlinePercentage >= 40){
                     online = 1;
                     onlineColor = "green";
                   }



                   var marksStatus = internal * online;
                   var Status = "<label style='font-weight:bold;color:red;'>Fail</label>";
                   if(marksStatus){
                     Status = "<label style='font-weight:bold;color:green;'>Pass</label>";
                   }

                   var downloadLink = '<a href="javascript:void(0)" class="download_exam_paper" data-exam_id="'+result.exam_list[i].ExamID+'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                   $("#students_given_exam_list").append("<tr><td>"+result.exam_list[i].ExamID+"</td><td>"+result.exam_list[i].AdmissionID+"</td><td>"+result.exam_list[i].StudentName+"</td><td>"+result.exam_list[i].Course_Name+"</td><td>"+DateCreated+"</td><td><label style='color:"+internalColor+"'>"+internalMarks+"/"+internalOutOf+"</label></td><td><label style='color:"+onlineColor+"'>"+onlineMarks+"/"+onlineOutOf+"</label></td><td>"+totalMarks+"/"+totalOutOf+"</td><td>"+percentage+"</td><td>"+Status+"</td><td>"+result.exam_list[i].Centre_Name+"</td><td>"+downloadLink+"</td></tr>");
                 }
               }
               else{
                 $("#view_question_paper_details").append("<label>"+result.message+"</label>");
               }
               $("#download-exam-paper-form").find("#studentname-question-paper").val("");
               $("#download-exam-paper-form").find("#fromdate-question-paper").val("");
               $("#download-exam-paper-form").find("#todate-question-paper").val("");
             }
           }
         }
      });
    }

    $(document).on("click",".download_exam_paper",function(){
      var examId = $(this).attr("data-exam_id");
      $.ajax({
         dataType: "json",
         url: base_url('get_exam_paper_backup_by_exam_id/'+examId),
         type: "post",
         data: { onlineexam_token: csrfTkn()},
         success: function(data){
           $('.adminToken').val(data.csrf_token);
           var filename = "question_paper_backup_"+examId+".xls";
           var a = $("<a>");
            a.attr("href",data.file);
            $(".download_exam_paper").closest("td").append(a);
            a.attr("download",filename);
            a[0].click();
            a.remove();
         }
      });
    });

  });
  </script>
