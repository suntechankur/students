<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studentdetails_controller extends MX_Controller {

     public function __construct()
        {
                parent::__construct();
                $this->load->model('studentdetails_model');
                $this->load->model('login/login_model');
        }

        public function setinstruction($admission_id){
            if($admission_id == $this->session->userdata('student_data')[0]['ADMISSION_ID']){
              if(isset($_POST['accept_terms'])){
                $acceptance = $this->login_model->update_policy_details($admission_id);
                if($acceptance){
                  redirect(base_url('student_details/'.$admission_id.'/ds'));
                }
                else{
                  redirect(base_url('logout'));
                }
              }
              $this->load->view('instruction_set');
            }
            else{
              $this->session->unset_userdata('student_data');
              $this->session->sess_destroy();
              redirect(base_url());
            }
        }

        public function student_details($admission_id){
              $data['studData'] = $this->studentdetails_model->getStudentData($admission_id);
              $data['centerDetails'] = $this->studentdetails_model->getCenterDetails();
              $data['universityDetails'] = $this->studentdetails_model->getUniversityDetails();
              $data['courseDetails'] = $this->studentdetails_model->getCourseDetails($admission_id);

              $data['installmentPlan'] = $this->studentdetails_model->getAdmissionInstallmentTransaction($admission_id);
              $data['receiptDetails'] = $this->studentdetails_model->getAdmissionReceipt($admission_id);
              $data['courses'] = $this->studentdetails_model->all_courses();
              $data['modules'] = $this->studentdetails_model->all_modules();

              $total_fees = 0;
              foreach($data['receiptDetails'] as $receipts){
                if($receipts['AMOUNT_DEPOSITED'] != NULL){
                  $total_fees += $receipts['AMOUNT_PAID_FEES'];
                }
              }
              $data['studData'][0]['FEE_PAID'] = $total_fees;

              if(count($data['receiptDetails'])){
                  $totalFees = $data['receiptDetails'][0]['TOTALFEES'];
                  $deductAmount = $data['receiptDetails'][0]['AMOUNT_PAID'] - $data['receiptDetails'][0]['AMOUNT_PAID_SERVICETAX'];

                  for($i=0;$i<count($data['receiptDetails']);$i++){
                      $j = $totalFees - $deductAmount;

                      $data['receiptDetails'][$i]['BALANCES'] = $j;       //adding this value in receiptDetails array
                      $totalFees = $j;

                      if($i != (count($data['receiptDetails']) - 1)){
                          $deductAmount = $data['receiptDetails'][$i + 1]['AMOUNT_PAID'] - $data['receiptDetails'][$i + 1]['AMOUNT_PAID_SERVICETAX'];
                      }

                      $j = "";
                  }
              }

              $data['otherReceipts'] = $this->studentdetails_model->getOtherReceiptsData($admission_id);
              $data['courseStatus'] = $this->studentdetails_model->getStudentCourseStatus($admission_id);
              for($i=0;$i<count($data['courseStatus']);$i++){
                  $lectureCount= $this->studentdetails_model->getStudentLectureDetails($data['courseStatus'][$i]['BATCHID']);
                  $attendanceCount= $this->studentdetails_model->getStudentAttendanceDetails($data['courseStatus'][$i]['BATCHID'],$admission_id);

                  $data['courseStatus'][$i]['PRESENT'] = $attendanceCount;
                  $data['courseStatus'][$i]['TOTALNOBATCH'] = $lectureCount;
              }

              // for getting the pending subject details from course details and course status array doing arraydiff for getting the resultant pending subject details

              $fullCourseSubject['COURSE_NAME'] = array();
              $courseStatus['COURSE_NAME'] = array();

              for($i=0;$i<count($data['courseDetails']);$i++){
                  $fullCourseSubject['COURSE_NAME'][] = $data['courseDetails'][$i]['COURSE_NAME'];
              }
              for($j=0;$j<count($data['courseStatus']);$j++){
                  $courseStatus['COURSE_NAME'][] = $data['courseStatus'][$j]['COURSE_NAME'];
              }

              $data['pendingSubject'][] = array_diff($fullCourseSubject['COURSE_NAME'], $courseStatus['COURSE_NAME']);

              if(isset($_POST['update_details'])){
                if($_POST['ENQUIRY_EMAIL'] != ""){
                    $enquiry_id = $_POST['ENQUIRY_ID'];
                    unset($_POST['ENQUIRY_ID']);
                    unset($_POST['update_details']);
                    $booked = $this->studentdetails_model->update_student_normal_details($_POST,$enquiry_id);
                    if($booked){
                        $this->session->set_flashdata('success', 'Email Id Updated Successfully !!!');
                        redirect(base_url('student_details/'.$admission_id.'/ds'));
                    }
                  }
                  else{
                    $this->session->set_flashdata('success', 'If you are updating your data then fill all blank textbox details.');
                    redirect(base_url('student_details/'.$admission_id.'/ds'));
                  }
                }
              // $data['examStatus'] = $this->student_model->getStudentExamStatus($enqId);
              $this->load->admin_view('student_information',$data);
        }

        public function exam_details($admission_id){
          $data['examStatus'] = $this->studentdetails_model->getStudentExamStatus($admission_id);
          $data['examSchedular'] = $this->studentdetails_model->getExamDatesAsPerStream();
          // echo "<pre>";
          // print_r($data['examSchedular']);
          // echo "</pre>";
          // exit();
          $data['studData'] = $this->studentdetails_model->getStudentData($admission_id);
          $data['otherReceipts'] = $this->studentdetails_model->getOtherReceiptsData($admission_id);
          $data['courseDetails'] = $this->studentdetails_model->getCourseDetails($admission_id);
          $data['centres'] = $this->studentdetails_model->suggested_centre();
          $data['examdetails'] = $this->studentdetails_model->getExamBookingDetails($admission_id);
          $data['courses'] = $this->studentdetails_model->all_courses();
          $data['modules'] = $this->studentdetails_model->all_modules();

          if(isset($_POST['book_exam'])){
            if($_POST['VISIBLE'] == "show"){
              if(($_POST['EXAM_FEES_RECEIPT_NO'] != "") && ($_POST['COURSE_ID'] != "") && ($_POST['CENTRE_ID'] != "") && ($_POST['exam_type'] == "practical")){
                $booked = $this->studentdetails_model->bookExam($_POST,$_POST['exam_type']);
                if($booked){
                  $confirmation = $this->hallticket_print($admission_id,$booked,$_POST['exam_type'],$_POST['EMAIL_ID']);
                  if($confirmation){
                    $this->session->set_flashdata('success', 'Exam Booked Successfully !!!');
                    redirect(base_url('exam_details/'.$admission_id.'/dxe'));
                  }
                }
              }
              elseif (($_POST['EXAM_TIME'] != "") && ($_POST['EXAM_FEES_RECEIPT_NO'] != "") && ($_POST['COURSE_ID'] != "") && ($_POST['CENTRE_ID'] != "") && ($_POST['exam_type'] == "theory")) {
                $booked = $this->studentdetails_model->bookExam($_POST,$_POST['exam_type']);
                if($booked == "notfound"){
                  $this->session->set_flashdata('danger', 'Question paper not found.');
                  redirect(base_url('exam_details/'.$admission_id.'/dxe'));
                }
                elseif($booked){
                  $confirmation = $this->hallticket_print($admission_id,$booked,$_POST['exam_type'],$_POST['EMAIL_ID']);
                  if($confirmation){
                    $this->session->set_flashdata('success', 'Exam Booked Successfully !!!');
                    redirect(base_url('exam_details/'.$admission_id.'/dxe'));
                  }
                }
                else{
                  $this->session->set_flashdata('danger', 'Please fill all required details.');
                  redirect(base_url('exam_details/'.$admission_id.'/dxe'));
                }
              }
              else{
                $this->session->set_flashdata('danger', 'Please select all required details.');
                redirect(base_url('exam_details/'.$admission_id.'/dxe'));
              }
            }
            else{
              $this->session->set_flashdata('danger', 'Exam booking not started yet.');
              redirect(base_url('exam_details/'.$admission_id.'/dxe'));
            }
          }
          $this->load->admin_view('exam_details_and_bookings',$data);
        }

        public function getexamtimingslot(){
          $centreid = $this->input->post('centreid');
          $examdate = $this->input->post('examdate');
          $examType = 'THEORY';
          $exam_slots  = $this->studentdetails_model->getExamBookingTimingSlots($centreid,$examdate,$examType);

          echo json_encode($exam_slots);
        }

        public function hallticket_print($admId,$examid,$exam_type,$email_id){
          $hallticketdetails = $this->studentdetails_model->getExamHallticketDetails($admId,$examid,$exam_type);

          $this->load->helper('currency');
          $this->load->helper('pdf_helper');
          tcpdf();
          // create new PDF document
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

          // set document information
          $pdf->SetCreator(PDF_CREATOR);
          $pdf->SetAuthor('St. Angelos');
          $pdf->SetTitle('Print student hallticket');
          $pdf->SetSubject('Print student hallticket');
          $pdf->SetKeywords('St.angelos student hallticket');

          // set default header data
          $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

          // set header and footer fonts
          $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
          $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

          // set default monospaced font
          $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

          // set margins
          $pdf->SetMargins('15', '0', '15');
          $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
          $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

          // set auto page breaks
          $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

          // set image scale factor
          $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

          $pdf->SetPrintHeader(false);
          $pdf->SetPrintFooter(false);

          $pdf->setFontSubsetting(false);

          // add a page
          $pdf->AddPage();

          $examDate_replace = str_replace('/', '-', $hallticketdetails[0]['EXAM_DATE']);
          $examDate = date("d/m/Y", strtotime($examDate_replace));

          $examType = "";
          if($hallticketdetails[0]['EXAM_TYPE'] == "PRACTICAL"){
            $examType = "Practical Exam Hall Ticket";
          }
          else{
            $examType = "Theory Exam Hall Ticket";
          }

          $html = '<div>
                          <div>
                              <span align="center">
                                  <h2 style="padding:5px;">St. Angelos Computer Education </h2>
                                  <label>'.$examType.'</label>
                             </span>
                             <hr style="height:1px;">
                          </div>
                      <table width="640px" style="font-size: small; height: 1400px; padding:5px;">
                          <tr><td colspan="2"><strong>Name</strong></td><td colspan="6">: '.$hallticketdetails[0]['Name'].'</td></tr>
                          <tr><td colspan="2"><strong>Subject</strong></td><td colspan="6">: '.$hallticketdetails[0]['COURSE_NAME'].'</td></tr>
                          <tr><td colspan="2"><strong>Admission Id</strong></td><td colspan="6">: '.$hallticketdetails[0]['ADMISSION_ID'].'</td></tr>
                          <tr><td colspan="2"><strong>Exam Id</strong></td><td colspan="6">: '.$hallticketdetails[0]['EXAM_ID'].'</td></tr>
                          <tr><td colspan="2"><strong>Exam Date</strong></td><td colspan="6">: '.$examDate.'</td></tr>
                          <tr><td colspan="2"><strong>Exam Time</strong></td><td colspan="6">: '.$hallticketdetails[0]['EXAM_TIME'].'</td></tr>
                          <tr><td colspan="2"><strong>Centre</strong></td><td colspan="6">: '.$hallticketdetails[0]['Exam_Center'].'</td></tr>
                          <tr><td colspan="2"><strong>Exam Centre</strong></td><td colspan="6">: '.$hallticketdetails[0]['CENTRE'].'</td></tr>
                          <tr><td colspan="2"><strong>Address</strong></td><td colspan="6">: '.$hallticketdetails[0]['ADDRESS'].'</td></tr>
                          <tr><td colspan="2"><strong>Name</strong></td><td colspan="2">_____________________</td><td colspan="2">_____________________</td><td colspan="2">_____________________</td></tr>
                          <tr><td colspan="2"><strong>Signature</strong></td><td colspan="2">_____________________</td><td colspan="2">_____________________</td><td colspan="2">_____________________</td></tr>
                          <tr><td colspan="2"></td><td colspan="2" align="center">Student</td><td colspan="2" align="center">Faculty</td><td colspan="2" align="center">C.M.</td></tr>
                          <tr><td colspan="8"><strong>Note:</strong> Kindly carry your ID Card. Re-examination will carry Rs. 1000/- in case of absenteeism & Rs. 300/- in case of failure or for next exam.</td></tr>
                          <tr><td colspan="8">For exam related queries contact to : <strong>9324488356/9324488368.</strong></td></tr>
                      </table>
                  </div>';

          $file_name = $this->encrypt->encode($hallticketdetails[0]['Name']).$hallticketdetails[0]['EXAM_ID'].".pdf";

          // output the HTML content
          $pdf->writeHTML($html, true, false, true, false, '');

          // reset pointer to the last page
          $pdf->lastPage();

          //Close and output PDF document
          $attachment = $pdf->Output($file_name, 'S');

          $attachments = array("0"=>array("file"=>$attachment,"name"=>$file_name));

          // for mail forwarding attachment on student mail id
          $from = "emis@saintangelos.com";
          $to = $email_id;
          // $to = "ankurprajapati66@gmail.com";
          $cc = array('ankur.prajapati@saintangelos.com','ramkishor.sharma@saintangelos.com');
          $subject = $examType." from saintangelos on ".date("d/m/Y");
          $body = "<h2>Dear ".$hallticketdetails[0]['Name']."</h2>
                   <br/>
                   <p style='color:red;font-weight:bold;'>Kindly take print of hallticket and also required authorised signature of Faculty, Centre Manager while coming to the exam centre.</p>
                   <p>* Please do not reply to this mail. This is automated generated email.<br><br>For more information visit on <a href='http://saintangelos.com/'>saintangelos.com</a></p>";
          $confirmationMail = $this->load->mailer($from,$to,$cc,"",$attachments,$subject,$body,"studentdesk");
          return $confirmationMail;
        }

        public function student_notices(){
          $data['notice'] = $this->studentdetails_model->all_notice();
          $this->load->admin_view('notice_board_details',$data);
        }

        public function notice_download($download_link){
           redirect(base_url('resources/NoticeBoard/'.$download_link));
        }

        public function get_assignments($admission_id){
          $data['all_assignments'] = $this->studentdetails_model->get_assignment_details($admission_id);
          $data['notice'] = $this->studentdetails_model->all_notice();
          if(isset($_POST['assignment_submit'])){
            if(!empty($_FILES['upload_assignment'])){
                $assignment_id = $_POST['ASSIGNMENT_FEEDING_MASTER_ID'];
                $path = $config['upload_path']   = 'resources/Students_assignment/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|zip|rar';
                $config['remove_spaces']  = true;
                $config['file_name'] = $assignment_id."_assignment_".$admission_id."_".time();
                $config['file_ext'] = pathinfo($_FILES["upload_assignment"]["name"],PATHINFO_EXTENSION);

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('upload_assignment'))
                {
                   $error = array('error' => $this->upload->display_errors());
                   $this->session->set_flashdata('success', 'Check extension of your file or try to upload assignments after some time.');
                   redirect(base_url('assignments/'.$admission_id.'/ngi'));
                }
                else
                {
                  $_POST['ADMISSION_ID'] = $admission_id;
                  $_POST['ASSIGNMENT_FILE_NAME'] = $config['file_name'] .".". $config['file_ext'];
                  $_POST['ASSIGNMENT_FILE_PATH'] = "resources/Students_assignment/". $config['file_name'] .".".$config['file_ext'];
                  $_POST['ASSIGNMENT_FILE_EXTENSION'] = ".".$config['file_ext'];
                  $data = array('upload_data' => $this->upload->data());
                  $upload_assignments = $this->studentdetails_model->upload_assignment($_POST);
                  if($upload_assignments){
                    $this->session->set_flashdata('success', 'Assignment uploaded successfully');
                    redirect(base_url('assignments/'.$admission_id.'/ngi'));
                  }
                }
            }
            else{
              $this->session->set_flashdata('success', 'Please select file to upload.');
              redirect(base_url('assignments/'.$admission_id.'/ngi'));
            }
          }
          $this->load->admin_view('assignment_details',$data);
        }

        public function batch_required($admission_id){
          $data['courses'] = $this->studentdetails_model->all_courses();
          $data['studData'] = $this->studentdetails_model->getStudentData($admission_id);
          if(isset($_POST['send_requirement'])){
            if(($_POST['PREFERRED_BAND_FROM1'] != "") && ($_POST['PREFERRED_BAND_FROM2'] != "") && ($_POST['PREFERRED_LOCATION1'] != "") && ($_POST['STUDENT_CONTACT'] != "") && ($_POST['STUDENT_EMAIL'] != "") && ($_POST['PREFERRED_BAND_TO1'] != "") && ($_POST['PREFERRED_BAND_TO2'] != "") && ($_POST['PREFERRED_LOCATION2'] != "") && ($_POST['COURSE_TAKEN'] != "")){
              $confirmation = $this->studentdetails_model->insert_batch_requirement($_POST);
              if($confirmation){
                $this->session->set_flashdata('success', 'Batch requirement sent successfully.');
                redirect(base_url('requirement_batch/'.$admission_id.'/hbr'));
              }
            }
            else{
              $this->session->set_flashdata('success', 'Please fill and select all details.');
              redirect(base_url('requirement_batch/'.$admission_id.'/hbr'));
            }
          }
          $this->load->admin_view('requirement_batch',$data);
        }

        public function query_and_complaints($admission_id){
          if(isset($_POST['submit_query'])){
            if(($_POST['query_date'] != "") && ($_POST['query'] != "")){
              $from = "emis@saintangelos.com";
              if(($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "abc@gmail.com") || ($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "")){
                $to = "studentdesk@saintangelos.com";
              }
              else{
                $to = $this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'];
              }
              $cc = array('ankur.prajapati@saintangelos.com','dinesh.kanojiya@saintangelos.com');
              $subject = "Query from Student Desk on ".date("d/m/Y");
              $body = "Hello Sir/Mam,
                       <br/>
                       <br/>
                       **".ucfirst($this->session->userdata('student_data')[0]['student_name'])." here,
                       <p>Issue Date From : ".$_POST['query_date']."</p>
                       <p>Query : ".$_POST['query']."</p>
                       <h5>Thanks And Regards</h5>";
              $confirmationMail = $this->load->mailer($from,$to,$cc,"","",$subject,$body,"studentdesk");
              if($confirmationMail){
                $this->session->set_flashdata('success', 'Query submitted successfully.');
                redirect(base_url('submit_queries/'.$admission_id.'/usq'));
              }
            }
            else{
              $this->session->set_flashdata('success', 'Please fill all details.');
              redirect(base_url('submit_queries/'.$admission_id.'/usq'));
            }
          }
          $this->load->admin_view('queries_or_complaint');
        }

        public function leave_application_and_break_request($admission_id){
          $data['studData'] = $this->studentdetails_model->getStudentData($admission_id);
          $data['courses'] = $this->studentdetails_model->all_courses();
          $data['modules'] = $this->studentdetails_model->all_modules();

          $data['courseDetails'] = $this->studentdetails_model->getCourseDetails($admission_id);

          $data['courseStatus'] = $this->studentdetails_model->getStudentCourseStatus($admission_id);
          for($i=0;$i<count($data['courseStatus']);$i++){
              $lectureCount= $this->studentdetails_model->getStudentLectureDetails($data['courseStatus'][$i]['BATCHID']);
              $attendanceCount= $this->studentdetails_model->getStudentAttendanceDetails($data['courseStatus'][$i]['BATCHID'],$admission_id);

              $data['courseStatus'][$i]['PRESENT'] = $attendanceCount;
              $data['courseStatus'][$i]['TOTALNOBATCH'] = $lectureCount;
          }

          // for getting the pending subject details from course details and course status array doing arraydiff for getting the resultant pending subject details

          $fullCourseSubject['COURSE_NAME'] = array();
          $courseStatus['COURSE_NAME'] = array();

          for($i=0;$i<count($data['courseDetails']);$i++){
              $fullCourseSubject['COURSE_NAME'][] = $data['courseDetails'][$i]['COURSE_NAME'];
          }
          for($j=0;$j<count($data['courseStatus']);$j++){
              $courseStatus['COURSE_NAME'][] = $data['courseStatus'][$j]['COURSE_NAME'];
          }

          $data['pendingSubject'][] = array_diff($fullCourseSubject['COURSE_NAME'], $courseStatus['COURSE_NAME']);

          if(isset($_POST['send_request'])){
            // $email_ids = $this->studentdetails_model->get_centre_person_mail_id($_POST['CENTRE_ID']);
            if($_POST['application_or_request'] == 'break_request'){
              if(($_POST['FROM_DATE'] != "") && ($_POST['TO_DATE'] != "") && ($_POST['EXPECTED_REJOINING_DATE'] != "") && ($_POST['CONTENT_OF_REASON'] != "")){
                $f_date = str_replace('/', '-',$_POST['FROM_DATE']);
	              $from_date = date('Y-m-d',strtotime($f_date));
                $t_date = str_replace('/', '-',$_POST['TO_DATE']);
	              $to_date = date('Y-m-d',strtotime($t_date));
                $r_date = str_replace('/', '-',$_POST['EXPECTED_REJOINING_DATE']);
	              $rejoining_date = date('Y-m-d',strtotime($r_date));

                $break_request = array('FROM_DATE' => $from_date,
                                        'TO_DATE' => $to_date,
                                        'EXPECTED_REJOINING_DATE' => $rejoining_date,
                                        'CONTENT_OF_REASON' => $_POST['CONTENT_OF_REASON'],
                                        'ADMISSION_ID' => $_POST['ADMISSION_ID'],
                                        'BREACK_APPLICATION_DATE' => date("Y-m-d H:i:s"),
                                        'MODIFIED_DATE' => date("Y-m-d H:i:s"));
                $break_request = $this->studentdetails_model->submit_break_request($break_request);
                if($break_request){
                  $from = "studentdesk@saintangelos.com";
                    if(($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "abc@gmail.com") || ($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "")){
                      $to = "studentdesk@saintangelos.com";
                    }
                    else{
                      $to = $this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'];
                    }
                    $cc = array('ankur.prajapati@saintangelos.com','dinesh.kanojiya@saintangelos.com');
                    $subject = "Break Application from ".$_POST['NAME'];
                    $body = "Student Desk has received Break Apllication from ".$_POST['NAME']."<br><br>
                              <table border='0'>
                                <tr><td> Admission ID </td><td> : </td><td> ".$_POST['ADMISSION_ID']."</td></tr>
                                <tr><td> Center </td><td> : </td><td> ".$_POST['CENTRE_NAME']."</td></tr>
                                <tr><td> Course Taken </td><td> : </td><td> ".$_POST['COURSE_TAKEN']."</td></tr>
                                <tr><td> Admission Date </td><td> : </td><td> ".$_POST['ADMISSION_DATE']."</td></tr>
                                <tr><td> Break from </td><td> : </td><td> ".$_POST['FROM_DATE']."</td></tr>
                                <tr><td> Break To </td><td> : </td><td> ".$_POST['TO_DATE']."</td></tr>
                                <tr><td> Expected Rejoining Date </td><td> : </td><td> ".$_POST['EXPECTED_REJOINING_DATE']."</td></tr>
                                <tr><td> Reason </td><td> : </td><td> ".$_POST['CONTENT_OF_REASON']."</td></tr>
                              </table>";
                    $confirmationMail = $this->load->mailer($from,$to,$cc,"","",$subject,$body,"studentdesk");
                    if($confirmationMail){
                      $this->session->set_flashdata('success', 'Break request submitted successfully.');
                      redirect(base_url('leave_application_and_break_request/'.$admission_id.'/al/rs'));
                    }
                }
              }
              else{
                $this->session->set_flashdata('success', 'Please fill all break request from details.');
                redirect(base_url('leave_application_and_break_request/'.$admission_id.'/al/rs'));
              }
            }
            else{
              if(($_POST['LEAVE_FROM_DATE'] != "") && ($_POST['LEAVE_TO_DATE'] != "") && ($_POST['LEAVE_REASON'] != "")){
                $l_f_date = str_replace('/', '-',$_POST['LEAVE_FROM_DATE']);
	              $leave_from_date = date('Y-m-d',strtotime($l_f_date));
                $l_t_date = str_replace('/', '-',$_POST['LEAVE_TO_DATE']);
	              $leave_to_date = date('Y-m-d',strtotime($l_t_date));

                $leave_application = array('LEAVE_FROM_DATE' => $leave_from_date,
                                            'LEAVE_TO_DATE' => $leave_to_date,
                                            'CENTRE_ID' => $_POST['CENTRE_ID'],
                                            'LEAVE_REASON' => $_POST['LEAVE_REASON'],
                                            'ADMISSION_ID' => $_POST['ADMISSION_ID'],
                                            'MODIFIED_BY' => $_POST['ADMISSION_ID'],
                                            'APPLICATION_DATE' => date("Y-m-d H:i:s"),
                                            'MODIFIED_DATE' => date("Y-m-d H:i:s"));
                $leave_application = $this->studentdetails_model->submit_leave_application($leave_application);
                if($leave_application){
                  $from = "studentdesk@saintangelos.com";
                    if(($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "abc@gmail.com") || ($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "")){
                      $to = "studentdesk@saintangelos.com";
                    }
                    else{
                      $to = $this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'];
                    }
                    $cc = array('ankur.prajapati@saintangelos.com','dinesh.kanojiya@saintangelos.com');
                    $subject = "Leave Application from ".$_POST['NAME'];
                    $body = "Student Desk has received Leave Application from ".$_POST['NAME']."<br><br>
                              <table border='0'>
                                <tr><td> Admission ID </td><td> : </td><td> ".$_POST['ADMISSION_ID']."</td></tr>
                                <tr><td> Center </td><td> : </td><td> ".$_POST['CENTRE_NAME']."</td></tr>
                                <tr><td> Application Date </td><td> : </td><td> ".date("d/m/Y")."</td></tr>
                                <tr><td> Leave From </td><td> : </td><td> ".$_POST['LEAVE_FROM_DATE']."</td></tr>
                                <tr><td> Leave To </td><td> : </td><td> ".$_POST['LEAVE_TO_DATE']."</td></tr>
                                <tr><td> Reason </td><td> : </td><td> ".$_POST['LEAVE_REASON']."</td></tr>
                              </table>";
                    $confirmationMail = $this->load->mailer($from,$to,$cc,"","",$subject,$body,"studentdesk");
                    if($confirmationMail){
                      $this->session->set_flashdata('success', 'Leave application submitted successfully.');
                      redirect(base_url('leave_application_and_break_request/'.$admission_id.'/al/rs'));
                    }
                }
              }
              else{
                $this->session->set_flashdata('success', 'Please fill all leave application details.');
                redirect(base_url('leave_application_and_break_request/'.$admission_id.'/al/rs'));
              }
            }
          }
          $this->load->admin_view('leave_application_break_request',$data);
        }

        public function placement_training($admission_id){
          $data['company_details'] = $this->studentdetails_model->placement_details($this->session->userdata('student_data')[0]['STREAM']);
          $data['studData'] = $this->studentdetails_model->getStudentData($admission_id);
          $data['courses'] = $this->studentdetails_model->all_courses();
          $data['modules'] = $this->studentdetails_model->all_modules();
          if(isset($_POST['resume_submit'])){
            if(!empty($_FILES['resume_file'])){
                $company_id = $_POST['COMPANYAPPLIEDFOR'];
                $centre_name = $_POST['centre_name'];
                $course_name = $_POST['course_name'];
                unset($_POST['centre_name']);
                unset($_POST['course_name']);
                $path = $config['upload_path']   = 'resources/Placement/';
                $config['allowed_types'] = 'pdf|doc';
                $config['remove_spaces']  = true;
                $config['file_name'] = $company_id."_resume_".$admission_id."_".time();
                $config['file_ext'] = pathinfo($_FILES["resume_file"]["name"],PATHINFO_EXTENSION);

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('resume_file'))
                {
                   $error = array('error' => $this->upload->display_errors());
                   $this->session->set_flashdata('success', 'Check extension of your file or try to upload resume after some time.');
                   redirect(base_url('placement_training_details/'.$admission_id.'/dit'));
                }
                else
                {
                  $_POST['STUDENTADMISSIONID'] = $admission_id;
                  $_POST['RESUMEFILENAME'] = $config['file_name'] .".". $config['file_ext'];
                  $_POST['RESUMEFILEPATH'] = "resources/Placement/". $config['file_name'] .".".$config['file_ext'];
                  $data = array('upload_data' => $this->upload->data());

                  // send resume on mail
                  if(($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "abc@gmail.com") || ($this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'] == "")){
                    $from = "studentdesk@saintangelos.com";
                  }
                  else{
                    $from = $this->session->userdata('student_data')[0]['ENQUIRY_EMAIL'];
                  }

                  $to = "placement@saintangelos.com";

                  //$file = $this->upload->data()['file_path'];
                  $attachments = $this->upload->data()['full_path']; //array("0"=>array("file"=>$file,"name"=>$config['file_name'] ));  // here is some error in attachments

                  // $cc = array('ankur.prajapati@saintangelos.com','paulritesh@saintangelos.com');
                  $subject = "Student Resume From ".$centre_name." Centre Applied For ".$_POST['JOBAPPLIEDFOR']."Designation";
                  $body = "<table border='1'>
                 <tr><td> Student Name :</td><td> ".ucfirst($this->session->userdata('student_data')[0]['student_name'])."</td></tr>
                 <tr><td>Admission id</td><td> ".$admission_id."</td></tr>
                 <tr><td>Course Taken :</td><td> ".$course_name."</td></tr>
                 <tr><td>Centre Name :</td><td> ".$centre_name."</td></tr>
                 <tr><td>Job Designation Applied For :</td><td> ".$_POST['JOBAPPLIEDFOR']."</td></tr></table><br /><p>Please Find the Student's Resume in the Attachment</p>";
                  $confirmationMail = $this->load->mailer($from,$to,"","",$attachments,$subject,$body,"studentdesk");
                  // send resume on mail

                  $upload_resume = $this->studentdetails_model->upload_resume($_POST);
                  if($upload_resume && $confirmationMail){
                    $this->session->set_flashdata('success', 'Resume uploaded successfully');
                    redirect(base_url('placement_training_details/'.$admission_id.'/dit'));
                  }
                }
            }
            else{
              $this->session->set_flashdata('success', 'Please select file to upload.');
              redirect(base_url('placement_training_details/'.$admission_id.'/dit'));
            }
          }
          $this->load->admin_view('placement_training_details',$data);
        }

        public function study_material($admission_id){
          $data['notice'] = $this->studentdetails_model->all_notice();
          if (strpos($this->session->userdata('student_data_course_taken'), 'YCMOU') !== false) {
            $data['univeristy_books'] = $this->studentdetails_model->fetch_university_books_details($this->session->userdata('student_data')[0]['STREAM']);
          }
          $this->load->admin_view('study_material_download',$data);
        }

        public function subject_name_for_search(){
          $book_type = $this->input->post('book_type');
          $data['subjects'] = $this->studentdetails_model->fetch_course_and_subject_details($book_type,$this->session->userdata('student_data')[0]['STREAM']);
          echo json_encode($data);
        }

        public function book_details(){
          $file_name = $this->input->post('file_name');
          $data['books'] = $this->studentdetails_model->fetch_ebook_by_subject_details($file_name);
          echo json_encode($data);
        }

        public function book_download($download_link){
           redirect(base_url('resources/Download_ebooks/'.$download_link));
        }

        public function videos($admission_id){
          $this->load->admin_view('company_profile');
        }

        public function testimonials($admission_id){
          $data["youtube_video_links"] = $this->studentdetails_model->get_testimonial_video_list();
          $this->load->admin_view('testimonials',$data);
        }

        public function edutalks($admission_id){
          $this->load->admin_view('edutalks');
        }

        public function student_policy($admission_id){
          $this->load->admin_view('student_policy');
        }

        public function events_activities($admission_id){
          $this->load->admin_view('activities');
        }

        public function update_password($admission_id){
          if(isset($_POST['update_password'])){
            $this->form_validation->set_rules('PASSWORD', 'Password', 'required');
            $this->form_validation->set_rules('NEW_PASSWORD', 'Password Confirmation', 'required');
            if ($this->form_validation->run() == TRUE){
              if(($_POST['PASSWORD'] != "") && ($_POST['NEW_PASSWORD'] != "")){
                if($_POST['PASSWORD'] == $_POST['NEW_PASSWORD']){
                  $confirmation = $this->studentdetails_model->update_student_password($admission_id,$_POST['PASSWORD']);
                    if($confirmation){
                      redirect(base_url('logout'));
                    }
                }
                else{
                  $this->session->set_flashdata('success', 'Both Password Not Matching.');
                  redirect(base_url('update_password/'.$admission_id.'/ssd'));
                }
              }
            }
            else{
              $this->session->set_flashdata('success', 'Please Fill Proper Details.');
              redirect(base_url('update_password/'.$admission_id.'/ssd'));
            }
          }
          $this->load->admin_view('update_password');
        }

}
