<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Queries & Complaints</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <?php echo form_open();?>
                    <div class="col-lg-12">
                      <div class="col-sm-4 col-md-offset-1">
                        <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="form-group">
                          <label>Date From Which You Are Facing The Issue : </label><input type="text" name="query_date" tabindex="1" class="form-control datepicker" placeholder="Date">
                        </div>
                        <div class="form-group">
                          <label>Query/issues : </label>
                          <textarea class="form-control" tabindex="2" placeholder="Query" name="query"></textarea>
                        </div>
                        <div class="form-group col-md-4 col-md-offset-4">
                          <input type="submit" name="submit_query" tabindex="3" class="btn btn-login" value="Submit" style="border-radius:0px;background:rgb(237,170,30);color: #fff;font-size: 12px;height: auto;font-weight: bold;padding: 10px 20px;border-color: #000">
                        </div>
                      </div>
                    </div>
                    <?php echo form_close();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>