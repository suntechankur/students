<div class="container">
      <div class="row">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Job Opportunities</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <label style="color:red">*Note: Resume should be (pdf, doc) format only.</label>
                    <?php
                    $centre_name = "";
                    $course_name = "";
                      foreach($studData as $sdata){
                        $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                             $courseNames = '';
                             //echo '<ul class="list-group">';
                              for($j=0;$j<count($courses);$j++){
                                  if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                        $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                      $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                    }

                              }
                        $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                             $moduleNames = '';
                             //echo '<ul class="list-group">';
                              for($j=0;$j<count($modules);$j++){
                                  if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                        $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                      $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                    }

                              }
                             //echo '</ul>';
                             $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
                             unset($sdata['MODULE_TAKEN']);

                             $centre_name = $sdata['CENTRE_NAME'];
                             $course_name = $sdata['COURSE_TAKEN'];
                      }?>
                    <table class='table table-bordered table-condensed' style='font-size:12px;border:none;'>
                      <tr style="font-weight:bold;">
                        <td>Sr No</td>
                        <td>Company Name</td>
                        <td>Stream</td>
                        <td>Designation</td>
                        <td>Job Profile</td>
                        <td>Job Location</td>
                        <td>Upload Resume</td>
                      </tr>
                        <?php
                        $i = 1;
                          foreach($company_details as $company){
                              echo "<tr>";
                              echo "<td>".$i++."</td>";
                              echo "<td>".$company['NAME_OF_COMPANY']."</td>";
                              echo "<td>".$company['STREAM']."</td>";
                              echo "<td>".$company['JOB_DESIGNATION']."</td>";
                              echo "<td>".$company['JOB_PROFILE']."</td>";
                              echo "<td>".$company['JOB_LOCATION']."</td>";
                              echo "<td><form enctype='multipart/form-data' method='post'><input type='hidden' name='centre_name' value='".$centre_name."'><input type='hidden' name='course_name' value='".$course_name."'><input type='hidden' name='JOBAPPLIEDFOR' value='".$company['JOB_DESIGNATION']."'><input type='hidden' name='COMPANYAPPLIEDFOR' value='".$company['COMPANY_ID']."'><input type='hidden' name='".$this->security->get_csrf_token_name()."' value='".$this->security->get_csrf_hash()."'><input type='file' name='resume_file'><button type='submit' name='resume_submit'>Upload</button></form></td>";
                              echo "</tr>";
                          }
                        ?>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
