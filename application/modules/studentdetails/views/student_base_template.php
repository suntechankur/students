<style>
body,html{
height: 100%;
}

/* remove outer padding */
.main .row{
padding: 0px;
margin: 0px;
}

/*Remove rounded coners*/

nav.sidebar.navbar {
border-radius: 0px;
}

nav.sidebar, .main{
-webkit-transition: margin 200ms ease-out;
  -moz-transition: margin 200ms ease-out;
  -o-transition: margin 200ms ease-out;
  transition: margin 200ms ease-out;
}

fieldset
{
border: 1px solid #000 !important;
margin: 0;
xmin-width: 0;
padding: 10px;
position: relative;
border-radius:2px;
padding-left:10px!important;
}

legend
{
  font-size:14px;
  font-weight:bold;
  border: 1px solid #ddd;
  border-radius: 4px;
  background-color: #ffffff;
}

/* Add gap to nav and right windows.*/
.main{
padding: 10px 10px 0 10px;
}


input[class*="form-control"],select[class*="form-control"]{
    font-size: 13px !important;
    border-radius:0px !important;
    height:30px !important;
}

.fa {
  width: 29px;
  height: 30px;
  padding: 8px 9px;
  font-size: 25px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  background: rgb(237,170,30);
  color:#fff;
  border-radius:5px;
}


.fa-facebook:hover {
  background: #3B5998;
  color:#fff;
  text-decoration: none;
}

.fa-twitter:hover {
  background: #55ACEE;
  color:#fff;
  text-decoration: none;
}

.fa-youtube {
  background: #bb0000;
}

.fa-instagram:hover {
  background: #ee2a7b;
  color:#fff;
  text-decoration: none;
}

.fa-whatsapp:hover {
  background: #4FCE5D;
  color:#fff;
  text-decoration: none;
}
/* .....NavBar: Icon only with coloring/layout.....*/

/*small/medium side display*/
@media (min-width: 768px) {

/*Allow main to be next to Nav*/
.main{
  position: absolute;
  width: calc(100% - 40px); /*keeps 100% minus nav size*/
  margin-left: 40px;
  float: right;
}

/*lets nav bar to be showed on mouseover*/
nav.sidebar:hover + .main{
  margin-left: 200px;
}

/*Center Brand*/
nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
  margin-left: 0px;
}
/*Center Brand*/
nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
  text-align: center;
  width: 100%;
  margin-left: 0px;
}

/*Center Icons*/
nav.sidebar a{
  padding-right: 13px;
}

/*adds border top to first nav box */
nav.sidebar .navbar-nav > li:first-child{
  border-top: 1px #000 solid;
}

/*adds border to bottom nav boxes*/
nav.sidebar .navbar-nav > li{
  font-size:12px;
  border-bottom: 1px #000 solid;
}

/* Colors/style dropdown box*/
nav.sidebar .navbar-nav .open .dropdown-menu {
  position: static;
  float: none;
  width: auto;
  margin-top: 0;
  background-color: transparent;
  border: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

/*allows nav box to use 100% width*/
nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
  padding: 0 0px 0 0px;
}

/*colors dropdown box text */
.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
  color: #777;
}

/*gives sidebar width/height*/
nav.sidebar{
  width: 200px;
  height: 100%;
  margin-left: -160px;
  float: left;
  z-index: 8000;
  margin-bottom: 0px;
}

/*give sidebar 100% width;*/
nav.sidebar li {
  width: 100%;
}

/* Move nav to full on mouse over*/
nav.sidebar:hover{
  margin-left: 0px;
}
/*for hiden things when navbar hidden*/
.forAnimate{
  opacity: 0;
}
}

/* .....NavBar: Fully showing nav bar..... */

@media (min-width: 1330px) {

/*Allow main to be next to Nav*/
.main{
  width: calc(100% - 200px); /*keeps 100% minus nav size*/
  margin-left: 200px;
}

/*Show all nav*/
nav.sidebar{
  margin-left: 0px;
  float: left;
}
/*Show hidden items on nav*/
nav.sidebar .forAnimate{
  opacity: 1;
}
}

nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
color: #CCC;
background-color: transparent;
}

nav:hover .forAnimate{
opacity: 1;
}
section{
padding-left: 15px;
}

.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: rgb(237,170,30);
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #F00;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #F00;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #FE3B3B;
	border-color: #FE3B3B;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.questions {
  font-weight: bold;
  color: #00415d;
}

.question-button{
border-radius: 100%;
border: 1px solid;
height: 30px;
padding: 4px 6px;
width: 30px;
text-align: center;
cursor: pointer;
background: #ff2e35;
color: #fff;
font-weight: bold;
-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}

.submit_test{
  border-radius: 10px;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.next_question,
.prev_question {
  border-radius: 100%;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}

.submit_test:hover,
.question-button:hover,
.next_question:hover,
.prev_question:hover {background-color: #e80007}

.submit_test:active,
.question-button:active,
.next_question:active,
.prev_question:active {
  background-color: #ff2e35;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  transform: translateY(4px);
}
</style>
<head>
  <title>STCE | Student Portal</title>
    <link rel="shortcut icon" href="<?php echo base_url('resources/'); ?>images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('resources/'); ?>images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>

<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<script type="text/javascript" src="<?php echo base_url('resources/'); ?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url('resources/'); ?>js/jquery-ui.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<nav class="navbar navbar-default sidebar" role="navigation" style="background:#fff;position:fixed;border: 1px solid black;box-shadow:6px -7px 15px -10px rgb(237,170,30);color:black;">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header" style="background:rgb(237,170,30);border:none;height:150px;padding:30px 8px;">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="JavaScript:Void(0);"><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height:60px;width:180px"></a>
        </div>
    <?php $admission_id = $this->session->userdata('student_data')[0]['ADMISSION_ID'];?>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url('student_details/'.$admission_id.'/ds');?>" style="font-weight:bold;padding:5px 15px;">My Details</a></li>
				<li><a href="<?php echo base_url('exam_details/'.$admission_id.'/dxe');?>" style="font-weight:bold;padding:5px 15px;">Examination</a></li>
				<li><a href="<?php echo base_url('notice/'.$admission_id.'/ce');?>" style="font-weight:bold;padding:5px 15px;">Notice Board</a></li>
				<li><a href="<?php echo base_url('assignments/'.$admission_id.'/ngi');?>" style="font-weight:bold;padding:5px 15px;">Assignment</a></li>
				<li><a href="<?php echo base_url('download_material/'.$admission_id.'/md');?>" style="font-weight:bold;padding:5px 15px;">Download</a></li>
				<!--<li><a href="<?php //echo base_url('requirement_batch/'.$admission_id.'/hbr');?>" style="font-weight:bold;padding:5px 15px;">Batch Requirement</a></li>-->
				<!--<li><a href="<?php //echo base_url('submit_queries/'.$admission_id.'/usq');?>" style="font-weight:bold;padding:5px 15px;">Queries / Complaint</a></li>-->
				<li><a href="<?php echo base_url('leave_application_and_break_request/'.$admission_id.'/al/rs');?>" style="font-weight:bold;padding:5px 15px;">Break Request / Leave Application</a></li>
				<!--<li><a href="<?php// echo base_url('placement_training_details/'.$admission_id.'/dit');?>" style="font-weight:bold;padding:5px 15px;">Placement / Training</a></li>-->
				<!--<li><a href="javascript:void(0);" style="font-weight:bold;padding:5px 15px;">Student Feedback</a></li>-->
				<!--<li><a href="<?php //echo base_url('company_videos/'.$admission_id.'/edc');?>" style="font-weight:bold;padding:5px 15px;">Videos</a></li>-->
				<li><a href="<?php echo base_url('students_testimonial/'.$admission_id.'/iolt');?>" style="font-weight:bold;padding:5px 15px;">Testimonial <span style="background-color:red;padding:3px 5px;font-weight:bold;border-radius:10px;color:white;margin-left:20px;">New</span></a></li>
				<!--<li><a href="<?php //echo base_url('activity_and_events/'.$admission_id.'/aae');?>" style="font-weight:bold;padding:5px 15px;color:rgb(237,170,30);">Event & Activities</a></li>-->
				<li><a href="<?php echo base_url('update_password/'.$admission_id.'/ssd');?>" style="font-weight:bold;padding:5px 15px;">Change Password</a></li>
				<li><a href="<?php echo base_url('logout');?>" style="font-weight:bold;padding:5px 15px;">Logout</a></li>
			</ul>
		</div>
		<div class="navbar-footer" style="border:none;text-align:center;padding:5px 8px;">
		    <label>Social Links:</label>
		    <p>
		          <a href="https://www.facebook.com/suntechedu/" target="_blank" class="fa fa-facebook" title="facebook"></a>
                  <a href="https://twitter.com/suntechedu" class="fa fa-twitter" target="_blank" title="twitter"></a>
                  <a href="https://www.instagram.com/suntechedu/" class="fa fa-instagram" target="_blank" title="instagram"></a>
                  <a href="https://wa.me/917709356777?text=I'm%20interested%20to%20join%20computer%20course." target="_blank" class="fa fa-whatsapp" title="whatsapp"></a>
		    </p>
		    <label>For More Visit :</label>
		    <p><a style="color:rgb(237,170,30);text-decoration:none;font-weight:bold;" href="https://www.suntechedu.com" target="_blank">www.suntechedu.com</a></p>
		</div>
	</div>
</nav>

<div class="main">
  <div class="row">
    <div class="col-lg-12">
      <p style="color:black;font-weight:bold;">Welcome  <?php echo $this->session->userdata('student_data')[0]['student_name'];?>, <label style="color:rgb(237,170,30);font-weight:bold;">If there is any query regarding this portal, Kindy mail on vijay@suntechedu.com / rajesh@suntechedu.com / ankur@suntechedu.com.</label></p>
      <?php if($this->session->flashdata('danger')) {
        echo "<script>alert('".$this->session->flashdata('danger')."');</script>";
       }
       elseif($this->session->flashdata('success')) {
        echo "<script>alert('".$this->session->flashdata('success')."');</script>";
       } ?>
    </div>
  </div>
  <?php $this->load->view($admin_view); ?>
</div>
<div id="divLoading"></div>
<script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>custom.js"></script>
