<style>
    .ui-corner-all
    {
        -moz-border-radius: 4px 4px 4px 4px;
    }
    .ui-widget-content
    {
        border: 5px solid black;
        color: #222222;
    }
    .ui-widget
    {
        font-family: Verdana,Arial,sans-serif;
        font-size: 15px;
    }
    .ui-menu
    {
        display: block;
        float: left;
        list-style: none outside none;
        margin: 0;
        padding: 2px;
        max-height: 250px;
        overflow: auto;
    }
    .ui-autocomplete
    {
        cursor: default;
        position: absolute;
    }
    .ui-menu .ui-menu-item
    {
        clear: left;
        float: left;
        margin: 0;
        padding: 0;
        width: 100%;
    }
    .ui-menu .ui-menu-item a
    {
        display: block;
        padding: 3px 3px 3px 3px;
        text-decoration: none;
        cursor: pointer;
        background-color: Green;
    }
    .ui-menu .ui-menu-item a:hover
    {
        display: block;
        padding: 3px 3px 3px 3px;
        text-decoration: none;
        color: White;
        cursor: pointer;
        background-color: red !important;
    }
    .ui-widget-content a
    {
        color: #222222;
    }

    #divLoading
{
    display : none;
}
#divLoading.show
{
    display : block;
    position : fixed;
    z-index: 100;
    background-image : url('http://loadinggif.com/images/image-selection/3.gif');
    background-color:#e24e31;
    opacity : 0.1;
    background-repeat : no-repeat;
    background-position : center;
    left : 0;
    bottom : 0;
    right : 0;
    top : 0;
}
#loadinggif.show
{
    left : 50%;
    top : 50%;
    position : absolute;
    z-index : 101;
    width : 32px;
    height : 32px;
    margin-left : -16px;
    margin-top : -16px;
}
div.content {
   width : 1000px;
   height : 1000px;
}
</style>
<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Download</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="form-group">
                      <div class="row" style="margin-left:-30px;">
                        <div class="col-lg-12" style="margin-left:0px;">
                          <div class="col-lg-3">
                          </div>
                          <div class="col-lg-3">
                            <label style="float:right;">By University <input type="radio" name="book_type" value="U" class="book_type_radio"></label>
                          </div>
                          <div class="col-lg-3">
                            <label>By Course <input type="radio" name="book_type" value="C" class="book_type_radio" style="border:2px solid red;"></label>
                          </div>
                          <div class="col-lg-3">
                          </div>
                        </div>
                        <div class="col-lg-12" style="margin-left:0px;">
                          <div class="col-lg-4">
                          </div>
                          <div class="col-lg-4">
                            <br/>
                            <select class="form-control book_download_from_student_desk book_selection" style="height:35px;">

                            </select>
                          </div>
                          <div class="col-lg-4">
                          </div>
                        </div>
                      </div>
                    </div>
                      <table class="table table-striped table-hover table-bordered table-condensed book_download_table" style='font-size:12px;display:none;'>
                        <thead>
                          <tr>
                            <th>Sr No.</th>
                            <th>Category</th>
                            <th>Name</th>
                            <th>Last Updated</th>
                            <th>Semester</th>
                            <th>Download</th>
                          </tr>
                        </thead>
                        <tbody class="books_details_download_section">

                        </tbody>
                      </table>
                      <table class="table table-striped table-hover table-bordered table-condensed university_book_download_table" style='font-size:12px;display:none;'>
                        <thead>
                          <tr>
                            <th>Sr No.</th>
                            <th>Category</th>
                            <th>Name</th>
                            <th>Last Updated</th>
                            <th>Semester</th>
                            <th>Download</th>
                          </tr>
                        </thead>
                        <tbody class="university_books_details_download_section">
                          <?php
                               if(!empty($this->session->userdata('student_data_course_taken'))){
                                 if (strpos($this->session->userdata('student_data_course_taken'), 'YCMOU') !== false) {
                                   $i = 1;
                                  foreach($univeristy_books as $books){
                                    $semester = "";
                          					if($books['SEMISTER'] == null){
                          						$semester = "";
                          					}
                          					else{
                          						$semester = $books['SEMISTER'];
                          					}
                                    echo "<tr><td>".($i++)."</td><td>".$books['TYPE']."</td><td>".$books['FILE_NAME']."</td><td>".$books['MODIFIED_DATE']."</td><td>".$semester."</td><td align='center'><span data-file_name=".$books['TITLE']." style='font-size:12px;cursor:pointer' class='download_book_link'><i class='fa fa-download'></i></span></td></tr>";
                                  }
                                 }
                               }?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
