<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Notice Board</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">


              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <?php
                    $stce_notices = array();
                    foreach($notice as $notice){
                      if($notice['UNIVERSITY_ID'] == "1"){
                        array_push($stce_notices,$notice);
                      }
                    }
                    ?>
                    <div>
                      <label style="font-size:20px;font-weight:bold;letter-spacing: 2px;" class="notice_form" data-notice="stce">STCE <span id="saip_span"><i class="fa fa-caret-right"></i></span></label>
                      <div id="saip_notice_form" style="display:none;" data-switch="0">
                        <ul>
                          <?php foreach($stce_notices as $stce){
                            $notice_body = str_replace('<img src="../IMAGES/download.jpg">','<i class="fa fa-download"></i>',$saip['NOTICE_BODY']);
                            $notice_date = str_replace('/', '-',$stce['NOTICEDISPLAY_DATE']);
                            $notice =  date("d/m/Y", strtotime($notice_date) );
                            echo "<li style='color:rgb(237,170,30);font-size:14px;' class='notice_title' id=".$saip['NOTICEID']."><label>".$saip['NOTICE_TITLE']." <sup style='font-size:10px;color:black;'>".$notice."</sup></label></li>";
                            echo "<div id='".$saip['NOTICEID']."_body' class='notice_body' style='display:none;' data-switch='0'>".$notice_body."</div>";
                          }?>
                        </ul>
                      </div>
                    </div>
                    <!--<div>-->
                    <!--  <label style="font-size:20px;font-weight:bold;letter-spacing: 2px;" class="notice_form" data-notice="ycmou">YCMOU <span id="ycmou_span"><i class="fa fa-caret-right"></i></span></label>-->
                    <!--  <div id="ycmou_notice_form" style="display:none;" data-switch="0">-->
                    <!--    <ul>-->
                          <?php //foreach($ycmou_notices as $ycmou){
                            // $notice_body = str_replace('<img src="../IMAGES/download.jpg">','<i class="fa fa-download"></i>',$ycmou['NOTICE_BODY']);
                            // $notice_date = str_replace('/', '-',$ycmou['NOTICEDISPLAY_DATE']);
                            // $notice =  date("d/m/Y", strtotime($notice_date) );
                            // echo "<li style='color:rgb(237,170,30);font-size:14px;' class='notice_title' id=".$ycmou['NOTICEID']."><label>".$ycmou['NOTICE_TITLE']." <sup style='font-size:10px;color:black;'>".$notice."</sup></label></li>";
                            // echo "<div id='".$ycmou['NOTICEID']."_body' class='notice_body' style='display:none;' data-switch='0'>".$notice_body."</div>";
                          //}?>
                    <!--    </ul>-->
                    <!--  </div>-->
                    <!--</div>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>