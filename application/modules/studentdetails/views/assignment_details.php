<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Assignment Details</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <label style="color:red">*Note: File should be (gif, jpg, png, jpeg, pdf, doc, zip, rar) format only.</label>
                    <table class='table table-bordered table-condensed' style='font-size:12px;border:none;'>
                      <tr style="font-weight:bold;">
                        <td>Batch Id</td>
                        <td>Faculty Name</td>
                        <td>Assignment Date</td>
                        <td>Assignment Topic</td>
                        <td>Assignment Description</td>
                        <td>Assignment Attachments</td>
                        <td>Last Date Of Submission</td>
                        <td>Course Name</td>
                        <td>Upload Assignment</td>
                      </tr>
                        <?php
                          foreach($all_assignments as $assignment){
                              $date = str_replace('/', '-',$assignment['ASSIGNMENT_DATE']);
                              $assignment_date =  date("d/m/Y", strtotime($date) );
                              $last_date = str_replace('/', '-',$assignment['LAST_DATE']);
                              $assignment_last_date =  date("d/m/Y", strtotime($last_date) );
                              if($assignment['ATTACHMENTS_FOR_REFERENCE'] != ""){
                                $link = "<a href= 'http://saintangelos.com:8282/sapeerp/get_assignment_or_projects_attachments/".$assignment['ATTACHMENTS_FOR_REFERENCE']."' target='_blank' style='text-align:center;'><i class='fa fa-2x fa-download'></i></a>";
                              }
                              else{
                                $link = "";
                              }
                              echo "<tr>";
                              echo "<td>".$assignment['BATCH_ID']."</td>";
                              echo "<td>".$assignment['Employee']."</td>";
                              echo "<td>".$assignment_date."</td>";
                              echo "<td>".$assignment['ASSIGNMENT_TOPIC']."</td>";
                              echo "<td>".$assignment['ASSIGNMENT_DESCRIPTION']."</td>";
                              echo "<td>".$link."</td>";
                              echo "<td>".$assignment_last_date."</td>";
                              echo "<td>".$assignment['SUBJECT']."</td>";
                              echo "<td><form enctype='multipart/form-data' method='post'><input type='hidden' name='ASSIGNMENT_FEEDING_MASTER_ID' value='".$assignment['ASSIGNMENT_ID']."'><input type='hidden' name='".$this->security->get_csrf_token_name()."' value='".$this->security->get_csrf_hash()."'><input type='file' name='upload_assignment' id='assignment_".$assignment['ASSIGNMENT_ID']."' data-assignment_id='".$assignment['ASSIGNMENT_ID']."'><button type='submit' class='assignment_upload' id='".$assignment['ASSIGNMENT_ID']."' name='assignment_submit'>Upload</button></form></td>";
                              echo "</tr>";
                          }
                        ?>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
