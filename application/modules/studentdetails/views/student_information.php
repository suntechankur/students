<div class="container">
      	<div class="row">
  			<div class="col-md-12">
  				<div class="panel panel-login">
            <div class="panel-heading">
              <div class="row"><!-- other content --><br/></div><br/>
              <div class="row">
                <div class="col-lg-9">
    							<div class="col-xs-4">
    								<a href="javascript:void(0)" class="student_desk_tabs active" id="basic-details-link" data-name="basic-details">Student details</a>
    							</div>
    							<div class="col-xs-4">
                    <a href="javascript:void(0)" class="student_desk_tabs" id="batch-details-link" data-name="batch-details">Batch Details</a>
    							</div>
    							<div class="col-xs-4">
                    <a href="javascript:void(0)" class="student_desk_tabs" id="update-details-link" data-name="update-details">Update details</a>
    							</div>
                </div>
                <div class="col-lg-3"></div>
                <hr>
  						</div>
            </div>
  					<div class="panel-body">
              <div class="row">
                <div class="col-lg-9">
                  <div id="basic-details-form" class="student_desk_forms" style="display: block;">
                    <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="col-lg-12" class="table-responsive">
                      <?php  foreach($studData as $sdata){ ?>
                        <?php echo form_open(); ?>
                            <div class="form-group">
                              <div class="row" style="margin-left:-30px;">
                                <div class="col-lg-12" style="margin-left:0px;text-align:center;">
                                  <?php
                                    $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                                         $courseNames = '';
                                         //echo '<ul class="list-group">';
                                          for($j=0;$j<count($courses);$j++){
                                              if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                                    $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                                  $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                                }

                                          }
                                         //echo '</ul>';
                                    ?>
                                    <?php
                                    $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                                         $moduleNames = '';
                                         //echo '<ul class="list-group">';
                                          for($j=0;$j<count($modules);$j++){
                                              if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                                    $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                                  $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                                }

                                          }
                                         //echo '</ul>';
                                         $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
                                         $data['COURSE_TAKEN'] = $sdata['COURSE_TAKEN'];
                                         $this->session->set_userdata('student_data_course_taken', $data['COURSE_TAKEN']);
                                         // unset($sdata['MODULE_TAKEN']);
                                    // echo "<pre>";
                                    // print_r($modules);
                                    // echo "</pre>";
                                    ?>
                                    <table class='table table-bordered table-condensed' style='font-size:12px;border:none;'>
                                      <tr>
                                        <td align="right"  style="border:none;"><label>Addmission Id:</label></td><td style="border:none;"><label><?php echo $sdata['ADMISSION_ID']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Name:</label></td><td style="border:none;"><label><?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Contact Details:</label></td><td style="border:none;"><label><?php echo $sdata['ENQUIRY_MOBILE_NO']." / ".$sdata['ENQUIRY_PARENT_NO']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Email:</label></td><td style="border:none;"><label><?php echo $sdata['ENQUIRY_EMAIL']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Centre:</label></td><td style="border:none;"><label><?php echo $sdata['CENTRE_NAME']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Admission Date:</label></td><td style="border:none;"><label><?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Course Taken:</label></td><td style="border:none;"><label><?php echo $sdata['COURSE_TAKEN']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Admission Type:</label></td><td style="border:none;"><label><?php if($sdata['ADMISSION_TYPE'] == "1"){echo "Regular";}else{echo $sdata['ADMISSION_TYPE'];}?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Course Fees:</label></td><td style="border:none;"><label>&#8377 <?php echo $sdata['TOTALFEES']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Fees Paid:</label></td><td style="border:none;"><label>&#8377 <?php echo $sdata['FEE_PAID']?></label></td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="border:none;"><label>Balance Fees:</label></td><td style="border:none;"><label>&#8377 <?php echo $sdata['TOTALFEES']-$sdata['FEE_PAID']?></label></td>
                                      </tr>
                                    </table>
                                    <p align="left"><label>Fees Payment Details</label></p>
                                    <table class="table table-striped table-hover table-bordered table-condensed" style='font-size:12px;'>
                          						<thead>
                          							<tr>
                          								<td><label>Receipt No</label></td>
                          								<td><label>Token No</label></td>
                          								<td><label>Date</label></td>
                          								<td><label>Amount Paid</label></td>
                          								<td><label>Payment Type</label></td>
                          								<td><label>Balance</label></td>
                          							</tr>
                          						</thead>
                          						<tbody style="font-size:11px;">
                          						<?php foreach($receiptDetails as $receiptData){?>
                          							<tr>
                          								<td><?php echo $receiptData['ADMISSION_RECEIPT_NO']?></td>
                          								<td><label></td>
                          								<td><?php $date=date_create($receiptData['PAYMENT_DATE']); echo date_format($date,"d/m/Y")?></td>
                          								<td><?php echo $receiptData['AMOUNT_PAID']?></td>
                          								<td>
                          									<?php if($receiptData['PAYMENT_TYPE'] == "CASH" || $receiptData['PAYMENT_TYPE'] == "1"){
                          											echo "CASH";
                          										}
                          										else{
                          											$date=date_create($receiptData['CHEQUE_DATE']);
                                                $payment_type = $receiptData['PAYMENT_TYPE'];
                                                if($receiptData['PAYMENT_TYPE'] == "2"){
                                                  $payment_type = "UPI";
                                                }
                                                if($receiptData['PAYMENT_TYPE'] == "3"){
                                                  $payment_type = "CHEQUE";
                                                }
                          											echo $payment_type."(".$receiptData['CHEQUE_NO'].", ".date_format($date,"d/m/Y").", ".$receiptData['CHEQUE_BANK_NAME'].")";
                          										}
                          									?>
                          								</td>
                          								<td><?php echo $receiptData['BALANCES']?></td>
                          							</tr>
                          						<?php } ?>
                          						</tbody>
                      	        		</table>
                                    <p align="left"><label>Other Fees Payment Details</label></p>
                                    <table class="table table-striped table-hover table-bordered table-condensed" style='font-size:12px;'>
                          						<thead>
                          							<tr>
                          								<td><label>Receipt No</label></td>
                          								<td><label>Date</label></td>
                          								<td><label>Particular</label></td>
                          								<td><label>Amount Paid</label></td>
                          								<td><label>Payment Type</label></td>
                          							</tr>
                          						</thead>
                          						<tbody style="font-size:11px;">
                                        <?php foreach($otherReceipts as $otherReceipt){?>
                            							<tr>
                            								<td><?php echo $otherReceipt['PENALTY_RECEIPT_NO']?></td>
                            								<td><?php $date=date_create($otherReceipt['PAYMENT_DATE']); echo date_format($date,"d/m/Y")?></td>
                            								<td><?php echo $otherReceipt['PARTICULAR']?></td>
                            								<td><?php echo $otherReceipt['AMOUNT']?></td>
                            								<td>
                                              <?php if($otherReceipt['PAYMENT_TYPE'] == "CASH" || $otherReceipt['PAYMENT_TYPE'] == "1"){
                            											echo "CASH";
                            										}
                            										else{
                            											$date=date_create($otherReceipt['CHEQUE_DATE']);
                                                  $payment_type = $otherReceipt['PAYMENT_TYPE'];
                                                  if($otherReceipt['PAYMENT_TYPE'] == "2"){
                                                    $payment_type = "UPI";
                                                  }
                                                  if($otherReceipt['PAYMENT_TYPE'] == "3"){
                                                    $payment_type = "CHEQUE";
                                                  }
                            											echo $payment_type."(".$otherReceipt['CHEQUE_NO'].", ".date_format($date,"d/m/Y").", ".$otherReceipt['BANK_NAME'].")";
                            										}
                            									?>
                                            </td>
                            							</tr>
                            						<?php } ?>
                          						</tbody>
                      	        		</table>
                                  </div>
                                </div>
                              </div>
                             <?php echo form_close(); ?>
                        <?php }?>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">&nbsp;</div>
                </div>
                <div class="row">
                  <div class="col-lg-9">
                    <div id="batch-details-form" class="student_desk_forms" style="display: none;">
                      <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                      <div class="col-lg-12" class="table-responsive">
                        <table class="table table-striped table-hover table-bordered table-condensed" style='font-size:12px;'>
                          <thead>
              							<tr>
              								<th>Batch Id</th>
              								<th>Subject</th>
              								<th>Centre</th>
              								<th>Faculty</th>
              								<th>Timing</th>
              								<th>Start Date</th>
              								<th>End Date</th>
              								<th>Attendance</th>
              								<!-- <th>Exam Date</th>
              								<th>Assignments/Journals Marks</th>
              								<th>Online Exam Marks</th>
              								<th>Total Marks</th>
              								<th>%</th>
              								<th>Result</th> -->
              							</tr>
              						</thead>
              						<tbody>
              						<?php foreach($courseStatus as $cStatus){?>
              							<tr>
              								<td><?php echo $cStatus['BATCHID']?></td>
              								<td><?php echo $cStatus['COURSE_NAME']?></td>
              								<td><?php echo $cStatus['CENTRE_NAME']?></td>
              								<td>
              								<?php if(($cStatus['FACULTY_NAME'] == "") || ($cStatus['FACULTY_NAME'] == null)){
              										echo $cStatus['EMP_FNAME']." ".$cStatus['EMP_MIDDLENAME']." ".$cStatus['EMP_LASTNAME'];
              									}
              									else{
              										echo $cStatus['FACULTY_NAME'];
              									}?>
              								</td>
              								<td><?php echo $cStatus['DAYS'].", ".$cStatus['STARTTIME']." to ".$cStatus['ENDTIME']?></td>
              								<td><?php $date=date_create($cStatus['STARTDATE']); echo date_format($date,"d/m/Y")?></td>
              								<td><?php $date1=date_create($cStatus['ACTUALENDDATE']); echo date_format($date1,"d/m/Y")?></td>
              								<td><?php echo $cStatus['PRESENT']."/ ".$cStatus['TOTALNOBATCH']?></td>
              								<!-- <td><a href="">Print</a></td>
              								<td></td>
              								<td></td>
              								<td></td>
              								<td></td>
              								<td></td> -->
              							</tr>
              						<?php } ?>
              						</tbody>
          	        		</table>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">&nbsp;</div>
                </div>
                <div class="row">
                  <div class="col-lg-9">
                    <div id="update-details-form" class="student_desk_forms" style="display: none;">
                      <div class="col-lg-12" id="update_student_details" class="table-responsive">
                        <?php echo form_open();?>
                        <div class="col-sm-6">
                          <?php  foreach($studData as $sdata){ ?>
                          <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                          <input type="hidden" name="ENQUIRY_ID" value="<?php echo $sdata['ENQUIRY_ID']?>">
        									<div class="form-group">
        										<label>Email Id. : </label><input type="email" name="ENQUIRY_EMAIL" tabindex="1" class="form-control" placeholder="Email ID" value="<?php echo $sdata['ENQUIRY_EMAIL']?>">
        									</div>
        									<div class="form-group">
        										<label>Contact details  : </label>
                            <hr>
        										<label>Mobile No. : <?php echo $sdata['ENQUIRY_MOBILE_NO']?></label><br/><label>Parent Mobile : <?php echo $sdata['ENQUIRY_PARENT_NO']?></label>
        									</div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <p>&nbsp;</p><p>&nbsp;</p>
        									</div>
                        </div>
                      <?php } ?>
                        <label style="font-size:14px;color:red;">*Note : Kindly contact your centre to update your contact and address details.</label>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-sm-2 col-sm-offset-4">
                              <input type="submit" name="update_details" tabindex="2" class="btn btn-login" value="Update" style="border-radius:0px;background:rgb(237,170,30);color: #fff;font-size: 12px;height: auto;font-weight: bold;padding: 10px 20px;border-color: #000">
                            </div>
                          </div>
                        </div>
                        <?php echo form_close();?>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">&nbsp;</div>
                </div>
              </div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>