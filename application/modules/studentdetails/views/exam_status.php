<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    padding-top: 10px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: red;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #F00;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #F00;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #FE3B3B;
	border-color: #FE3B3B;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.questions {
  font-weight: bold;
  color: #00415d;
}

.question-button{
border-radius: 100%;
border: 1px solid;
height: 30px;
padding: 4px 6px;
width: 30px;
text-align: center;
cursor: pointer;
background: #ff2e35;
color: #fff;
font-weight: bold;
-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}

.submit_test{
  border-radius: 10px;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.next_question,
.prev_question {
  border-radius: 100%;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}

.submit_test:hover,
.question-button:hover,
.next_question:hover,
.prev_question:hover {background-color: #e80007}

.submit_test:active,
.question-button:active,
.next_question:active,
.prev_question:active {
  background-color: #ff2e35;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  transform: translateY(4px);
}

</style>

<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height:50px;"><br/></div>
            <div class="row">
              <div class="col-xs-4">
              </div>
              <div class="col-xs-4">
                <h1>Exam Result</h1>
              </div>
              <div class="col-xs-4">
              </div>
            </div>
            <hr>
          </div>
					<div class="panel-body">
						<div class="row">
              <input class="examToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="hidden" id="exam-session-details" course_id="<?php echo $this->session->userdata('exam_details')[0]['course_id'];?>" exam_id="<?php echo $this->session->userdata('exam_details')[0]['examid'];?>">
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                      <h3>Indications</h3>
                      <label>Status Bar : </label>
                      <table>
                        <tr><td style="color:white;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:#299f37"></td><td> - Pass</td></tr>
                        <tr><td style="color:white;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:#e31e24"></td><td> - Fail</td></tr>
                      </table>
                      <br/>
                      <label>Question summary : </label>
                      <table>
                        <tr><td style="color:white;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:#299f37"></td><td> - Right</td></tr>
                        <tr><td style="color:white;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:#e31e24"></td><td> - Wrong</td></tr>
                        <tr><td colspan=2></td></tr>
                        <tr><td colspan=2>In case of time up</td></tr>
                        <tr><td colspan=2></td></tr>
                        <tr><td style="color:white;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:blue"></td><td> - Unattempted</td></tr>
                      </table>
                    </div>
                    <div class="col-sm-6 text-center">
                        <label>Student Name : <?php echo $this->session->userdata('exam_details')[0]['studentname'];?></label>
                        <br/>
                        <label>Course : <?php echo $this->session->userdata('exam_details')[0]['course'];?></label>
                        <br/>
                        <label>Date : <?php echo date('d/m/Y');?></label>
                        <br/>
                        <label>Regularity/Assignments Marks : <?php echo $this->session->userdata('exam_details')[0]['internal_marks'].'/'.$this->session->userdata('exam_details')[0]['internal_out_of'];?></label>
                        <br/>
                        <label>Practical/Projects Marks : <?php echo $this->session->userdata('exam_details')[0]['project_marks'].'/'.$this->session->userdata('exam_details')[0]['project_marks_out_of'];?></label>
                        <br/>
                        <?php
                              $marks = $this->session->userdata('exam_details')[0]['mark_obtained'];
                              $outof = $this->session->userdata('exam_details')[0]['out_of'];
                              $percentage = $marks/$outof*100;
                              $round_percentage = round($percentage);
                              $background_color = "background-color:#e31e24;";
                              $status = "Fail";
                              if($round_percentage >= 40){
                                $background_color = "background-color:#299f37;";
                                $status = "Pass";
                              }
                        ?>
                        <label>Theory/Online Marks : <?php echo $this->session->userdata('exam_details')[0]['mark_obtained'].'/'.$this->session->userdata('exam_details')[0]['out_of'].'  ('.$round_percentage.'%)';?></label>
                        <br/>
                        <label>Status</label>
                        <div class="progress">
                         <div class="progress-bar" role="progressbar" aria-valuenow="70"
                         aria-valuemin="0" aria-valuemax="100" style="font-weight:bold;width:<?php echo round($percentage).'%;'.$background_color;?>">
                          <?php echo round($percentage).'%';?>
                         </div>
                        </div>
                        <br/>
                        <label>Result : <?php echo $status;?></label>
                        <table style="border-collapse: collapse;" align="center">
                          <?php
                            for($i=0;$i<count($this->session->userdata('exam_status_data'));$i++){
                              $color = $this->session->userdata('exam_status_data')[$i]['color'];
                              $qno = $this->session->userdata('exam_status_data')[$i]['Qno'];
                              $start = array(1,11,21,31,41,51,61,71,81,91);
                              $end = array(10,20,30,40,50,60,70,80,90,100);
                              $breaks = $i+1;
                              if(in_array($breaks,$start)){
                                echo "<tr>";
                              }
                              echo "<td style='font-weight:bold;border: 1px solid #dddddd;text-align: center;padding: 8px;background-color:".$color."'>".$qno."</td>";
                              if(in_array($breaks,$end)){
                                echo "</tr>";
                              }
                            }
                          ?>
                        </table>
                        <?php
                          $this->session->unset_userdata('exam_details');
                          $this->session->unset_userdata('exam_status_data');
                          $this->session->sess_destroy();
                        ?>
                        <br/>
                        <button class='submit_test' type='submit' title='Close window' onclick="window.close();">Close</button>
                    </div>
                    <div class="col-sm-3"></div>
                  </div>
                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
