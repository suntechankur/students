<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Batch Requirement</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <?php echo form_open();?>
                    <div class="col-lg-12" id="update_student_details" class="table-responsive">
                      <div class="col-sm-4">
                        <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <?php
                        $centre_id = "";
                        $admission_id = "";
                        $name = "";
                        foreach($studData as $data){
                              $centre_id = $data['CENTRE_ID'];
                              $admission_id = $data['ADMISSION_ID'];
                              $name = $data['ENQUIRY_FIRSTNAME']." ".$data['ENQUIRY_LASTNAME'];
                        }?>
                        <input type="hidden" name="ADMISSION_ID" value="<?php echo $admission_id; ?>">
                        <input type="hidden" name="CENTRE_NAME" value="<?php echo $centre_id; ?>">
                        <input type="hidden" name="STUDENT_NAME" value="<?php echo $name; ?>">
                        <div class="form-group">
                          <label>Student Email : </label><input type="email" name="STUDENT_EMAIL" tabindex="1" class="form-control" placeholder="Email Id">
                        </div>
                        <div class="form-group">
                          <label>Preffered Batch Time 1 : </label>
                          <select name="PREFERRED_BAND_FROM1" tabindex="3" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="8.00 A.M ">8.00 A.M </option>
                    				<option value="9.00 A.M ">9.00 A.M </option>
                    				<option value="10.00 A.M">10.00 A.M</option>
                    				<option value="11.00 A.M">11.00 A.M</option>
                    				<option value="12.00 P.M">12.00 P.M</option>
                    				<option value="1.00 P.M ">1.00 P.M </option>
                    				<option value="2.00 P.M">2.00 P.M</option>
                    				<option value="3.00 P.M">3.00 P.M</option>
                    				<option value="4.00 P.M">4.00 P.M</option>
                    				<option value="5.00 P.M">5.00 P.M</option>
                    				<option value="6.00 P.M">6.00 P.M</option>
                    				<option value="7.00 P.M">7.00 P.M</option>
                    				<option value="8.00 P.M">8.00 P.M</option>
                    				<option value="9.00 P.M">9.00 P.M</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Preffered Batch Time 2 : </label>
                          <select name="PREFERRED_BAND_FROM2" tabindex="5" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="8.00 A.M ">8.00 A.M </option>
                    				<option value="9.00 A.M ">9.00 A.M </option>
                    				<option value="10.00 A.M">10.00 A.M</option>
                    				<option value="11.00 A.M">11.00 A.M</option>
                    				<option value="12.00 P.M">12.00 P.M</option>
                    				<option value="1.00 P.M ">1.00 P.M </option>
                    				<option value="2.00 P.M">2.00 P.M</option>
                    				<option value="3.00 P.M">3.00 P.M</option>
                    				<option value="4.00 P.M">4.00 P.M</option>
                    				<option value="5.00 P.M">5.00 P.M</option>
                    				<option value="6.00 P.M">6.00 P.M</option>
                    				<option value="7.00 P.M">7.00 P.M</option>
                    				<option value="8.00 P.M">8.00 P.M</option>
                    				<option value="9.00 P.M">9.00 P.M</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Preffered First Location : </label>
                          <select name="PREFERRED_LOCATION1" tabindex="7" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="Kandivali(w)">Kandivali(w)</option>
                            <option value="Thane">Thane</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Student Contact No : </label><input type="text" name="STUDENT_CONTACT" tabindex="2" class="form-control" placeholder="Contact No.">
                        </div>
                        <div class="form-group">
                          <label>to </label>
                          <select name="PREFERRED_BAND_TO1" tabindex="4" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="8.00 A.M ">8.00 A.M </option>
                    				<option value="9.00 A.M ">9.00 A.M </option>
                    				<option value="10.00 A.M">10.00 A.M</option>
                    				<option value="11.00 A.M">11.00 A.M</option>
                    				<option value="12.00 P.M">12.00 P.M</option>
                    				<option value="1.00 P.M ">1.00 P.M </option>
                    				<option value="2.00 P.M">2.00 P.M</option>
                    				<option value="3.00 P.M">3.00 P.M</option>
                    				<option value="4.00 P.M">4.00 P.M</option>
                    				<option value="5.00 P.M">5.00 P.M</option>
                    				<option value="6.00 P.M">6.00 P.M</option>
                    				<option value="7.00 P.M">7.00 P.M</option>
                    				<option value="8.00 P.M">8.00 P.M</option>
                    				<option value="9.00 P.M">9.00 P.M</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>to </label>
                          <select name="PREFERRED_BAND_TO2" tabindex="6" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="8.00 A.M ">8.00 A.M </option>
                    				<option value="9.00 A.M ">9.00 A.M </option>
                    				<option value="10.00 A.M">10.00 A.M</option>
                    				<option value="11.00 A.M">11.00 A.M</option>
                    				<option value="12.00 P.M">12.00 P.M</option>
                    				<option value="1.00 P.M ">1.00 P.M </option>
                    				<option value="2.00 P.M">2.00 P.M</option>
                    				<option value="3.00 P.M">3.00 P.M</option>
                    				<option value="4.00 P.M">4.00 P.M</option>
                    				<option value="5.00 P.M">5.00 P.M</option>
                    				<option value="6.00 P.M">6.00 P.M</option>
                    				<option value="7.00 P.M">7.00 P.M</option>
                    				<option value="8.00 P.M">8.00 P.M</option>
                    				<option value="9.00 P.M">9.00 P.M</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Preffered Second Location : </label>
                          <select name="PREFERRED_LOCATION2" tabindex="8" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <option value="Kandivali(w)">Kandivali(w)</option>
                            <option value="Thane">Thane</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="col-sm-8">
                        <div class="form-group">
                          <label>Subject Name : </label>
                          <select name="COURSE_TAKEN" tabindex="9" class="form-control">
                            <option selected="selected" disabled="disabled">Select</option>
                            <?php foreach($courses as $course){
                              echo "<option value=".$course['COURSE_NAME'].">".$course['COURSE_NAME']."</option>";
                            }?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 col-sm-offset-3">
                        <div class="form-group">
                          <input type="submit" name="send_requirement" tabindex="10" class="btn btn-login" value="Submit" style="border-radius:0px;background:rgb(237,170,30);color: #fff;font-size: 12px;height: auto;font-weight: bold;padding: 10px 20px;border-color: #000">
                        </div>
                      </div>
                    </div>
                    <?php echo form_close();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>