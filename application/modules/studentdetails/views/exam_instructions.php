<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    padding-top: 90px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: red;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #F00;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #F00;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #FE3B3B;
	border-color: #FE3B3B;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.submit_test{
  border-radius: 10px;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: #ff2e35;
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  -moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
</style>

<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height:50px;"><br/></div>
            <div class="row">
              <div class="col-xs-4">
              </div>
              <div class="col-xs-4">
                <h1>Exam Instructions</h1>
              </div>
              <div class="col-xs-4 float-right">
                <a href="<?php echo  base_url('logout');?>"><button class='submit_test' type='submit' title='Close window'>Exit</button></a>
              </div>
            </div>
            <hr>
          </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-9 col-md-offset-3">
                <!-- <form id="start-exam-submit" method="post" role="form"> -->
                  <div class="form-group">
                    <label>Student Name : </label><?php echo " ".$this->session->userdata('exam_details')[0]['studentname']?>
                    <br/>
                    <label>Subject Name : </label><?php echo " ".$this->session->userdata('exam_details')[0]['course']?>
                    <br/>
                    <label>Centre Name : </label><?php echo " ".$this->session->userdata('exam_details')[0]['centre']?>
                  </div>
                  <div class="form-group">
                        <span style="font-size: 10pt"><strong><span style="text-decoration: underline">
                            <br />
                            Instructions Read Carefully</span></strong> </span>

                        <div style="width:590px;height:272px; overflow:scroll; word-spacing:3px; line-height:20px;">
                          <br />
                          <ol>
                              <li style="text-align: justify">You will have
                                  <strong><?php echo $qpattern_details[0]['Duration'];?> Minutes</strong> to complete
                                  the exam.
                              </li>
                              <li style="text-align: justify">There are total <?php echo $qpattern_details[0]['Total'];?> questions each carries 1 mark.&nbsp;</li><li style="text-align: justify"><strong>Passing score will be 40% of total marks.
                              </strong></li>
                              <li style="text-align: justify">Your score and exam review will display in a summary
                                  report immediately after the exam.</li><li style="text-align: justify">Your certificates, if applicable, will
                                  given to you by your respective centre. Also You will receive your certificate after 15 days of your exam/module completion.
                              </li>
                              <li style="text-align: justify"><strong>Select the appropriate answer for each question then
                                  click Next icon (">").
                              </strong></li>
                              <li style="text-align: justify"><strong>Click on Previous to goto previous questions icon ("<").
                              </strong></li>
                              <li style="text-align: justify"><strong>Check your time on the clock in the top right
                                  corner of the exam window.
                              </strong></li>
                              <li style="text-align: justify">The candidate must show, on demand, the I-Card, Hall Ticket for
                                  admission in the examination rooms/hall. A candidate who does not possess the I-Card issued by the Board shall not be permitted for the examination under any circumstances
                                  by the Centre Superintendent.</li><li style="text-align: justify">Candidates are not allowed to carry any textual material,
                                  Calculators,Docu Pen, Slide Rules, Log Tables, Electronic Watches with facilities
                                  of calculator, printed or written material, bits of papers, mobile phone, pager
                                  or any other device, except the Admit Card inside the Examination Room/Hall. If
                                  any candidate is in possession of any of the above item, his/her candidature will
                                  be treated as unfairmeans and cancel the current examination &amp; also debarred
                                  the candidate for future examination(s).</li><li style="text-align: justify">No candidate, without the special permission of the
                                  Centre Superintendent or the Invigilator concerned, will leave his/her seat or Examination
                                  Room until the full duration of the paper is over. Candidates should not leave the
                                  room/hall without handing over their Answer Sheets to the Invigilator on duty.</li><li style="text-align: justify">Tea, coffee, cold drinks or snacks are not allowed to
                                  be taken into the examination rooms during examination hours.</li><li style="text-align: justify">During the examination time, the invigilator will check
                                  Admit Card of the candidate to satisfy himself/herself about the identity of each
                                  candidate. The invigilator will also put his/her signatures in the place provided
                                  in the Answer Sheet on Side-1.
                              </li>
                              <li>
                                  <p align="justify" mjijy="0" phxha="0" style="text-align: justify">
                                      Candidates shall maintain perfect silence and attend to their Question Paper only.
                                      Any conversation or gesticulation or disturbance in the Examination Room/Hall shall
                                      be deemed as misbehaviour. If a candidate is found using unfair means or impersonating,
                                      his/her candidature shall be cancelled and he/she will be liable to be debarred
                                      for taking examination either permanently or for a specified period according to
                                      the nature of offence.
                                      If any candidate is in possession of any item(s) as mentioned in para 5 above, his/her
                                      candidature for current examination will be cancelled and also liable to be debarred
                                      for future examination(s). <font face="Verdana" size="1"></font>
                                  </p>
                               </li>
                               <li style="text-align: justify"><strong>Click on the Start button to start and launch
                                   the exam questions.
                               </strong></li>
                          </ol>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-4 col-sm-offset-2">
                        <input type="submit" name="start-exam" id="start-exam" class="form-control btn btn-login" value="Start Exam">
                      </div>
                    </div>
                  </div>
                <!-- </form> -->
                <input type="hidden" value=<?php echo $start_exam_link ?> id="link-to-start-exam">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <script>
    $('document').ready(function(){
      $("#start-exam").click(function(){
        var href = $("#link-to-start-exam").val();

        var newwindow = window.open(href, null, 'fullscreen=yes,status=no,toolbar=no,menubar=no,location=no,titlebar=no');

        if (window.focus) {
            newwindow.focus();
        }
        $("#start-exam").attr('disabled',true);
        return false;
    });
  });
  </script>
