<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    padding-top: 10px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	-moz-box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(255, 0, 0);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: red;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #F00;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #F00;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #FE3B3B;
	border-color: #FE3B3B;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}
</style>

<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-login">
					<div class="panel-heading">
            <div class="row"><br/><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height:50px;"><br/><br/></div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
                <form action="" method="post" role="form">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <embed src="<?php echo base_url('resources/'); ?>StudentInstructionManualV17.pdf" type="application/pdf" width="100%" height="600px" />
                    <div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
                          <br><br>
  												<input type="submit" name="accept_terms" id="accept_condition" tabindex="4" class="form-control btn btn-login" value="Accept">
                          <br>
  											</div>
  										</div>
  									</div>
                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
