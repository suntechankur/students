<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs active" data-name="tab_1">Edutalk 1</a>
                </div>
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs" data-name="tab_2">Edutalk 2</a>
                </div>
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs" data-name="tab_3">Edutalk 3</a>
                </div>
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs" data-name="tab_4">Edutalk 4</a>
                </div>
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs" data-name="tab_5">Edutalk 5</a>
                </div>
                <div class="col-xs-2">
                  <a href="javascript:void(0)" class="student_desk_tabs" data-name="tab_6">Edutalk 6</a>
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms" style="display:block;" id="tab_1-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_1.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                  <div class="student_desk_forms" style="display:none;" id="tab_2-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_2.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                  <div class="student_desk_forms" style="display:none;" id="tab_3-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_3.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                  <div class="student_desk_forms" style="display:none;" id="tab_4-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_4.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                  <div class="student_desk_forms" style="display:none;" id="tab_5-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_5.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                  <div class="student_desk_forms" style="display:none;" id="tab_6-form">
                    <embed src="<?php echo base_url('resources/Edutalks/'); ?>Edutalk_6.pdf" type="application/pdf" width="100%" height="1330px" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
