<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active" id="exam-details-link" data-name="exam-details">Exam Details details</a>
                </div>
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs" id="practical-exam-link" data-name="practical-exam">Practical Exam Booking</a>
                </div>
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs" id="theory-exam-link" data-name="theory-exam">Theory Exam Booking</a>
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-9">
                <div id="exam-details-form" class="student_desk_forms" style="display: block;">
                  <u><h3>Practical Exam Details</h3></u>
                  <table class="table table-striped table-hover table-bordered table-condensed" style='font-size:12px;'>
                    <thead>
                      <tr>
                        <th>Exam Id</th>
                        <th>Exam Date</th>
                        <th>Subject</th>
                        <th>Internal Exam / Project Marks</th>
                        <th>Percentage</th>
                        <th>Result</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($examStatus as $eStatus){
                          if($eStatus['EXAM_TYPE'] == "PRACTICAL"){

                          $marks = $eStatus['INTERNAL_MARKS'] + $eStatus['EXTERNAL_MARKS'];
                          $out_of = $eStatus['INTERNAL_OUT_OF'] + $eStatus['EXTERNAL_OUT_OF'];

                          if(($eStatus['INTERNAL_MARKS'] == 0) || ($eStatus['INTERNAL_MARKS'] == "") || ($eStatus['INTERNAL_MARKS'] == null)){
                            $eStatus['INTERNAL_MARKS'] = 0;
                            $internal_percentage  = 0;
                          }
                          elseif(($eStatus['INTERNAL_OUT_OF'] == 0) || ($eStatus['INTERNAL_OUT_OF'] == "") || ($eStatus['INTERNAL_OUT_OF'] == null)){
                            $eStatus['INTERNAL_OUT_OF'] = 0;
                            $internal_percentage  = 0;
                          }
                          else{
                            $internal_percentage  = round($eStatus['INTERNAL_MARKS'] / $eStatus['INTERNAL_OUT_OF'] * 100);
                          }

                          if($marks == 0){
                            $percentage = 0;
                          }
                          else{
                            $percentage = round($marks / $out_of * 100);
                          }
                          ?>
                        <tr>
                          <td><?php echo $eStatus['EXAM_ID']?></td>
                          <td><?php $date=date_create($eStatus['EXAM_DATE']); echo date_format($date,"d/m/Y")?></td>
                          <td><?php echo $eStatus['COURSE_NAME']?></td>
                          <td>
                          <?php
                            $color = "green";
                            if($internal_percentage < 40){
                              $color = "red";
                            }

                            if($eStatus['INTERNAL_MARKS'] && $eStatus['INTERNAL_OUT_OF']){
                              echo "<label style='color:".$color."'>".$eStatus['INTERNAL_MARKS']." / ".$eStatus['INTERNAL_OUT_OF']."</label>";
                            }
                            else{
                              echo "-";
                            }
                          ?>
                          </td>
                          <td>
                          <?php if($percentage){
                              echo "<label style='color:".$color."'>".$percentage."%</label>";
                            }
                          ?>
                          </td>
                          <td><?php
                          if($internal_percentage < 40){echo "<label style='color:red;'>Fail</label>";}else{echo "<label style='color:green;'>Pass</label>";}?></td>
                        </tr>
                      <?php } } ?>
                    </tbody>
                  </table>

                  <u><h3>Theory Exam Details</h3></u>
                  <table class="table table-striped table-hover table-bordered table-condensed" style='font-size:12px;'>
                    <thead>
                      <tr>
                        <th>Exam Id</th>
                        <th>Exam Date</th>
                        <th>Time</th>
                        <th>Subject</th>
                        <th>Internal Exam / Project Marks</th>
                        <th>External Exam Marks</th>
                        <th>Percentage</th>
                        <th>Result</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($examStatus as $eStatus){
                          if($eStatus['EXAM_TYPE'] == "THEORY"){
                          $marks = $eStatus['INTERNAL_MARKS'] + $eStatus['EXTERNAL_MARKS'];
                          $out_of = $eStatus['INTERNAL_OUT_OF'] + $eStatus['EXTERNAL_OUT_OF'];

                          if(($eStatus['INTERNAL_MARKS'] == 0) || ($eStatus['INTERNAL_MARKS'] == "") || ($eStatus['INTERNAL_MARKS'] == null)){
                            $eStatus['INTERNAL_MARKS'] = 0;
                            $internal_percentage  = 0;
                          }
                          elseif(($eStatus['INTERNAL_OUT_OF'] == 0) || ($eStatus['INTERNAL_OUT_OF'] == "") || ($eStatus['INTERNAL_OUT_OF'] == null)){
                            $eStatus['INTERNAL_OUT_OF'] = 0;
                            $internal_percentage  = 0;
                          }
                          else{
                            $internal_percentage  = round($eStatus['INTERNAL_MARKS'] / $eStatus['INTERNAL_OUT_OF'] * 100);
                          }

                          if(($eStatus['EXTERNAL_MARKS'] == 0) || ($eStatus['EXTERNAL_MARKS'] == "") || ($eStatus['EXTERNAL_MARKS'] == null)){
                            $eStatus['EXTERNAL_MARKS'] = 0;
                            $external_percentage  = 0;
                          }
                          elseif(($eStatus['EXTERNAL_OUT_OF'] == 0) || ($eStatus['EXTERNAL_OUT_OF'] == "") || ($eStatus['EXTERNAL_OUT_OF'] == null)){
                            $eStatus['EXTERNAL_OUT_OF'] = 0;
                            $external_percentage  = 0;
                          }
                          else{
                            $external_percentage  = round($eStatus['EXTERNAL_MARKS'] / $eStatus['EXTERNAL_OUT_OF'] * 100);
                          }

                          $iColor = "green";
                          $eColor = "green";
                          if($internal_percentage < 40){$iColor = "red";}
                          if($external_percentage < 40){$eColor = "red";}

                          if($marks == 0){
                            $percentage = 0;
                          }
                          else{
                            $percentage = round($marks / $out_of * 100);
                          }
                          ?>
                        <tr>
                          <td><?php echo $eStatus['EXAM_ID']?></td>
                          <td><?php $date=date_create($eStatus['EXAM_DATE']); echo date_format($date,"d/m/Y")?></td>
                          <td><?php echo $eStatus['EXAM_TIME']?></td>
                          <td><?php echo $eStatus['COURSE_NAME']?></td>
                          <td>
                          <?php
                            if($eStatus['INTERNAL_MARKS'] && $eStatus['INTERNAL_OUT_OF']){
                              echo "<label style='color:".$iColor."'>".$eStatus['INTERNAL_MARKS']." / ".$eStatus['INTERNAL_OUT_OF']."</label>";
                            }
                            else{
                              echo "-";
                            }
                          ?>
                          </td>
                          <td>
                            <?php
                              if($eStatus['EXTERNAL_MARKS'] && $eStatus['EXTERNAL_OUT_OF']){
                                echo "<label style='color:".$eColor."'>".$eStatus['EXTERNAL_MARKS']." / ".$eStatus['EXTERNAL_OUT_OF']."</label>";
                              }
                              else{
                                echo "-";
                              }
                            ?>
                          </td>
                          <td>
                          <?php if($percentage){
                              if(($internal_percentage < 40) || ($external_percentage < 40)){
                                echo "<label style='color:red'>".$percentage."%</label>";
                              }
                              else{
                                echo "<label style='color:green'>".$percentage."%</label>";
                              }
                            }
                          ?>
                          </td>
                          <td><?php
                          if(($internal_percentage < 40) || ($external_percentage < 40)){echo "<label style='color:red;'>Fail</label>";}else{echo "<label style='color:green;'>Pass</label>";}?></td>
                        </tr>
                      <?php } } ?>
                    </tbody>
                  </table>

                  <fieldset>
                    <legend style="width: 70px;padding: 5px 15px;margin-bottom:0px;font-size:14px;text-decoration: underline;color:red">Note:</legend>
                    <div class="panel panel-default" style="border:none;">
                      <div class="panel-body" style="font-size:12px;">
                        <ul>
                          <li>Kindly note for exam marks you can check <span style="color:red;font-weight:bold;text-decoration:underline;">"Theory Exam Details"</span> Table for Percentage and Marks bifercation.</li>
                          <li>And at the time of certificate issue remaining marks (out of 10) will also gets added, which may vary your final certificate percentage.</li>
                          <li>Refer below Marks Color Notation as per Individual Marks Percentage
                            <ol>
                              <li><span style="background-color:red;padding:1px 5px 1px 5px;color:white;font-weight:bold;border-radius:20%">Red</span> - Below 40%</li>
                              <li><span style="background-color:green;padding:1px 5px 1px 5px;color:white;font-weight:bold;border-radius:20%">Green</span> - Above 40%</li>
                            </ol>
                          </li>
                          <li>Getting Status <span style="color:green;font-weight:bold;">Pass</span> in individual exam you have to secure atleaset 40% in both exams Practical as well as Theory.</li>
                        </ul>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
              <div class="col-lg-3">&nbsp;</div>
            </div>
              <div class="row">
                <div class="col-lg-9">
                  <div id="practical-exam-form" class="student_desk_forms" style="display: none;">
                      <?php
                      $actual_practical_date = "";
                      $start_practical_date = "";
                      $end_practical_date = "";
                      $end_practical_date2 = "";
                        foreach($examSchedular as $examschedule){
                          if($examschedule['EXAM_TYPE'] == "PRACTICAL"){
                            if($examschedule['STREAM'] == $this->session->userdata('student_data')[0]['STREAM']){
                              $actual_practical_date = date('d/m/Y', strtotime($examschedule['EXAM_DATE']));
                              $start_practical_date = date('d/m/Y', strtotime('-1 month', strtotime($examschedule['EXAM_DATE'])));
                              $end_practical_date = date('d/m/Y', strtotime('-6 day', strtotime($examschedule['EXAM_DATE'])));
                              $end_practical_date2 = date('d/m/Y', strtotime('-5 day', strtotime($examschedule['EXAM_DATE'])));
                            }
                          }
                        }
                        //echo "<pre>";print_r($examSchedular);echo "</pre>";
                      ?>
                    <div class="col-lg-12" class="table-responsive">
                      <fieldset>
              					<legend style="width: 125px;padding: 5px 15px;margin-bottom:0px;font-size:14px;text-decoration: underline;">Instructions :</legend>
                        <input type="hidden" id="practical_date" value="<?php echo $actual_practical_date;?>">
              					<div class="panel panel-default" style="border:none;">
              						<div class="panel-body" style="font-size:12px;">
              							<ul>
                              <li>Online Exam booking is open between  <b id="start_practical_date"><?php echo $start_practical_date;?></b>   and  <b id="end_practical_date" data-date="<?php echo $end_practical_date2;?>"><?php echo $end_practical_date;?></b></li>
                              <li>All the fields are mandatory to select. </li>
                              <li>Student has to confirm all the details, while doing exam booking. for e.g. Admission id, Name, Centre, Course, Fees, Exam subject, Exam centre, Exam date, Exam time etc.</li>
                              <li>In case of Exam Fee Receipt No. is not visible, then student need to contact their respective centre regarding Exam fees.</li>
                            </ul>
                            <label><u>Student has to follow below process to book their exams:</u></label>
                            <ol>
                              <li>Read Instruction Properly.</li>
                              <li>Click on <b>I Accept & Understand</b> button to ​proceed further.</li>
                              <li>Select <b>Exam Fee Receipt No, Exam Subject, Exam Centre, Exam Date etc</b>.</li>
                              <li><b>Select time slot</b> as per seat availability.</li>
                              <li>Confirm all the details and click on <b>Register</b> button.</li>
                              <li>Once you click on register, details sent on your valid E-mail id.</li>
                              <li style="color:red;font-weight:bold;">Identity Card and Hallticket is compulsory, Student will not be allowed in the examination hall.</li>
                              <li>During exam students are not permitted to have wireless communication devices (e.g. cellphones) on their desks under any circumstances. </li>
                              <li>Discipline should be follow by students. </li>
                            </ol>
                            <label><u>Exam Cancellation process:</u></label>
                            <ul>
                              <li>If any <b>Exam(Practical/Theory)</b> is booked and student will remain <b>ABSENT</b> in the exam he/she has to pay <b>Penalty</b> of <b>Rs.1000 Per Subject</b>.</li><br/>
                              <li>If student <b>FAILED</b> in the <b>Exam(Practical/Theory)</b> he/she has to pay <b>RE-EXAMINATION</b> Fee of <b>Rs. 300 Per Subject</b>.</li><br/>
                              <li style="font-weight:bold;">In case if you are facing any difficulties regarding Exam Booking, Subject not displayed</li>
                              <li style="font-weight:bold;">For exam cancellation inform us before 2 days of exam scheduled.</li>
                              <li style="font-weight:bold;">Mail us for both queries on exam@suntechedu.com</li>
                            </ul>
              						</div>
              					</div>
              				</fieldset>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-4 col-sm-offset-4" id="practical-exam-booking-form">
                          </div>
                        </div>
                      </div>
                    </div>
<! ----------------------------------------------------------->
<?php
  foreach($studData as $sdata){
  ?>
<div id="practical_booking_form" style="display:none;">
<div class="panel-body">
  <?php echo form_open(); ?>
    <div class="col-md-12" style="font-size:12px;">

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
            <div class="col-lg-4">
              <label>Addmission Id: <?php echo $sdata['ADMISSION_ID']?></label>
              <input type="hidden" name="ADMISSION_ID" value="<?php echo $sdata['ADMISSION_ID']?>">
              <input type="hidden" name="STREAM" value="<?php echo $sdata['STREAM']?>">
              <input type="hidden" id="practical_exam_form_visible" name="VISIBLE" value="0">
            </div>
            <div class="col-lg-4">
              <label>Full Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
            </div>
            <div class="col-lg-4">
              <label>Centre Name: <?php echo $sdata['CENTRE_NAME']?></label>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
            <div class="col-lg-4">
              <label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
            </div>
            <div class="col-lg-4">
              <?php
                $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                     $courseNames = '';
                      for($j=0;$j<count($courses);$j++){
                          if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                              $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                            }

                      }
                ?>
                <?php
                $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                     $moduleNames = '';
                      for($j=0;$j<count($modules);$j++){
                          if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                              $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                            }

                      }
                     $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
                     unset($sdata['MODULE_TAKEN']);
                ?>
              <label>Course Taken : <?php echo $sdata['COURSE_TAKEN']?></label>
            </div>
            <div class="col-lg-4">
              <label>Admission Type : <?php if($sdata['ADMISSION_TYPE'] == "1"){echo "Regular";}else{echo $sdata['ADMISSION_TYPE'];}?></label>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
              <div class="col-md-4">
                <label>Exam Fee Receipt No : <span class="text-danger">*</span></label>
                <select name="EXAM_FEES_RECEIPT_NO" class="form-control">
                  <option value="">Select Receipt</option>
                  <?php foreach($otherReceipts as $otherreceipt){?>
                    <option value="<?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?>"><?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-8">
                <?php $examdata = array();
                foreach($examdetails as $exam){
                  if($exam['EXAM_TYPE'] == "PRACTICAL"){
                    array_push($examdata,$exam['COURSE_ID']);
                  }
                }
                ?>
                <label>Subject Name : <span class="text-danger">*</span></label>
                <select name="COURSE_ID" class="form-control">
                  <option value="">Exam Subject</option>
                  <?php foreach($courseDetails as $course){
                          if(!(in_array($course['COURSE_ID'], $examdata))){?>
                            <option value="<?php echo $course['COURSE_ID'];?>"><?php echo $course['COURSE_NAME'];?></option>
                  <?php } } ?>
                </select>
              </div>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">

              <div class="col-md-4">
                <label>Exam Centre : <span class="text-danger">*</span></label>
                <select name="CENTRE_ID" class="form-control">
                  <option value="">Select Centre Name</option>
                  <?php foreach($centres as $centre){
                            if(($centre['CENTRE_ID'] == "155") || ($centre['CENTRE_ID'] == "200")){?>
                            <option value="<?php echo $centre['CENTRE_ID'];?>"><?php echo $centre['CENTRE_NAME'];?></option>
                  <?php     }
                          }?>
                </select>
              </div>
              <div class="col-md-4">
                <label>Active Email Id : <span class="text-danger">*</span></label>
                <input type="text" name="EMAIL_ID" class="form-control" style="height:34px;">
              </div>
              <div class="col-md-4">
                <label>Exam Date : <span class="text-danger">*</span></label>
                <?php	$examDate = str_replace('/', '-',$actual_practical_date);
                      $exam_date =  date("Y-m-d", strtotime($examDate) ); ?>
                <input type="text" class="form-control" style="height:34px;" value="<?php echo $actual_practical_date;?>" readonly>
                <input type="hidden" name="EXAM_DATE" class="form-control" value="<?php echo $exam_date;?>">
              </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">

              <div class="col-md-4">
              </div>
              <div class="col-md-4">
                <input class="admtok" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="exam_type" value="practical">
                <button type="submit" name="book_exam" class="btn btn-primary">Register</button>
              </div>

          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
   <?php echo form_close(); ?>
  </div>
<?php }?>
<! ----------------------------------------------------------->
                  </div>
                </div>
                <div class="col-lg-3">&nbsp;</div>
              </div>
              <div class="row">
                <div class="col-lg-9">
                  <div id="theory-exam-form" class="student_desk_forms" style="display: none;">
                    <?php
                    $actual_theory_date = "";
                    $start_theory_date = "";
                    $end_theory_date = "";
                    $end_theory_date2 = "";
                      foreach($examSchedular as $examschedule){
                        if($examschedule['EXAM_TYPE'] == "THEORY"){
                          $actual_theory_date = date('d/m/Y', strtotime($examschedule['EXAM_DATE']));
                          $start_theory_date = date('d/m/Y', strtotime('-25 day', strtotime($examschedule['EXAM_DATE'])));
                          $end_theory_date = date('d/m/Y', strtotime('-3 day', strtotime($examschedule['EXAM_DATE'])));
                          $end_theory_date2 = date('d/m/Y', strtotime('-2 day', strtotime($examschedule['EXAM_DATE'])));
                        }
                      }
                      //echo "<pre>";print_r($examSchedular);echo "</pre>";
                    ?>
                    <div class="col-lg-12" class="table-responsive">
                      <fieldset>
                        <legend style="width: 125px;padding: 5px 15px;margin-bottom:0px;font-size:14px;text-decoration: underline;">Instructions :</legend>
                        <div class="panel panel-default" style="border:none;">
                          <div class="panel-body" style="font-size:12px;">
                            <input type="hidden" id="theory_date" value="<?php echo $actual_theory_date;?>">
                            <ul>
                              <li>Online Exam booking is open between  <b id="start_theory_date"><?php echo $start_theory_date;?></b>   and  <b id="end_theory_date" data-date="<?php echo $end_theory_date2;?>"><?php echo $end_theory_date;?></b></li>
                              <li>All the fields are mandatory to select. </li>
                              <li>Student has to confirm all the details, while doing exam booking. for e.g. Admission id, Name, Centre, Course, Fees, Exam subject, Exam centre, Exam date, Exam time etc.</li>
                              <li>In case of Exam Fee Receipt No. is not visible, then student need to contact their respective centre regarding Exam fees.</li>
                            </ul>
                            <label><u>Student has to follow below process to book their exams:</u></label>
                            <ol>
                              <li>Read Instruction Properly.</li>
                              <li>Click on <b>I Accept & Understand</b> button to ​proceed further.</li>
                              <li>Select <b>Exam Fee Receipt No, Exam Subject, Exam Centre, Exam Date etc</b>.</li>
                              <li><b>Select time slot</b> as per seat availability.</li>
                              <li>Confirm all the details and click on <b>Register</b> button.</li>
                              <li>Once you click on register, details sent on your valid E-mail id.</li>
                              <li style="color:red;font-weight:bold;">Identity Card and Hallticket is compulsory, Student will not be allowed in the examination hall.</li>
                              <li>During exam students are not permitted to have wireless communication devices (e.g. cellphones) on their desks under any circumstances. </li>
                              <li>Discipline should be follow by students. </li>
                            </ol>
                            <label><u>Exam Cancellation process:</u></label>
                            <ul>
                              <li>If any <b>Exam(Practical/Theory)</b> is booked and student will remain <b>ABSENT</b> in the exam he/she has to pay <b>Penalty</b> of <b>Rs.1000 Per Subject</b>.</li><br/>
                              <li>If student <b>FAILED</b> in the <b>Exam(Practical/Theory)</b> he/she has to pay <b>RE-EXAMINATION</b> Fee of <b>Rs. 300 Per Subject</b>.</li><br/>
                              <li style="font-weight:bold;">In case if you are facing any difficulties regarding Exam Booking, Subject not displayed</li>
                              <li style="font-weight:bold;">For exam cancellation inform us before 2 days of exam scheduled.</li>
                              <li style="font-weight:bold;">Mail us for both queries on exam@suntechedu.com</li>
                            </ul>
                          </div>
                        </div>
                      </fieldset>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-4 col-sm-offset-4" id="theory-exam-booking-form">
                          </div>
                        </div>
                      </div>
                    </div>
<! ----------------------------------------------------------->
<?php
  foreach($studData as $sdata){
  ?>

<div id="theory_booking_form" style="display:none;">
<div class="panel-body">
  <?php echo form_open(); ?>
    <div class="col-md-12" style="font-size:12px;">

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
            <div class="col-lg-4">
              <label>Addmission Id: <?php echo $sdata['ADMISSION_ID']?></label>
              <input type="hidden" name="ADMISSION_ID" value="<?php echo $sdata['ADMISSION_ID']?>">
              <input type="hidden" name="STREAM" value="<?php echo $sdata['STREAM']?>">
              <input type="hidden" id="theory_exam_form_visible" name="VISIBLE" value="0">
            </div>
            <div class="col-lg-4">
              <label>Full Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
            </div>
            <div class="col-lg-4">
              <label>Centre Name: <?php echo $sdata['CENTRE_NAME']?></label>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
            <div class="col-lg-4">
              <label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
            </div>
            <div class="col-lg-4">
              <?php
                $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                     $courseNames = '';
                      for($j=0;$j<count($courses);$j++){
                          if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                              $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                            }

                      }
                ?>
                <?php
                $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                     $moduleNames = '';
                      for($j=0;$j<count($modules);$j++){
                          if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                              $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                            }

                      }
                     $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
                     unset($sdata['MODULE_TAKEN']);
                ?>
              <label>Course Taken : <?php echo $sdata['COURSE_TAKEN']?></label>
            </div>
            <div class="col-lg-4">
              <label>Admission Type : <?php if($sdata['ADMISSION_TYPE'] == "1"){echo "Regular";}else{echo $sdata['ADMISSION_TYPE'];}?></label>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">
              <div class="col-md-4">
                <label>Exam Fee Receipt No : <span class="text-danger">*</span></label>
                <select name="EXAM_FEES_RECEIPT_NO" class="form-control">
                  <option value="">Select Receipt</option>
                  <?php foreach($otherReceipts as $otherreceipt){?>
                    <option value="<?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?>"><?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-8">
                <?php $exampractical = array();
                      $examtheory = array();
                      $examdata = array();
                foreach($examdetails as $exam){
                  if($exam['EXAM_TYPE'] == "PRACTICAL"){
                    array_push($exampractical,$exam['COURSE_ID']);
                  }
                  else if($exam['EXAM_TYPE'] == "THEORY"){
                    array_push($examtheory,$exam['COURSE_ID']);
                  }
                }
                $examdata = array_diff($exampractical, $examtheory);
                ?>
                <label>Subject Name : <span class="text-danger">*</span></label>
                <select name="COURSE_ID" class="form-control">
                  <option value="">Exam Subject</option>
                  <?php foreach($courseDetails as $course){
                          if(in_array($course['COURSE_ID'], $examdata)){?>
                            <option value="<?php echo $course['COURSE_ID'];?>"><?php echo $course['COURSE_NAME'];?></option>
                  <?php } } ?>
                </select>
              </div>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">

              <div class="col-md-2">
								<label>Exam Centre : <span class="text-danger">*</span></label>
								<select name="CENTRE_ID" class="form-control select_exam_centre_name">
									<option value="">Select Centre Name</option>
									<?php foreach($centres as $centre){
														if(($centre['CENTRE_ID'] == "155") || ($centre['CENTRE_ID'] == "200")){?>
														<option value="<?php echo $centre['CENTRE_ID'];?>"><?php echo $centre['CENTRE_NAME'];?></option>
									<?php     }
													}?>
								</select>
							</div>
              <div class="col-md-4">
                <label>Active Email Id : <span class="text-danger">*</span></label>
                <input type="text" class="form-control" style="height:34px;" name="EMAIL_ID">
              </div>
              <div class="col-md-3">
                <label>Exam Date : <span class="text-danger">*</span></label>
                <?php	$examDate = str_replace('/', '-',$actual_theory_date);
                      $exam_date =  date("Y-m-d", strtotime($examDate) ); ?>
                <input type="text" class="form-control" style="height:34px;" value="<?php echo $actual_theory_date;?>" readonly>
                <input type="hidden" name="EXAM_DATE" class="form-control" id="exam_scheduled_date" value="<?php echo $exam_date;?>">
              </div>
              <div class="col-md-3">
								<label>Exam Time : <span class="text-danger">*</span></label>
								<input type="text" name="EXAM_TIME" class="form-control" style="height:34px;" id="exam_scheduled_time" readonly>
							</div>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row" style="margin-left:-30px;">
          <div class="col-lg-12" style="margin-left:0px;">

              <div class="col-md-4">
              </div>
              <div class="col-md-4">
                <input class="adminToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="exam_type" value="theory">
                <button type="submit" name="book_exam" class="btn btn-primary">Register</button>
              </div>

          </div>
        </div>
      </div>


            <div class="form-group" id="exam_time_select_table" style="display:none;">
              <div class="row" style="margin-left:-30px;">
                <div class="col-lg-12" style="margin-left:0px;">

                  <div class="panel panel-default">
                    <div class="panel-body table-responsive">
                      <table class="table table-striped table-hover" style="font-size:12px;">
                        <thead>
                          <tr>
                            <th>Timing</th>
                            <th>Capacity</th>
                            <th>Booked</th>
                            <th>Available Seats</th>
                            <th>Select Time</th>
                          </tr>
                        </thead>
                        <tbody id="exam_slots">
                          <tr><td>09:00 AM</td><td>20</td><td id="bookedfor900am" data-time1="09:00" data-time2="09:00 AM"></td><td class="availableseat" id="notbookedfor900am" data-time1="09:00" data-time2="09:00 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor900am" data-time="09:00 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>09:50 AM</td><td>20</td><td id="bookedfor950am" data-time1="09:50" data-time2="09:50 AM"></td><td class="availableseat" id="notbookedfor950am" data-time1="09:50" data-time2="09:50 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor950am" data-time="09:50 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>10:40 AM</td><td>20</td><td id="bookedfor1040am" data-time1="10:40" data-time2="10:40 AM"></td><td class="availableseat" id="notbookedfor1040am" data-time1="10:40" data-time2="10:40 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1040am" data-time="10:40 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>11:30 AM</td><td>20</td><td id="bookedfor1130am" data-time1="11:30" data-time2="11:30 AM"></td><td class="availableseat" id="notbookedfor1130am" data-time1="11:30" data-time2="11:30 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1130am" data-time="11:30 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>12:20 PM</td><td>20</td><td id="bookedfor1220pm" data-time1="12:20" data-time2="12:20 PM"></td><td class="availableseat" id="notbookedfor1220pm" data-time1="12:20" data-time2="12:20 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1220pm" data-time="12:20 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>01:10 PM</td><td>20</td><td id="bookedfor110pm" data-time1="01:10" data-time2="01:10 PM"></td><td class="availableseat" id="notbookedfor110pm" data-time1="01:10" data-time2="01:10 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor110pm" data-time="01:10 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>02:00 PM</td><td>20</td><td id="bookedfor200pm" data-time1="02:00" data-time2="02:00 PM"></td><td class="availableseat" id="notbookedfor200pm" data-time1="02:00" data-time2="02:00 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor200pm" data-time="02:00 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>02:50 PM</td><td>20</td><td id="bookedfor250pm" data-time1="02:50" data-time2="02:50 PM"></td><td class="availableseat" id="notbookedfor250pm" data-time1="02:50" data-time2="02:50 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor250pm" data-time="02:50 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>03:40 PM</td><td>20</td><td id="bookedfor340pm" data-time1="03:40" data-time2="03:40 PM"></td><td class="availableseat" id="notbookedfor340pm" data-time1="03:40" data-time2="03:40 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor340pm" data-time="03:40 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>04:30 PM</td><td>20</td><td id="bookedfor430pm" data-time1="04:30" data-time2="04:30 PM"></td><td class="availableseat" id="notbookedfor430pm" data-time1="04:30" data-time2="04:30 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor430pm" data-time="04:30 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                          <tr><td>05:20 PM</td><td>20</td><td id="bookedfor520pm" data-time1="05:20" data-time2="05:20 PM"></td><td class="availableseat" id="notbookedfor520pm" data-time1="05:20" data-time2="05:20 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor520pm" data-time="05:20 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>
              </div>
            </div>

      </div>
    </div>
  </div>
   <?php echo form_close(); ?>
  </div>
<?php }?>
<! ----------------------------------------------------------->
                  </div>
                </div>
                <div class="col-lg-3">&nbsp;</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>