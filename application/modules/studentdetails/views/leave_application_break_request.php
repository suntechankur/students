<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-6">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Break Request / Leave Application</a>
                </div>
                <div class="col-xs-6">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-9">
                  <div id="update-details-form" class="student_desk_forms">
                    <div class="col-lg-12" id="update_student_details" class="table-responsive">
                      <?php echo form_open();?>
                        <?php  foreach($studData as $sdata){ ?>
                        <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <?php
                          $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                               $courseNames = '';
                               //echo '<ul class="list-group">';
                                for($j=0;$j<count($courses);$j++){
                                    if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                          $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                        $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                      }

                                }

                          $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                               $moduleNames = '';
                               //echo '<ul class="list-group">';
                                for($j=0;$j<count($modules);$j++){
                                    if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                          $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                        $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                      }

                                }
                               //echo '</ul>';
                               $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
                               unset($sdata['MODULE_TAKEN']);
                          ?>
                        <div class="col-sm-12">
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>Admission Id: <?php echo $sdata['ADMISSION_ID']?></label>
                              <input type="hidden" name="ADMISSION_ID" value="<?php echo $sdata['ADMISSION_ID']?>">
                              <input type="hidden" name="CENTRE_ID" value="<?php echo $sdata['CENTRE_ID']?>">
                              <input type="hidden" name="NAME" value="<?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?>">
                              <input type="hidden" name="CENTRE_NAME" value="<?php echo $sdata['CENTRE_NAME']?>">
                              <input type="hidden" name="COURSE_TAKEN" value="<?php echo $sdata['COURSE_TAKEN']?>">
                              <input type="hidden" name="ADMISSION_DATE" value="<?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?>">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <label>Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="col-md-8">
                            <div class="form-group">
                              <label>Course Taken: <?php echo $sdata['COURSE_TAKEN']?></label>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Centre: <?php echo $sdata['CENTRE_NAME']?></label>
                            </div>
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Break Request&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="radio" name="application_or_request" class="application_or_request" value="break_request">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Leave Application&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="radio" name="application_or_request" class="application_or_request" value="leave_application">
                            </div>
                          </div>
                        </div>

                        <div class="request_application_form" id="break_request" style="display:none;">
                          <div class="col-sm-12">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>From Date: </label><input type="text" name="FROM_DATE" tabindex="1" class="form-control datepicker" placeholder="From Date">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>To Date: </label><input type="text" name="TO_DATE" tabindex="2" class="form-control datepicker" placeholder="To Date">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Expected Rejoining Date: </label><input type="text" name="EXPECTED_REJOINING_DATE" tabindex="3" class="form-control datepicker" placeholder="Rejoining Date">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Reason For Break (max 500 chars) : </label><textarea name="CONTENT_OF_REASON" pattern=".{30,500}" style="border-radius:0px;" tabindex="4" class="form-control" placeholder="Reason"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="request_application_form" id="leave_application" style="display:none;">
                          <div class="col-sm-12">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Leave From Date: </label><input type="text" name="LEAVE_FROM_DATE" tabindex="1" class="form-control datepicker" placeholder="From Date">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Leave To Date: </label><input type="text" name="LEAVE_TO_DATE" tabindex="2" class="form-control datepicker" placeholder="To Date">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Reason For Leave (max 500 chars) : </label><textarea name="LEAVE_REASON" pattern=".{30,500}" style="border-radius:0px;" tabindex="3" class="form-control" placeholder="Reason"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php } ?>
                      <div class="form-group" id="application_button" style="display:none;">
                        <div class="row">
                          <div class="col-sm-2 col-sm-offset-4">
                            <input type="submit" name="send_request" tabindex="5" class="btn btn-login" value="Submit" style="border-radius:0px;background:rgb(237,170,30);color: #fff;font-size: 12px;height: auto;font-weight: bold;padding: 10px 20px;border-color: #000">
                          </div>
                        </div>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">&nbsp;</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>