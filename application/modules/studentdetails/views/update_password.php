<div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><!-- other content --><br/></div><br/>
            <div class="row">
              <div class="col-lg-9">
                <div class="col-xs-4">
                  <a href="javascript:void(0)" class="student_desk_tabs active">Update Password</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                </div>
              </div>
              <div class="col-lg-3"></div>
              <hr>
            </div>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="student_desk_forms">
                    <div class="col-lg-12" id="update_student_details" class="table-responsive">
                      <?php echo form_open();?>
                      <div class="col-sm-6 col-sm-offset-1">
                        <input type="hidden" class="adminToken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="form-group">
                          <label>New Password : </label><input type="password" name="PASSWORD" tabindex="1" class="form-control" placeholder="New Password">
                        </div>
                        <div class="form-group">
                          <label>Re Type Password : </label><input type="password" name="NEW_PASSWORD" tabindex="2" class="form-control" placeholder="Re Type Password">
                        </div>
                        <div class="form-group">
                          <label style="font-size:14px;color:red;">*Note : After password update your session automatically gets logged out.</label>
                        </div>
                        <div class="form-group col-sm-4 col-sm-offset-4">
                          <input type="submit" name="update_password" tabindex="3" class="btn btn-login" value="Update" style="border-radius:0px;background:rgb(237,170,30);color: #fff;font-size: 12px;height: auto;font-weight: bold;padding: 10px 20px;border-color: #000">
                        </div>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>