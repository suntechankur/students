<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studentdetails_model extends CI_Model {

    public $exam_db;
    public function __construct() {
        $this->exam_db = $this->load->database('exam',TRUE);
        $this->student = $this->load->database('studentdesk',TRUE);
        $this->website = $this->load->database('website',TRUE);
        parent::__construct();
    }

    public function getStudentData($admission_id){
        $where = "AM.ADMISSION_ID = '$admission_id' AND AR.ISACTIVE='1' AND AR.AMOUNT_PAID IS NOT NULL";
        $query = $this->db->select('AM.ADMISSION_ID,AM.ENQUIRY_ID,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_MIDDLENAME,EM.ENQUIRY_LASTNAME,CM.CENTRE_NAME,CM.CENTRE_ID,AM.ADMISSION_DATE,AM.STREAM,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.STATUS,AM.REMARKS,AM.ADMISSION_TYPE,AM.TOTALFEES,SUM(AR.AMOUNT_PAID_FEES) as FEE_PAID,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_CITY,EM.ENQUIRY_ZIP,EM.ENQUIRY_STATE,EM.ENQUIRY_COUNTRY,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_EMAIL,EM.ENQUIRY_DATEOFBIRTH,EpM.EMP_FNAME,AM.IN_TAKE,AM.YEAR,AM.ADMISSION_TYPE,AM.CENTRE_ID,CM.CENTRE_NAME,AM.UNIVERSITY_NAME,AM.UNIVERSITY_REGISTRATION_NO,AM.UNIVERSITY_REGISTRATION_NO_2')
                          ->from('admission_master AM')
                          ->join('enquiry_master EM', 'AM.ENQUIRY_ID=EM.ENQUIRY_ID', 'left')
                          ->join('centre_master CM', 'AM.CENTRE_ID=CM.CENTRE_ID', 'left')
                          ->join('admission_receipts AR', 'AM.ADMISSION_ID=AR.ADMISSION_ID', 'left')
                          ->join('employee_master EpM', 'AM.HANDLED_BY=EpM.EMPLOYEE_ID', 'left')
                          ->where($where)
                          ->get();

        return $query->result_array();
    }

    public function getCenterDetails(){
        $query = $this->db->select('*')
                          ->from('centre_master')
                          ->group_by('CENTRE_NAME')
                          ->order_by('ISACTIVE','DESC')
                          ->get();

        return $query->result_array();
    }
    public function getUniversityDetails(){
        $query = $this->db->select('UNIVERSITY_NAME')
                          ->from('admission_master')
                          ->group_by('UNIVERSITY_NAME')
                          ->get();

        return $query->result_array();
    }
    public function getCourseDetails($admission_id){
        $query = $this->db->select('MM.MODULE_NAME,CM.COURSE_NAME,ACT.COURSE_ID')
                          ->from('admission_course_transaction ACT')
                          ->join('module_master MM', 'MM.MODULE_ID=ACT.MODULE_ID', 'left')
                          ->join('course_master CM', 'CM.COURSE_ID=ACT.COURSE_ID', 'left')
                          ->where('ACT.ADMISSION_ID',$admission_id)
                          ->get();

        return $query->result_array();
    }

    public function getAdmissionInstallmentTransaction($admId){
      $query = $this->db->select('DUEDATE,DUE_AMOUNT,ADMISSION_INSTALLEMENT_ID')
                        ->from('admission_installment_transaction')
                        ->where('ADMISSION_ID',$admId)
                        ->get();
                        //print $this->db->last_query();
      return $query->result_array();
    }

    public function getAdmissionReceipt($admId){
      $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,AR.ADMISSION_RECEIPT_NO,AR.PAYMENT_DATE,AR.AMOUNT_PAID,AR.AMOUNT_PAID_FEES,AR.PAYMENT_TYPE,AM.TOTALFEES,AR.AMOUNT_PAID_SERVICETAX,AR.CHEQUE_NO,AR.CHEQUE_DATE,AR.CHEQUE_BANK_NAME,AR.AMOUNT_DEPOSITED')
                        ->from('admission_receipts AR')
                        ->join('admission_master AM','AM.ADMISSION_ID = AR.ADMISSION_ID')
                        ->where('AM.ADMISSION_ID',$admId)
                        ->where('AR.ISACTIVE','1')
                        ->get();
      //print $this->db->last_query();
      return $query->result_array();
    }

    public function all_courses(){
      $query = $this->db->select('COURSE_ID,COURSE_NAME,MIN_FEES,MAX_FEES')
                        ->from('course_master')
                        ->group_by('COURSE_NAME');

      $query = $this->db->get();
      return $query->result_array();
    }

    public function all_modules(){
      $query = $this->db->select('MODULE_ID,MODULE_NAME')
                        ->from('module_master')
                        ->get();
      return $query->result_array();
    }

    public function getOtherReceiptsData($admId){
      $query = $this->db->select('PR.ADMISSION_ID,PR.PENALTY_RECEIPT_ID,PR.PENALTY_RECEIPT_NO,PR.PAYMENT_DATE,if(CF.CONTROLFILE_VALUE IS NULL,PR.PARTICULAR,CF.CONTROLFILE_VALUE) as PARTICULAR,PR.AMOUNT,PR.AMOUNT_WITHOUT_ST,PR.PAYMENT_TYPE,PR.SERVICE_TAX,PR.CHEQUE_NO,PR.CHEQUE_DATE,PR.BANK_NAME,PR.BRANCH')
                        ->from('penalties_receipt_master PR')
                        ->join('control_file CF','CF.CONTROLFILE_ID = PR.PARTICULAR','left')
                        ->where('PR.ADMISSION_ID',$admId)
                        ->where('PR.ISACTIVE','1')
                        ->get();
      return $query->result_array();
    }

    public function getStudentCourseStatus($enqId){
        $query = $this->db->select('BST.BATCHID,CM.COURSE_NAME,CeM.CENTRE_NAME,BM.FACULTY_NAME,EM.EMP_FNAME,EM.EMP_MIDDLENAME,EM.EMP_LASTNAME,BM.DAYS,BM.STARTTIME,BM.ENDTIME,BM.STARTDATE,BM.ACTUALENDDATE')
                          ->from('batch_master_std_trans BST')
                          ->join('batch_master BM', 'BM.BATCHID=BST.BATCHID', 'left')
                          ->join('course_master CM', 'BM.SUBJECT=CM.COURSE_ID', 'left')
                          ->join('centre_master CeM', 'BM.CENTRE_ID=CeM.CENTRE_ID', 'left')
                          ->join('employee_master EM', 'BM.FACULTY=EM.EMPLOYEE_ID', 'left')
                          ->where('BST.ADMISSION_ID',$enqId)
                          ->get();

        return $query->result_array();
    }

    public function getStudentLectureDetails($batchId){
        $query = $this->db->select('BATCH_ID')
                          ->from('batch_lecture_details')
                          ->where('BATCH_ID',$batchId)
                          ->get();

        return $query->num_rows();
    }
    public function getStudentAttendanceDetails($batchId,$enqId){
      $where = "BLD.BATCH_ID ='$batchId' AND BAD.ADMISSION_ID ='$enqId' AND PRESENT = '1'";
        $query = $this->db->select('PRESENT')
                          ->from('batch_lecture_details BLD')
                          ->join('batch_attendance_details BAD', 'BLD.BATCH_LECTURE_ID=BAD.BATCH_LECTURE_ID', 'left')
                          ->where($where)
                          ->get();

        return $query->num_rows();
    }

    public function getStudentExamStatus($admission_id){
        $query = $this->db->select('EMN.EXAM_MASTER_NEW_ID as EXAM_ID,EMN.EXAM_DATE,EMN.EXAM_TIME,EMN.ONLINE_MARKS as EXTERNAL_MARKS,EMN.ONLINE_MARKS_OUT_OF as EXTERNAL_OUT_OF,EMN.PROJECT_MARKS as INTERNAL_MARKS,EMN.PROJECT_MARKS_OUT_OF as INTERNAL_OUT_OF,EMN.PERCENTAGE,EMN.RESULT,CM.COURSE_NAME,EMN.EXAM_TYPE')
                          ->from('exam_master_new EMN')
                          ->join('course_master CM', 'CM.COURSE_ID=EMN.COURSE_ID', 'left')
                          ->where('EMN.ADMISSION_ID',$admission_id)
                          ->get();

        return $query->result_array();
    }

    public function getExamDatesAsPerStream(){
        $query = $this->db->select('EXAM_DATE,STREAM,EXAM_TYPE')
                          ->from('exam_schedule_master')
                          ->where('TRUE',"1")
                          ->get();

        return $query->result_array();
    }

    public function suggested_centre(){
        // $centerRolesIds = array(35,36,37,38,39,40,41,42);
        $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE','1')
                          // if(in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $centerRolesIds)){
                            // ->where('CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID'])
                          // }
                          ->get();
        return $query->result_array();
    }

    public function getExamBookingDetails($admission_id){
        $query = $this->db->select('EXAM_MASTER_NEW_ID,EXAM_DATE,EXAM_TIME,ONLINE_MARKS,ONLINE_MARKS_OUT_OF,JOURNAL_MARKS,JOURNAL_MARKS_OUT_OF,PROJECT_MARKS,PROJECT_MARKS_OUT_OF,PERCENTAGE,EXAM_FEES_RECEIPT_NO,EXAM_TYPE,RESULT,em.COURSE_ID as COURSE_ID,COURSE_NAME,IS_EXAM_GIVEN')
                          ->from('exam_master_new em')
                          ->join('course_master cm','em.COURSE_ID=cm.COURSE_ID','left')
                          ->where('ADMISSION_ID',$admission_id)
                          ->get();

        return $query->result_array();
    }

    public function getExamBookingTimingSlots($centreid,$examdate,$exam_type){
      $query = $this->db->select('EXAM_TIME,count(EXAM_TIME) as Booking_Count')
                        ->from('exam_master_new')
                        ->where('CENTRE_ID',$centreid)
                        ->where('EXAM_DATE',$examdate)
                        ->where('EXAM_TYPE',$exam_type)
                        ->order_by('EXAM_TIME')
                        ->group_by('EXAM_TIME')
                        ->get();

      return $query->result_array();
    }

    public function bookExam($data,$exam_type){
      unset($data['book_exam']);
      unset($data['exam_type']);
      unset($data['EMAIL_ID']);
      unset($data['VISIBLE']);

      $data['MODIFIED_BY'] = $this->session->userdata('student_data')[0]['ADMISSION_ID'];
      $data['MODIFIED_DATE'] = date("Y-m-d");
      if($exam_type == "practical"){
        $data['EXAM_TYPE'] = "PRACTICAL";
        $this->db->insert('exam_master_new',$data);   // for mysql
        $this->sql->insert('exam_master_new',$data);  // for mssql
        if($this->db->affected_rows() > 0){
          return $this->db->insert_id();
        }
        else{
          return false;
        }
      }
      else{
        $data['EXAM_TYPE'] = "THEORY";
        $query = $this->db->select('count(QNo) as question_count')
                          ->from('question_paper_master')
                          ->where('CourseID',$data['COURSE_ID'])
                          ->group_by('QNo')
                          ->get();
        $question_count = $query->row()->question_count;

        $query1 = $this->db->select('count(Course_ID) as qpcount')
                          ->from('qpattern')
                          ->where('Course_ID',$data['COURSE_ID'])
                          ->group_by('Course_ID')
                          ->get();
        $qpcount = $query1->row()->qpcount;

        if($question_count && $qpcount){
          $this->db->insert('exam_master_new',$data);   // for mysql
          $this->sql->insert('exam_master_new',$data);  // for mssql
          if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
          }
          else{
            return "false";
          }
        }
        else{
          return "notfound";
        }
      }

    }

  public function getExamHallticketDetails($admId,$examid,$exam_type){
    $query = $this->db->select('CONCAT(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Name,CM.COURSE_NAME,EN.ADMISSION_ID,EN.EXAM_MASTER_NEW_ID as EXAM_ID,EN.EXAM_DATE,EN.EXAM_TIME,CeM.CENTRE_NAME as Exam_Center,CE.CENTRE_NAME as CENTRE,CE.ADDRESSS1 as ADDRESS,EN.EXAM_TYPE')
                      ->from('exam_master_new EN')
                      ->join('admission_master AM','EN.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','left')
                      ->join('course_master CM','CM.COURSE_ID=EN.COURSE_ID','left')
                      ->join('centre_master CeM','CeM.CENTRE_ID=AM.CENTRE_ID','left')
                      ->join('centre_master CE','CE.CENTRE_ID=EN.CENTRE_ID','left')
                      ->where('EN.ADMISSION_ID',$admId)
                      ->where('EN.EXAM_MASTER_NEW_ID',$examid)
                      ->where('EN.EXAM_TYPE',$exam_type)
                      ->get();

    return $query->result_array();
  }

  public function all_notice(){
    $query = $this->db->select('*')
                      ->from('student_notice')
                      ->order_by('NOTICEDISPLAY_DATE','DESC')
                      ->get();

    return $query->result_array();
  }

  public function get_assignment_details($admission_id){
    $query = $this->db->select('AF.ASSIGNMENT_FEEDING_MASTER_ID as ASSIGNMENT_ID,AF.BATCH_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as Employee,AF.ASSIGNMENT_DATE,AF.ASSIGNMENT_TOPIC,AF.ASSIGNMENT_DESCRIPTION,AF.ASSIGNMENT_SUBMISSION_LAST_DATE as LAST_DATE,CM.COURSE_NAME as SUBJECT,AF.ATTACHMENTS_FOR_REFERENCE')
                      ->from('assignment_feeding_master AF')
                      ->join('batch_master_std_trans BT','BT.BatchID=AF.BATCH_ID','right')
                      ->join('batch_master BM','BM.BATCHID=BT.BatchID','right')
                      ->join('employee_master EM','EM.EMPLOYEE_ID=BM.FACULTY','right')
                      ->join('course_master CM','CM.COURSE_ID=BM.SUBJECT','right')
                      ->where('BT.Admission_ID',$admission_id)
                      ->get();

    return $query->result_array();
  }

  public function update_student_password($admission_id,$password){
    $password = $this->load->encrypt_decrypt_data($password,"encrypt");
    $this->db->set('PASSWORD',$password)
             ->where('ADMISSION_ID',$admission_id)
             ->where('USERNAME',$admission_id)
             ->update('student_desk_users');       // for mysql

       if($this->db->affected_rows() > 0){
         return "true";
       }
  }

  public function update_student_normal_details($data,$enquiry_id){
    $this->db->set($data)
             ->where('ENQUIRY_ID',$enquiry_id)
             ->update('enquiry_master');       // for mysql

       if($this->db->affected_rows() > 0){
         return "true";
       }
       else{
         return "false";
       }
  }

  public function insert_batch_requirement($data){
    unset($data['send_requirement']);

    $data['MODIFIED_DATE'] = date("Y-m-d");
    $data['REQUESTDATE'] = date("Y-m-d");
    $this->db->insert('batch_requirement',$data);   // for mysql
    // $this->sql->insert('exam_master_new',$data);  // for mssql
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function upload_assignment($data){
    unset($data['assignment_submit']);

    $data['CREATED_DATE'] = date("Y-m-d");
    $this->db->insert('assignment_upload_master',$data);
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function placement_details($stream){
    $where = "";
    if($stream == "H"){
      $where = "(STREAM='Hardware and Networking' or STREAM='H/W' or STREAM='')";
    }
    elseif ($stream == "P") {
      $where = "(STREAM='Programming' or STREAM='')";
    }
    elseif ($stream == "G") {
      $where = "(STREAM='Graphics and Animation' or STREAM='Graphics' or STREAM='')";
    }
    elseif ($stream == "B") {
      $where = "(STREAM='Basics' or STREAM='Basic' or STREAM='')";
    }
    elseif ($stream == "O") {
      $where = "(STREAM='Other' or STREAM='')";
    }
    $query = $this->db->select('*')
                      ->from('company_info')
                      ->where('ISACTIVE',"1")
                      ->where($where)
                      ->order_by('COMPANY_ID','DESC')
                      ->get();

    return $query->result_array();
  }

  public function upload_resume($data){
    unset($data['resume_submit']);

    $data['APPLIED_ON_DATE'] = date("Y-m-d");
    $this->db->insert('student_resume',$data);
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function fetch_course_and_subject_details($book_type,$stream){

    $where = "dm.UNIVERSITY_NAME IS NULL";
    $query = $this->student->select('cm.COURSE_NAME,cm.COURSE_ID')
                          ->from('download_material dm')
                          ->join('misonline_new.course_master cm','cm.COURSE_ID=dm.SUBJECT_ID','right')
                          ->where($where)
                          ->where('dm.STREAM',$stream)
                          ->group_by('cm.COURSE_NAME')
                          ->get();

    return $query->result_array();
  }

  public function fetch_university_books_details($stream){
    $where = "";
    if (strpos($this->session->userdata('student_data_course_taken'), 'YCMOU') !== false) {
        $where = "UNIVERSITY_NAME LIKE '%YCMOU%'";
    }

    $query = $this->student->select('*')
                          ->from('download_material')
                          ->where($where)
                          ->where('STREAM',$stream)
                          ->order_by('TYPE')
                          ->get();

    return $query->result_array();
  }

  public function fetch_ebook_by_subject_details($subject){
    $query = $this->student->select('*')
                          ->from('download_material')
                          ->where('SUBJECT_ID',$subject)
                          ->order_by('TYPE')
                          ->get();

    return $query->result_array();
  }

  public function submit_break_request($break_request){
    $this->db->insert('STUDENT_BREACK_APPLICATION',$break_request);
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function submit_leave_application($leave_application){
    $this->db->insert('STUDENT_LEAVE_APLLICATION_DETAILS',$leave_application);
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function get_centre_person_mail_id($centre_id){
    $query = $this->student->select('EMP_OFFICIAL_EMAIL')
                            ->from('employee_master')
                            ->where('CENTRE_ID',$centre_id)
                            ->where('ISACTIVE',"1")
                            ->get();

    return $query->result_array();
  }

  public function get_testimonial_video_list(){
    $query = $this->website->select('*')
  					  ->from('testimonial_videos')
  					  ->where('is_active',1)
  					  ->get();
  	return $query->result_array();
  }
}
