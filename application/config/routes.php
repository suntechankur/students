<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = 'login/login_controller/admin_credentials';
$route['404_override'] = 'errors/error_controller/page_not_found';
$route['logout'] = 'login/login_controller/logout';
//$route['instructions'] = 'exam/exam_controller/setinstruction';
// for exam login
// $route['instructions/(:any)'] = 'exam/exam_controller/setinstruction/$1';
$route['student_instructions/(:any)/si/0'] = 'studentdetails/studentdetails_controller/setinstruction/$1';
$route['student_details/(:any)/ds'] = 'studentdetails/studentdetails_controller/student_details/$1';
$route['exam_details/(:any)/dxe'] = 'studentdetails/studentdetails_controller/exam_details/$1';
$route['exam/get_exam_slots'] = 'studentdetails/studentdetails_controller/getexamtimingslot';
$route['notice/(:any)/ce'] = 'studentdetails/studentdetails_controller/student_notices/$1';
$route['notice_download/(:any)/lwn'] = 'studentdetails/studentdetails_controller/notice_download/$1';
$route['assignments/(:any)/ngi'] = 'studentdetails/studentdetails_controller/get_assignments/$1';
$route['download_material/(:any)/md'] = 'studentdetails/studentdetails_controller/study_material/$1';
$route['ebook/get_subject_name'] = 'studentdetails/studentdetails_controller/subject_name_for_search';
$route['ebook/get_book_details'] = 'studentdetails/studentdetails_controller/book_details';
$route['book_download/(:any)/ndb'] = 'studentdetails/studentdetails_controller/book_download/$1';
$route['upload/assignment'] = 'studentdetails/studentdetails_controller/upload_assignment';
$route['requirement_batch/(:any)/hbr'] = 'studentdetails/studentdetails_controller/batch_required/$1';
$route['submit_queries/(:any)/usq'] = 'studentdetails/studentdetails_controller/query_and_complaints/$1';
$route['leave_application_and_break_request/(:any)/al/rs'] = 'studentdetails/studentdetails_controller/leave_application_and_break_request/$1';
$route['placement_training_details/(:any)/dit'] = 'studentdetails/studentdetails_controller/placement_training/$1';
$route['company_videos/(:any)/edc'] = 'studentdetails/studentdetails_controller/videos/$1';
$route['students_testimonial/(:any)/iolt'] = 'studentdetails/studentdetails_controller/testimonials/$1';
$route['edutalks/(:any)/u'] = 'studentdetails/studentdetails_controller/edutalks/$1';
$route['student_policy/(:any)/tiy'] = 'studentdetails/studentdetails_controller/student_policy/$1';
$route['activity_and_events/(:any)/aae'] = 'studentdetails/studentdetails_controller/events_activities/$1';
$route['update_password/(:any)/ssd'] = 'studentdetails/studentdetails_controller/update_password/$1';
