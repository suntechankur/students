<?php defined('BASEPATH') OR die('No direct script access allowed');

class MY_URI extends CI_URI {
    public function filter_uri(&$str)
    {
        if(is_cli()) :
            return;
        endif;

        return parent::filter_uri($str);
    }
}

/* End of file MY_URI.php */
/* Location: ./application/core/MY_URI.php */
